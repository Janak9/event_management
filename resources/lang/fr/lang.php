<?php
   return [
      'msg' => '<h3><b>Instruction pour ajouter une clé PayUMoney</b></h3><br>
          <strong>Paiement uniquement via PayUmoney.</strong><br>
          <small><em>Vous devez avoir pour créer un compte dans PayUmoney.</em></small><br><br>
          <b>Suivez l’étape suivante pour créer un compte et obtenir la clé et le sel du commerçant:</b><br><br>

          <b>Étape 1 :</b><p> Cliquez ici<a href="https://www.payumoney.com/">PayUmoney</a></p>
          <b>Étape 2 :</b><p> Cliquez sur Créer un compte et entrez votre email et cliquez sur Suivant.</p>
          <b>Étape 3 :</b><p> Sélectionnez l\'option "Devenir un marchand". </p>
          <b>Étape 4 :</b><p> Maintenant, remplissez vos informations. Et appuyez sur Suivant.</p>
          <b>Étape 5 :</b><p> Entrez OTP You Recieved.</p>
          <b>Étape 6 :</b><p> Maintenant, cliquez sur "Démarrer l\'intégration" Option.</p>
          <b>Étape 7 :</b><p> Leur clé de commerce et sel de marchand sont simplement affichés. Copiez-le et collez-le.</p>
          <br><br>
          <b><p style="color:red;">S\'il vous plaît vérifier votre clé et le sel que vous avez entré.</p>
              <p style="color:red;"> Si vous entrez une mauvaise clé, vous ne pouvez pas obtenir de paiement. C\'est votre responsabilité de le vérifier.</p>
          </b>'
   ];
?>