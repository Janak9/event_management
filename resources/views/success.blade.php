
{{-- defined('BASEPATH') OR exit('No direct script access allowed'); --}}

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Event Management</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="" type="image/x-icon">
	<link rel="stylesheet" href="{{ URL::to('home_res/css/bootstrap4.min.css') }}">
	<script src="{{ URL::to('home_res/js/bootstrap.min.js') }}" type="text/javascript"></script>
</head>
<body>

<!-- Bootstrap 4 Navbar  -->
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
	<a href="" class="navbar-brand">PayUMoney Gateway</a>

	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
	<div class="collapse navbar-collapse" id="navbarsExampleDefault">
		<ul class="navbar-nav ml-auto">
			<li class="nav-item">
				<a href="/" class="nav-link">Home</a>
			</li>
		</ul>
	</div>
</nav>
<!-- End Bootstrap 4 Navbar -->

	

<div class="container mt-5">
	<div class="row">
        <div class="col-md-2"></div>  
        <div class="col-md-8">
        	<div class="card">
        		<h4 class="card-header">Transaction <label for="Success" class="badge badge-success">Success</label></h4>
        		<div class="card-body">
		                <p>Thank You. Your order status is Success</br>
		                Your Transaction ID for this transaction is {{$data['txnid']}}</br>
		                We have received a payment of Rs. {{$data['amount']}} 0. Your order  will dispatch soon.</p>
		            
        		</div>
        	</div>
            
         </div> 
        <div class="col-md-2"></div>
    </div>
	<!-- Footer -->
	
	<footer style="position: absolute;bottom:0; width: 90%;">
		<hr>
		<p>Copyright &copy; {{ date('Y') }}  
			</p>
	</footer>
</div> 

</body>
</html>
