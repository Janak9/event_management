@extends('home.header')

@section('main-content')
<section class="agenda-section text-center" >
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>    
            <div class="col-md-8">
                <div class="card">
                    @if (session()->get('msg'))
                        <p class="alert alert-success">{{ session()->get('msg') }}</p>
                    @endif
                    <h1>View Package Features</h1>
                    <a class="btn btn-info" href="{{ route('package_features.create') }}">Add Package Features</a>
                    <a class="btn btn-info" href="{{ route('package_features.trashed_PackageFeatures') }}">View Trash</a>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="text-center" scope="col">Id</th>
                                <th class="text-center" scope="col">Package Name</th>
                                <th class="text-center" scope="col">Icon</th>
                                <th class="text-center" scope="col">Description</th>
                                <th class="text-center" scope="col" colspan="2">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @isset($package_features)
                            @foreach ($package_features as $row)
                                <tr>
                                    <td scope="row">{{ $loop->iteration }}</td>
                                    <td>{{ @$row->packages->pkg_name }}</td>
                                    <td scope="row"><i class="fa fa-{{ $row->icon }}"></i> ({{ $row->icon }})</td>
                                    <td scope="row">{{ $row->description }}</td>
                                    <td><a class="btn btn-primary" href="{{ route('package_features.edit', $row->id) }}">Edit</a>
                                        <a class="btn btn-danger" href="{{ route('package_features.destroy', $row->id) }}">Delete</a></td>
                                </tr>
                            @endforeach
                            @endisset
                            @isset($trashed_packagefeatures)
                            @foreach ($trashed_packagefeatures as $row)
                                <tr>
                                        <td scope="row">{{ $loop->iteration }}</td>
                                        <td>{{ $row->packages->pkg_name }}</td>
                                        <td scope="row"><i class="fa fa-{{ $row->icon }}"></i> ({{ $row->icon }})</td>
                                        <td scope="row">{{ $row->description }}</td>
                                        <td><a class="btn btn-primary" href="{{ route('package_features.restore_PackageFeatures', $row->id) }}">Restore</a>
                                            <a class="btn btn-danger" href="{{ route('package_features.forcedelete_PackageFeatures', $row->id) }}">Force Delete</a>
                                </tr>
                            @endforeach
                            @endisset
                        </tbody>
                    </table>
                    @isset($package_features)
                        <div style="">
                            {{ $package_features->links() }} 
                        </div>
                    @endisset
                    @isset($trashed_packagefeatures)
                        <div style="">
                            {{ $trashed_packagefeatures->links() }} 
                        </div>
                    @endisset
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
