@extends('home.header')
@section('slider')
    <!-- slider -->
    <div class="slider-area" id="page-top">
        <div class="bend niceties preview-2">
            <div class="slides nivoSlider" id="ensign-nivoslider">
                <!-- slider image-->
                <img alt="image" src="{{ URL::to('home_res/images/slide1.jpg') }}" title="#slider-direction-1" style="display: none; width: 1351px;">
            </div>
            <!-- end : slider image-->
            <div class="t-cn slider-direction" id="slider-direction-1">
                <div class="slider-progress"></div>
                <div class="slider-content t-cn s-tb slider-1 col-md-12">
                    <!-- slider content-->
                    <div class="title-container s-tb-c title-compress">
                        <div class="tp-caption big-title rs-title customin customout bg-sld-cp-primary wow fadeInDownBig animated"
                            data-wow-delay="0.1s" data-wow-duration="1s"
                            style="visibility: visible;-webkit-animation-duration: 1s; -moz-animation-duration: 1s; animation-duration: 1s;-webkit-animation-delay: 0.1s; -moz-animation-delay: 0.1s; animation-delay: 0.1s;">
                            <span class="title-builder">Event Management</span>
                            <br>
                        </div>
                        <div class="tp-caption small-content customin customout rs-title-small bg-sld-cp-primary tp-resizeme rs-parallaxlevel-0 wow bounceInUp animated"
                            data-wow-delay="0.2s" data-wow-duration="2s"
                            style="visibility: visible;-webkit-animation-duration: 2s; -moz-animation-duration: 2s; animation-duration: 2s;-webkit-animation-delay: 0.2s; -moz-animation-delay: 0.2s; animation-delay: 0.2s;">
                            we make your event awesome!
                        </div>
                    </div>
                    <div class="button tp-caption customin rs-parallaxlevel-0">
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 widget-area">
                            <!-- Widget Search -->
                            <form method="get" action="{{ route('eventsearch') }}">
                                <div class="container" style="border: 1px outset;margin-left: 77%;width: 728px;">
                                    <h2 style="font-weight: bold;
                                    font-size: 18px;">Search Here...</h2>
                                    <ul class="list-inline">
                                    <li style="color: #000;"> 
                                            <select class="form-control col-lg-3" style="width: 190px;height: 40px;" name="search_type">
                                            <option value="-1" disabled>Search By</option>
                                            <option value="1">Events Name</option>
                                            <option value="2" @if (app('request')->input('search_type') == 2)
                                                selected
                                            @endif>Package Details</option>
                                        </select></li>
                                    <li style="color: #000;"><input class="form-control col-lg-3" type="text" style="width: 190px;height: 40px;" placeholder="Search" title="Search By Either Event Name or Either Package Details Wise(Like Price Per Person,City,Date Wise.." name="search" value="{{ app('request')->input('search') }}"></li>
                                    <li style="color: #000;"><button type="submit" style="margin-top:-17%;margin-right:41%;height:40px;width:190px" class="btn btn-chos hvr-shutter-in-horizontal" title="search">Search</button></li>
                                    </ul>
                                </div>
                                {{-- </aside> --}}
                            </form>
                        </div>
                    </div>                 
                </div>
                <!-- end : slider content-->
            </div>
        </div>
    </div>
    <!-- slider end-->
@endsection

@section('main-content')
    <!-- Start:About Section 
==================================================-->
    <section class="about-section" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-sm-8 col-xs-8 wow fadeInRight" data-wow-delay="0.3s"
                    style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: 0.3s; -moz-animation-delay: 0.3s; animation-delay: 0.3s;">
                    <!--About Left-->
                    <div class="about-left">
                        <span>01</span>
                        <h3>About The Event</h3>
                        <p>The Online Event Management System is used for Online Event Package Creation and Booking. So user can save the time and book package at home easily.
                            In our project, user can create their own event package accordong to their choice.
                            User can also Book their event package according to his availability (like date, time, number of seat etc.).
                            We can provide two types of package : 1) public  2) private
                            Public events can any one join.
                            Private events can only booked when user who create it will accept the booking request. 
                            </p>
                        
                    </div>
                    <!--/About Left-->
                </div>
                <!--/.col-lg-6 col-md-4 col-sm-5-->
            </div>
            <!--/.row-->
        </div>
        <!--/container-->
    </section>
    <!-- End:About Section 
==================================================-->


    <!-- Start:Agenda Section 
==================================================-->
    <section class="agenda-section" id="agenda">
        <div class="container">
            <!-- Start: Heading -->
            <div class="title-header">
                <span>02</span>
                <h3>agenda of event</h3>
                <p>The Online Event Management System is used for Online Event Package Creation and Booking. So user can save the time and book package at home easily.
                    In our project, user can create their own event package accordong to their choice.</p>
            </div>
            <!-- End: Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <ul class="nav nav-tabs" id="myTab">
                        <li class="active">
                           <a data-target="#allergist" data-toggle="tab"> <h3 class="title">Packages</h3></a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane active" id="allergist">
                          @isset($packages)
                          {{-- {{ dd($packages) }} --}}
                          @foreach($packages as $pkg) 
                          {{-- @foreach($events as $event)  --}}
                            <div class="col-lg-12 guide-all-list">
                                    <!-- Work Item -->
                                    <div class="col-lg-4 col-sm-6 col-xs-12">
                                        <div class="guide-item-img">
                                            <div class="guide-item"><img alt="" class=""
                                                    src="{{ URL::to('home_res/images/package/'.$pkg->image) }}">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ End: Work Item -->
                                    <!-- Work Item -->
                                    <div class="col-lg-8 col-sm-6 col-xs-12">
                                        <div class="guide-item">
                                            <div id="socialHolder">
                                                <div id="socialShare" class="btn-group share-group">
                                                    <a data-toggle="dropdown" class="btn btn-info">
                                                            <i class="fa fa-share-alt fa-inverse"></i>
                                                    </a>
                                                    <ul class="dropdown-menu" style="min-width: 0px;padding: 0px;background-color: transparent;">
                                                        <li>
                                                            <a target="blank" title="WhatsApp" data-action="share/whatsapp/share" href="https://api.whatsapp.com/send?text={{ url('/') }}/eventDetail/{{$pkg->id}}%0A%0A I inviting you for {{$pkg->pkg_name}}.%0A%0A Click above link to check package information." class="btn btn-whatsapp">
                                                                <i class="fa fa-whatsapp"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a title="Email" class="btn btn-mail" target="blank" href="mailto:?subject=Invitation&body={{ url('/') }}/eventDetail/{{$pkg->id}}%0A%0A I inviting you for {{$pkg->pkg_name}}.%0A%0A Click above link to check package information.">
                                                                <i class="fa fa-envelope"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <h3 class="title">{{$pkg->pkg_name}}
                                            {{-- data-action="share/whatsapp/share" --}}
                                                {{-- {{$pkg->eid}} --}}
                                                {{-- {{$event->event_name}} --}}
                                            </h3>
                                            <h4>
                                                    {{$pkg->events->event_name}}
                                            </h4>
                                            <h4 class="title">Type:{{$pkg->type}}</h4>
                                            <!-- /.title -->
                                            <p class="description">{{$pkg->description}}</p>
                                            <!-- /.description -->
                                            <div class="post-meta">
                                                <ul>
                                                    <li>
                                                        <span>Price Per Person -</span>
                                                        {{$pkg->price_per_person}}
                                                    </li>
                                                    <li><i class="fa fa-clock-o"></i> <span>Person Capacity - {{$pkg->person_capacity}}</span>
                                                    </li>
                                                    <li><i class="fa fa-map-marker"></i> <span>City - {{$pkg->city}}</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <a href="eventDetail/{{$pkg->id}}" class="btn btn-chos hvr-shutter-in-horizontal" title="read">read more</a>
                                        </div>
                                    </div>
                            </div> 
                            {{-- end repeated div --}}
                                {{-- @endforeach --}}
                            @endforeach 
                            @endisset
                      
                        <!--/Item -->
                        {{-- <div class="col-lg-12 guide-all-list">
                            <!-- Work Item -->
                            <div class="col-lg-4 col-sm-6 col-xs-12">
                                <div class="guide-item-img">
                                    <div class="guide-item"><img alt="" class=""
                                            src="{{ URL::to('home_res/images/ag2.jpg') }}">
                                    </div>
                                </div>
                            </div>
                            <!--/ End: Work Item -->
                            <!-- Work Item -->
                            <div class="col-lg-8 col-sm-6 col-xs-12">
                                <div class="guide-item">
                                    <h3 class="title">Improving Website</h3>
                                    <!-- /.title -->
                                    <p class="description">We are a network of board physician are a network of board
                                        certified physician are a network of board certified are a network of board
                                        certified physician are a network of board certified physician are a network of
                                        board certified interventional</p>
                                    <!-- /.description -->
                                    <div class="post-meta">
                                        <ul>
                                            <li>
                                                <span>David Doe -</span> <a
                                                    href="http://ttheme.website/tidytheme/Jonvent/preview/index-slideshow.html#">CEO
                                                    at Facebook</a>
                                            </li>
                                            <li><i class="fa fa-clock-o"></i> <span>75 mins</span>
                                            </li>
                                            <li><i class="fa fa-map-marker"></i> <span>7th floor</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/Item -->
                        <div class="col-lg-12 guide-all-list">
                            <!-- Work Item -->
                            <div class="col-lg-4 col-sm-6 col-xs-12">
                                <div class="guide-item-img">
                                    <div class="guide-item"><img alt="" class=""
                                            src="{{ URL::to('home_res/images/ag3.jpg') }}">
                                    </div>
                                </div>
                            </div>
                            <!--/ End: Work Item -->
                            <!-- Work Item -->
                            <div class="col-lg-8 col-sm-6 col-xs-12">
                                <div class="guide-item">
                                    <h3 class="title">PHP Development</h3>
                                    <!-- /.title -->
                                    <p class="description">We are a network of board physician are a network of board
                                        certified physician are a network of board certified are a network of board
                                        certified physician are a network of board certified physician are a network of
                                        board certified interventional</p>
                                    <!-- /.description -->
                                    <div class="post-meta">
                                        <ul>
                                            <li>
                                                <span>David Doe -</span> <a
                                                    href="http://ttheme.website/tidytheme/Jonvent/preview/index-slideshow.html#">CEO
                                                    at Facebook</a>
                                            </li>
                                            <li><i class="fa fa-clock-o"></i> <span>75 mins</span>
                                            </li>
                                            <li><i class="fa fa-map-marker"></i> <span>7th floor</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                        <!--/Item -->
                        <div class="form-group row mb-0">
                            <div class="col-md-12 offset-md-6">
                                <a href="/eventlist"> 
                                    <input class="submit-button btn" style="margin-left: 43%; margin-top: 28px;" name="submit" value="View More" type="submit">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="neurologist">
                        <div class="col-lg-12 guide-all-list">
                            <!-- Work Item -->
                            <div class="col-lg-4 col-sm-6 col-xs-12">
                                <div class="guide-item-img">
                                    <div class="guide-item"><img alt="" class=""
                                            src="{{ URL::to('home_res/images/ag3.jpg') }}">
                                    </div>
                                </div>
                            </div>
                            <!--/ End: Work Item -->
                            <!-- Work Item -->
                            <div class="col-lg-8 col-sm-6 col-xs-12">
                                <div class="guide-item">
                                    <h3 class="title">PHP Development</h3>
                                    <!-- /.title -->
                                    <p class="description">We are a network of board physician are a network of board
                                        certified physician are a network of board certified are a network of board
                                        certified physician are a network of board certified physician are a network of
                                        board certified interventional</p>
                                    <!-- /.description -->
                                    <div class="post-meta">
                                        <ul>
                                            <li>
                                                <span>David Doe -</span> <a
                                                    href="http://ttheme.website/tidytheme/Jonvent/preview/index-slideshow.html#">CEO
                                                    at Facebook</a>
                                            </li>
                                            <li><i class="fa fa-clock-o"></i> <span>75 mins</span>
                                            </li>
                                            <li><i class="fa fa-map-marker"></i> <span>7th floor</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/Item -->
                        <div class="col-lg-12 guide-all-list">
                            <!-- Work Item -->
                            <div class="col-lg-4 col-sm-6 col-xs-12">
                                <div class="guide-item-img">
                                    <div class="guide-item"><img alt="" class=""
                                            src="{{ URL::to('home_res/images/ag2.jpg') }}">
                                    </div>
                                </div>
                            </div>
                            <!--/ End: Work Item -->
                            <!-- Work Item -->
                            <div class="col-lg-8 col-sm-6 col-xs-12">
                                <div class="guide-item">
                                    <h3 class="title">Improving Website</h3>
                                    <!-- /.title -->
                                    <p class="description">We are a network of board physician are a network of board
                                        certified physician are a network of board certified are a network of board
                                        certified physician are a network of board certified physician are a network of
                                        board certified interventional</p>
                                    <!-- /.description -->
                                    <div class="post-meta">
                                        <ul>
                                            <li>
                                                <span>David Doe -</span> <a
                                                    href="http://ttheme.website/tidytheme/Jonvent/preview/index-slideshow.html#">CEO
                                                    at Facebook</a>
                                            </li>
                                            <li><i class="fa fa-clock-o"></i> <span>75 mins</span>
                                            </li>
                                            <li><i class="fa fa-map-marker"></i> <span>7th floor</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/Item -->
                        <div class="col-lg-12 guide-all-list">
                            <!-- Work Item -->
                            <div class="col-lg-4 col-sm-6 col-xs-12">
                                <div class="guide-item-img">
                                    <div class="guide-item"><img alt="" class=""
                                            src="{{ URL::to('home_res/images/ag1.jpg') }}">
                                    </div>
                                </div>
                            </div>
                            <!--/ End: Work Item -->
                            <!-- Work Item -->
                            <div class="col-lg-8 col-sm-6 col-xs-12">
                                <div class="guide-item">
                                    <h3 class="title">PHP Development</h3>
                                    <!-- /.title -->
                                    <p class="description">We are a network of board physician are a network of board
                                        certified physician are a network of board certified are a network of board
                                        certified physician are a network of board certified physician are a network of
                                        board certified interventional</p>
                                    <!-- /.description -->
                                    <div class="post-meta">
                                        <ul>
                                            <li>
                                                <span>David Doe -</span> <a
                                                    href="http://ttheme.website/tidytheme/Jonvent/preview/index-slideshow.html#">CEO
                                                    at Facebook</a>
                                            </li>
                                            <li><i class="fa fa-clock-o"></i> <span>75 mins</span>
                                            </li>
                                            <li><i class="fa fa-map-marker"></i> <span>7th floor</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/Item -->
                    </div>
                    <div class="tab-pane" id="pathologists">
                        <div class="col-lg-12 guide-all-list">
                            <!-- Work Item -->
                            <div class="col-lg-4 col-sm-6 col-xs-12">
                                <div class="guide-item-img">
                                    <div class="guide-item"><img alt="" class=""
                                            src="{{ URL::to('home_res/images/ag2.jpg') }}">
                                    </div>
                                </div>
                            </div>
                            <!--/ End: Work Item -->
                            <!-- Work Item -->
                            <div class="col-lg-8 col-sm-6 col-xs-12">
                                <div class="guide-item">
                                    <h3 class="title">PHP Development</h3>
                                    <!-- /.title -->
                                    <p class="description">We are a network of board physician are a network of board
                                        certified physician are a network of board certified are a network of board
                                        certified physician are a network of board certified physician are a network of
                                        board certified interventional</p>
                                    <!-- /.description -->
                                    <div class="post-meta">
                                        <ul>
                                            <li>
                                                <span>David Doe -</span> <a
                                                    href="http://ttheme.website/tidytheme/Jonvent/preview/index-slideshow.html#">CEO
                                                    at Facebook</a>
                                            </li>
                                            <li><i class="fa fa-clock-o"></i> <span>75 mins</span>
                                            </li>
                                            <li><i class="fa fa-map-marker"></i> <span>7th floor</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/Item -->
                        <div class="col-lg-12 guide-all-list">
                            <!-- Work Item -->
                            <div class="col-lg-4 col-sm-6 col-xs-12">
                                <div class="guide-item-img">
                                    <div class="guide-item"><img alt="" class=""
                                            src="{{ URL::to('home_res/images/ag4.jpg') }}">
                                    </div>
                                </div>
                            </div>
                            <!--/ End: Work Item -->
                            <!-- Work Item -->
                            <div class="col-lg-8 col-sm-6 col-xs-12">
                                <div class="guide-item">
                                    <h3 class="title">Improving Website</h3>
                                    <!-- /.title -->
                                    <p class="description">We are a network of board physician are a network of board
                                        certified physician are a network of board certified are a network of board
                                        certified physician are a network of board certified physician are a network of
                                        board certified interventional</p>
                                    <!-- /.description -->
                                    <div class="post-meta">
                                        <ul>
                                            <li>
                                                <span>David Doe -</span> <a
                                                    href="http://ttheme.website/tidytheme/Jonvent/preview/index-slideshow.html#">CEO
                                                    at Facebook</a>
                                            </li>
                                            <li><i class="fa fa-clock-o"></i> <span>75 mins</span>
                                            </li>
                                            <li><i class="fa fa-map-marker"></i> <span>7th floor</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/Item -->
                        <div class="col-lg-12 guide-all-list">
                            <!-- Work Item -->
                            <div class="col-lg-4 col-sm-6 col-xs-12">
                                <div class="guide-item-img">
                                    <div class="guide-item"><img alt="" class=""
                                            src="{{ URL::to('home_res/images/ag1.jpg') }}">
                                    </div>
                                </div>
                            </div>
                            <!--/ End: Work Item -->
                            <!-- Work Item -->
                            <div class="col-lg-8 col-sm-6 col-xs-12">
                                <div class="guide-item">
                                    <h3 class="title">PHP Development</h3>
                                    <!-- /.title -->
                                    <p class="description">We are a network of board physician are a network of board
                                        certified physician are a network of board certified are a network of board
                                        certified physician are a network of board certified physician are a network of
                                        board certified interventional</p>
                                    <!-- /.description -->
                                    <div class="post-meta">
                                        <ul>
                                            <li>
                                                <span>David Doe -</span> <a
                                                    href="http://ttheme.website/tidytheme/Jonvent/preview/index-slideshow.html#">CEO
                                                    at Facebook</a>
                                            </li>
                                            <li><i class="fa fa-clock-o"></i> <span>75 mins</span>
                                            </li>
                                            <li><i class="fa fa-map-marker"></i> <span>7th floor</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/Item -->
                    </div>
                    <div class="tab-pane" id="physiatrist">
                        <div class="col-lg-12 guide-all-list">
                            <!-- Work Item -->
                            <div class="col-lg-4 col-sm-6 col-xs-12">
                                <div class="guide-item-img">
                                    <div class="guide-item"><img alt="" class=""
                                            src="{{ URL::to('home_res/images/ag1.jpg') }}">
                                    </div>
                                </div>
                            </div>
                            <!--/ End: Work Item -->
                            <!-- Work Item -->
                            <div class="col-lg-8 col-sm-6 col-xs-12">
                                <div class="guide-item">
                                    <h3 class="title">PHP Development</h3>
                                    <!-- /.title -->
                                    <p class="description">We are a network of board physician are a network of board
                                        certified physician are a network of board certified are a network of board
                                        certified physician are a network of board certified physician are a network of
                                        board certified interventional</p>
                                    <!-- /.description -->
                                    <div class="post-meta">
                                        <ul>
                                            <li>
                                                <span>David Doe -</span> <a
                                                    href="http://ttheme.website/tidytheme/Jonvent/preview/index-slideshow.html#">CEO
                                                    at Facebook</a>
                                            </li>
                                            <li><i class="fa fa-clock-o"></i> <span>75 mins</span>
                                            </li>
                                            <li><i class="fa fa-map-marker"></i> <span>7th floor</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/Item -->
                        <div class="col-lg-12 guide-all-list">
                            <!-- Work Item -->
                            <div class="col-lg-4 col-sm-6 col-xs-12">
                                <div class="guide-item-img">
                                    <div class="guide-item"><img alt="" class=""
                                            src="{{ URL::to('home_res/images/ag1.jpg') }}">
                                    </div>
                                </div>
                            </div>
                            <!--/ End: Work Item -->
                            <!-- Work Item -->
                            <div class="col-lg-8 col-sm-6 col-xs-12">
                                <div class="guide-item">
                                    <h3 class="title">Improving Website</h3>
                                    <!-- /.title -->
                                    <p class="description">We are a network of board physician are a network of board
                                        certified physician are a network of board certified are a network of board
                                        certified physician are a network of board certified physician are a network of
                                        board certified interventional</p>
                                    <!-- /.description -->
                                    <div class="post-meta">
                                        <ul>
                                            <li>
                                                <span>David Doe -</span> <a
                                                    href="http://ttheme.website/tidytheme/Jonvent/preview/index-slideshow.html#">CEO
                                                    at Facebook</a>
                                            </li>
                                            <li><i class="fa fa-clock-o"></i> <span>75 mins</span>
                                            </li>
                                            <li><i class="fa fa-map-marker"></i> <span>7th floor</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/Item -->
                        <div class="col-lg-12 guide-all-list">
                            <!-- Work Item -->
                            <div class="col-lg-4 col-sm-6 col-xs-12">
                                <div class="guide-item-img">
                                    <div class="guide-item"><img alt="" class=""
                                            src="{{ URL::to('home_res/images/ag3.jpg') }}">
                                    </div>
                                </div>
                            </div>
                            <!--/ End: Work Item -->
                            <!-- Work Item -->
                            <div class="col-lg-8 col-sm-6 col-xs-12">
                                <div class="guide-item">
                                    <h3 class="title">PHP Development</h3>
                                    <!-- /.title -->
                                    <p class="description">We are a network of board physician are a network of board
                                        certified physician are a network of board certified are a network of board
                                        certified physician are a network of board certified physician are a network of
                                        board certified interventional</p>
                                    <!-- /.description -->
                                    <div class="post-meta">
                                        <ul>
                                            <li>
                                                <span>David Doe -</span> <a
                                                    href="http://ttheme.website/tidytheme/Jonvent/preview/index-slideshow.html#">CEO
                                                    at Facebook</a>
                                            </li>
                                            <li><i class="fa fa-clock-o"></i> <span>75 mins</span>
                                            </li>
                                            <li><i class="fa fa-map-marker"></i> <span>7th floor</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/Item -->
                    </div>
                </div>
            </div>
            <!---/.row -->
        </div>
        <!--/.container -->
    </section>
    <!-- End: Agenda Section 
==================================================-->
            <!-- Start: Heading -->
            {{-- <div class="title-header">
                <span>04</span>
                <h3>Event News</h3>
                <p>There are many variations of passages of Lorem Ipsum available There are many variations of passages
                    of Lorem Ipsum available There are many variations of passages of Lorem Ipsum available</p>
            </div> --}}
            <!-- End: Heading -->

    <!-- Start: Achieve  Section 
==================================================-->
    <section class="achieve-section" id="achieve-section">
        <!-- container -->
        <div class="container">
            <!-- col-lg-3 col-md-3 col-sm-6 col-xs-6 -->
            <div class="col-lg-3 col-sm-3 col-xs-6">
                <div class="col-lg-4 icon-lay">
                    {{-- <i class="icon_gift_alt"></i> --}}
                    <i class="fa fa-edge" aria-hidden="true"></i>
                </div>
                <div class="col-lg-8">
                    <h2 class="stat-count count">{{ $total_events }}</h2>
                    <h5>Events</h5>
                </div>
            </div>
            <!-- col-lg-3 col-md-3 col-sm-6 col-xs-6 /- -->
            <!-- col-lg-3 col-md-3 col-sm-6 col-xs-6 -->
            <div class="col-lg-3 col-sm-3 col-xs-6">
                <div class="col-lg-4 icon-lay">
                    <i class="fa fa-suitcase" aria-hidden="true"></i>
                </div>
                <div class="col-lg-8">
                    <h2 class="stat-count count">{{ $total_packages }}</h2>
                    <h5>Packages</h5>
                </div>
            </div>
            <!-- col-lg-3 col-md-3 col-sm-6 col-xs-6 /- -->
            <!-- col-lg-3 col-md-3 col-sm-6 col-xs-6 -->
            <div class="col-lg-3 col-sm-3 col-xs-6">
                <div class="col-lg-4 icon-lay">
                    <i class="fa fa-users" aria-hidden="true"></i>
                </div>
                <div class="col-lg-8">
                    <h2 class="stat-count count">{{ $total_users }}</h2>
                    <h5>Users</h5>
                </div>
            </div>
            <!-- col-lg-3 col-md-3 col-sm-6 col-xs-6 /- -->
            <!-- col-lg-3 col-md-3 col-sm-6 col-xs-6 -->
            <div class="col-lg-3 col-sm-3 col-xs-6">
                <div class="col-lg-4 icon-lay">
                    <i class="fa fa-credit-card" aria-hidden="true"></i>
                </div>
                <div class="col-lg-8">
                    <h2 class="stat-count count">{{ $total_payments }}</h2>
                    <h5>Bookings</h5>
                </div>
            </div>
            <!-- col-lg-3 col-md-3 col-sm-6 col-xs-6 /- -->
        </div>
        <!-- container /- -->
    </section>
    <!-- End: Achive Section 
==================================================-->


    <!-- Start: Gallery Section 
==================================================-->
<section class="gallery-section" id="gallery">
        <div class="container">
            <!-- Start: Heading -->
            <div class="title-header">
                <span>03</span>
                <h3>Gallery</h3>
                {{-- <p>There are many Events related photos will be shown </p> --}}
            </div>
            <!-- End: Heading -->
            <div id="main_area">
                <!-- Slider -->
                <div class="row">
                    <div class="col-sm-6" id="slider-thumbs">
                        <!-- Bottom switcher of slide -->
                        <ul class="hide-bullets">
                            <li class="col-lg-6 col-sm-6 col-xs-12">
                                <a class="thumbnail" id="carousel-selector-0"><img alt="gallery" style="height:283px; width:278px;"
                                        src="{{ URL::to('home_res/images/package/1566666506.jpg') }}">
                                </a>
                            </li>
                            <li class="col-lg-6 col-sm-6 col-xs-12">
                                <a class="thumbnail" id="carousel-selector-1"><img alt="gallery" style="height:283px; width:278px;"
                                        src="{{ URL::to('home_res/images/package/1566911819.jpg') }}">
                                </a>
                            </li>
                            <li class="col-lg-6 col-sm-6 col-xs-12">
                                <a class="thumbnail" id="carousel-selector-2"><img alt="gallery" style="height:283px; width:278px;"
                                        src="{{ URL::to('home_res/images/package/1566911722.jpg') }}">
                                </a>
                            </li>
                            <li class="col-lg-6 col-sm-6 col-xs-12">
                                <a class="thumbnail" id="carousel-selector-3"><img alt="gallery" style="height:283px; width:278px;"
                                        src="{{ URL::to('home_res/images/package/1566911549.jpg') }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-6 col-sm-6">
                        <div class="col-lg-12 col-xs-12" id="slider">
                            <!-- Top part of the slider -->
                            <div class="row">
                                <div class="col-lg-12 col-sm-12" id="carousel-bounding-box">
                                    <div class="carousel slide" id="myCarousel">
                                        <!-- Carousel items -->
                                        <div class="carousel-inner">
                                        @foreach($images as $img)
                                            <div class="item @if ($loop->first)
                                                active
                                            @endif" data-slide-number="{{ $loop->iteration }}"><img alt="gallery"
                                                    src="{{ URL::to('home_res/images/package/'.$img->image) }}">
                                            </div>
                                        @endforeach
                                        </div>
                                        <!-- Carousel nav -->
                                        <a class="left carousel-control" data-slide="prev"
                                            href="http://ttheme.website/tidytheme/Jonvent/preview/index-slideshow.html#myCarousel"
                                            role="button"><span class="glyphicon glyphicon-chevron-left"></span></a>
                                        <a class="right carousel-control" data-slide="next"
                                            href="http://ttheme.website/tidytheme/Jonvent/preview/index-slideshow.html#myCarousel"
                                            role="button"><span class="glyphicon glyphicon-chevron-right"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/Slide-->
                    </div>
                </div>
                <!-- row /- -->
            </div>
        </div>
        <!-- container /- -->
    </section>
    <!-- End: Gallery Section 
==================================================-->


@endsection