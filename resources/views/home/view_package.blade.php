@extends('home.header')

@section('main-content')
<section class="agenda-section" >
    <div class="container">
        <div class="row">    
            <div class="col-md-12">
                <div class="card">
                    <div class="text-center">
                    @if (session()->get('msg'))
                        <p class="alert alert-success">{{ session()->get('msg') }}</p>
                    @endif
                    <h1>View Packages</h1>
                    <a class="btn btn-info" href="{{ route('packages.create') }}">Add Package</a>
                    <a class="btn btn-info" href="{{ route('packages.trashed_packages') }}">View Trash</a>
                    </div>
                    @isset($packages)
                    @foreach ($packages as $row)
                    <div class="col-lg-12 guide-all-list">
                        <!-- Work Item -->
                        <div class="col-lg-4 col-sm-6 col-xs-12">
                            <div class="guide-item-img">
                                <div class="guide-item">
                                    <img src="{{ URL::to('home_res/images/package/'.$row->image) }}">
                                </div>
                            </div>
                        </div>
                        <!--/ End: Work Item -->
                        <!-- Work Item -->
                        <div class="col-lg-8 col-sm-6 col-xs-12">
                            <div class="guide-item">
                                <div id="socialHolder">
                                    <div id="socialShare" class="btn-group share-group">
                                        <a data-toggle="dropdown" class="btn btn-info">
                                                <i class="fa fa-share-alt fa-inverse"></i>
                                        </a>
                                        <ul class="dropdown-menu" style="min-width: 0px;padding: 0px;background-color: transparent;">
                                            <li>
                                                <a target="blank" title="WhatsApp" data-action="share/whatsapp/share" href="https://api.whatsapp.com/send?text={{ url('/') }}/eventDetail/{{$row->id}}%0A%0A I inviting you for {{$row->pkg_name}}.%0A%0A Click above link to check package information." class="btn btn-whatsapp">
                                                    <i class="fa fa-whatsapp"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a title="Email" class="btn btn-mail" target="blank" href="mailto:?subject=Invitation&body={{ url('/') }}/eventDetail/{{$row->id}}%0A%0A I inviting you for {{$row->pkg_name}}.%0A%0A Click above link to check package information.">
                                                    <i class="fa fa-envelope"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <h3 class="title">{{$row->pkg_name}}
                                    {{-- {{$row->eid}} --}}
                                    {{-- {{$event->event_name}} --}}
                                </h3>
                                <h4>{{$row->events->event_name}}</h4>
                                <h4 class="title">Type:{{$row->type}}</h4>
                                <!-- /.title -->
                                <h4>Deposite Per Person - {{$row->deposite_per_person}}</h4>
                                <h4>Available Seat - {{$row->available_seat}}</h4>
                                <!-- /.description -->
                                <div class="post-meta">
                                    <ul>
                                        <li>
                                            <span>Price Per Person -</span>
                                            {{$row->price_per_person}}
                                        </li>
                                        <li><i class="fa fa-clock-o"></i> <span>Person Capacity - {{$row->person_capacity}}</span>
                                        </li>
                                        <li><i class="fa fa-map-marker"></i> <span>City - {{$row->city}}</span>
                                        </li>
                                    </ul>
                                </div>
                                <a href="eventDetail/{{$row->id}}" class="btn btn-chos hvr-shutter-in-horizontal" title="read">read more</a>
                                <div style="text-align: right">
                                    <h4><a class="btn btn-primary" href="{{ route('packages.edit', $row->id) }}">Edit</a>
                                        <a class="btn btn-danger" href="{{ route('packages.destroy', $row->id) }}">Delete</a>
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div> 
                    @endforeach
                    @endisset
                    @isset($trashed_packages)
                    @foreach ($trashed_packages as $row)
                    <div class="col-lg-12 guide-all-list">
                        <!-- Work Item -->
                        <div class="col-lg-4 col-sm-6 col-xs-12">
                            <div class="guide-item-img">
                                <div class="guide-item"><img alt="" class=""
                                        src="{{ URL::to('home_res/images/package/'.$row->image) }}">
                                </div>
                            </div>
                        </div>
                        <!--/ End: Work Item -->
                        <!-- Work Item -->
                        <div class="col-lg-8 col-sm-6 col-xs-12">
                            <div class="guide-item">
                                <h3 class="title">{{$row->pkg_name}}
                                    {{-- {{$row->eid}} --}}
                                    {{-- {{$event->event_name}} --}}
                                </h3>
                                <h4>{{$row->events->event_name}}</h4>
                                <h4 class="title">Type:{{$row->type}}</h4>
                                <!-- /.title -->
                                <h4>Deposite Per Person - {{$row->deposite_per_person}}</h4>
                                <!-- /.description -->
                                <div class="post-meta">
                                    <ul>
                                        <li>
                                            <span>Price Per Person -</span>
                                            {{$row->price_per_person}}
                                        </li>
                                        <li><i class="fa fa-clock-o"></i> <span>Person Capacity - {{$row->person_capacity}}</span>
                                        </li>
                                        <li><i class="fa fa-map-marker"></i> <span>City - {{$row->city}}</span>
                                        </li>
                                    </ul>
                                </div>
                                <a href="eventDetail/{{$row->id}}" class="btn btn-chos hvr-shutter-in-horizontal" title="read">read more</a>
                                <div style="text-align: right">
                                    <h4><a class="btn btn-primary" href="{{ route('packages.restore_packages', $row->id) }}">Restore</a>
                                        <a class="btn btn-danger" href="{{ route('packages.forcedelete_packages', $row->id) }}">Force Delete</a>
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div> 
                    @endforeach
                    @endisset
                    {{-- <div style="">
                            {{ $packages->appends(['search_type' =>  Request::get('search_type'),'search' => Request::get('search') ])->links() }} 
                    </div> --}}

                    @isset($packages)
                    <div style="">
                        {{ $packages->links() }} 
                    </div>
                    @endisset
                    @isset($trashed_packages)
                    <div style="">
                        {{ $trashed_packages->links() }} 
                    </div>
                    @endisset

                    {{-- <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="text-center" scope="col">Id</th>
                                <th class="text-center" scope="col">Type</th>
                                <th class="text-center" scope="col">City</th>
                                <th class="text-center" scope="col">Contact No</th>
                                <th class="text-center" scope="col">Price Per Person</th>
                                <th class="text-center" scope="col">Deposite Per Person</th>
                                <th class="text-center" scope="col">Person Capacity</th>
                                <th class="text-center" scope="col" colspan="2">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($packages as $row)
                                <tr>
                                    <td scope="row">{{ $row->id }}</td>
                                    <td>{{ $row->type }}</td>
                                    <td>{{ $row->city }}</td>
                                    <td>{{ $row->contact_no }}</td>
                                    <td>{{ $row->price_per_person }}</td>
                                    <td>{{ $row->deposite_per_person }}</td>
                                    <td>{{ $row->person_capacity }}</td>
                                    <td><a class="btn btn-primary" href="{{ route('packages.edit', $row->id) }}">Edit</a>
                                        <a class="btn btn-danger" href="{{ route('packages.destroy', $row->id) }}">Delete</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table> --}}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection