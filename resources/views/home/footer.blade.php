
    <!-- Start:Footer Section 
==================================================-->
    <footer class="footer-section footer-indx">
        <div class="container">
            <div class="row">
                <!-- Start: contact info -->
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="footerwidget contact_info">
                        <h5>contact info</h5>
                        <ul>
                            <li>
                                <i class="fa fa-map-marker"></i>
                                <p>
                                        Dr. R.K. Desai Marg,
                                        Athwalines, Surat
                                </p>
                            </li>
                            <li>
                                <i class="fa fa-phone"></i>
                                <p>+00 978-783-0597</p>
                            </li>
                            <li>
                                <i class="fa fa-envelope"></i>
                                <p>eventmanagement@gmail.com</p>
                            </li>
                        </ul>
                        {{-- <ul class="social-footerr list-inline">
                            <li>
                                <a href="#"
                                    title="Follow Us on Facebook"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="#"
                                    title="Watch Our Videos"><i class="fa fa-instagram"></i></a>
                            </li>
                            <li>
                                <a href="#"
                                    title="Follow Us on Twitter"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="#"
                                    title="Watch Our Videos"><i class="fa fa-youtube-play"></i></a>
                            </li>
                        </ul> --}}
                    </div>
                </div>
                <!-- End: About -->
                <!-- Start: Explor link -->
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="footerwidget">
                        <h5>useful links</h5>
                        <div class="widget_mailchimp" id="mailchimp-2">
                            <div class="newsletter_form_wrapper">
                                <ul>
                                    <li><a href="{{ route('events.index') }}">Event</a></li>
                                    <li><a href="{{ route('login') }}">Login</a></li>
                                    <li><a href="{{ route('register') }}">Register</a></li>
                                    <li>  <a href="/aboutus">About Us</a></li>
                                </ul>
                            </div>
                            <!--end newsletter_form_wrapper -->
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="footerwidget">
                            <h5>Network</h5>
                            <div class="widget_mailchimp" id="mailchimp-2">
                                <div class="newsletter_form_wrapper">
                                        <ul class="social-footerr list-inline" style="color:#bababa;">
                                                <li>
                                                    <a href="#"
                                                        title="Follow Us on Facebook"><i class="fa fa-facebook"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#"
                                                        title="Watch Our Videos"><i class="fa fa-instagram"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#"
                                                        title="Follow Us on Twitter"><i class="fa fa-twitter"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#"
                                                        title="Watch Our Videos"><i class="fa fa-youtube-play"></i></a>
                                                </li>
                                        </ul>
                                </div>
                                <!--end newsletter_form_wrapper -->
                            </div>
                        </div>
                    </div>
                <!-- End: Explor link -->
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="footerwidget">
                        <h5>About Us</h5>
                        <p style="color:#bababa;font-size:14px">
                            The Online Event Management System is used for Online Event Package Creation and Booking. So user can save the time and book package at home easily.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- Start:Subfooter -->
        <div class="subfooter">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p>2019 © Copyright <a
                        href="#">event.</a> All
                    rights Reserved.</p>
            </div>
            <div class="col-md-6 col-sm-6">
                <a class="scrollup foot-abt"
                    href="#">Go to Top</a>
            </div>
        </div>
        <!-- End:Subfooter -->
    </footer>
    <!-- End:Footer Section 
========================================-->


    <!-- START : Switcher 
========================================-->
    <div class="demo_changer">
        <div class="demo-icon">
            <i class="fa fa-cog" aria-hidden="true" style="font-size: 28px;text-align: center;"></i>
        </div>
        <div class="form_holder">
            <div class="layout_styles">
                <p>CHOOSE YOUR COLOR</p>
                <div class="predefined_styles">
                    <a class="styleswitch" href="#"
                        rel="alternate">
                        <img alt="" src="{{ URL::to('home_res/images/c1.jpg') }}">
                    </a>
                    <a class="styleswitch" href="#"
                        rel="next">
                        <img alt="" src="{{ URL::to('home_res/images/c2.jpg') }}">
                    </a>
                    <a class="styleswitch" href="#"
                        rel="author">
                        <img alt="" src="{{ URL::to('home_res/images/c3.jpg') }}">
                    </a>
                    <a class="styleswitch" href="#"
                        rel="bookmark">
                        <img alt="" src="{{ URL::to('home_res/images/c4.jpg') }}">
                    </a>
                </div>
                <div class="line"></div>
                <span>
                    <a class="styleswitch"
                        href="#?default=true"
                        rel="tag">RESET STYLE</a>
                </span>
            </div>
        </div>
    </div>
    <!-- END Switcher 
========================================-->



    <!-- Scripts
========================================-->
    <!-- jquery -->
    {{-- <script src="{{ URL::to('home_res/js/jquery-1.12.1.min.js') }}" type="text/javascript"></script> --}}
    <!-- Modernizer -->
    <script src="{{ URL::to('home_res/js/modernizr.js') }}" type="text/javascript"></script>
    <!-- Bootstrap -->
    <script src="{{ URL::to('home_res/js/bootstrap.min.js') }}"></script>
    <!-- Use For Animation -->
    <script src="{{ URL::to('home_res/js/wow.min.js') }}"></script>
    <!-- Use For Map -->
    {{-- <script src="{{ URL::to('home_res/js/js') }}"></script> --}}
    <!-- Use For Map -->
    {{-- <script src="{{ URL::to('home_res/js/maps.js') }}"></script> --}}
    <!-- Use For carousel -->
    <script src="{{ URL::to('home_res/js/owl.carousel.min.js') }}"></script>
    <!-- Use For single Page Nav -->
    <script src="{{ URL::to('home_res/js/jquery.singlePageNav.min.js') }}"></script>
    <!--fswit Switcher   -->
    <script src="{{ URL::to('home_res/js/fswit.js') }}"></script>
    <!-- Use For nivo slider -->
    <script src="{{ URL::to('home_res/js/jquery.nivo.slider.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::to('home_res/js/nivo-home.min.js') }}" type="text/javascript"></script>
    <!-- Use For jquery Easing -->
    <script src="{{ URL::to('home_res/js/jquery.easing.min.js') }}"></script>
    <script src="{{ URL::to('home_res/js/jquery.easing.compatibility.min.js') }}"></script>
    <!-- Use For Dropdown Menu -->
    <script src="{{ URL::to('home_res/js/bootstrap-hover-dropdown.min.js') }}"></script>

    <!-- Custom Scripts
========================================-->
    <script src="{{ URL::to('home_res/js/main.js') }}"></script>
</body>

</html>
