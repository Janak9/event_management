@extends('home.header')
@section('slider')
<header id="page-top" class="blog-banner">
        <!-- Start: Header Content -->
        <div class="container" id="blog">
            <div class="row text-center wow fadeInUp animated" data-wow-delay="0.5s" style="visibility: visible;-webkit-animation-delay: 0.5s; -moz-animation-delay: 0.5s; animation-delay: 0.5s;">
                <div class="blog-header">
                    <!-- Headline Goes Here -->
                    <h3>Our Events <br><small> Events List</small> </h3>
                </div>
            </div>
            <!-- End: .row -->
        </div>
        <!-- End: Header Content -->
    </header>
@endsection
@section('main-content')
    <!-- Page content 
==================================================-->
    <div class="container-fluid no-padding page-content">
        <div class="container">
            <div class="row">
                <!-- Blog Area -->
                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 blog-area">
                    @forelse($packages as $pkg) 
                    <article class="blog-post-list wow fadeInLeft animated" data-wow-delay="0.3s" style="visibility: visible;-webkit-animation-delay: 0.3s; -moz-animation-delay: 0.3s; animation-delay: 0.3s;">
                        <div class="entry-cover">
                            <a href="eventDetail/{{$pkg->id}}"><img alt="blog-1" src="{{ URL::to('home_res/images/package/'.$pkg->image) }}">
                            </a>
                        </div>
                        <div class="entry-meta" style="top:-480px;padding:10px;">
                            <div id="socialHolder">
                                <div id="socialShare" class="btn-group share-group">
                                    <a data-toggle="dropdown" class="btn btn-info">
                                            <i class="fa fa-share-alt fa-inverse"></i>
                                    </a>
                                    <ul class="dropdown-menu" style="min-width: 0px;padding: 0px;background-color: transparent;">
                                        <li>
                                            <a target="blank" title="WhatsApp" data-action="share/whatsapp/share" href="https://api.whatsapp.com/send?text={{ url('/') }}/eventDetail/{{$pkg->id}}%0A%0A I inviting you for {{$pkg->pkg_name}}.%0A%0A Click above link to check package information." class="btn btn-whatsapp">
                                                <i class="fa fa-whatsapp"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a title="Email" class="btn btn-mail" target="blank" href="mailto:?subject=Invitation&body={{ url('/') }}/eventDetail/{{$pkg->id}}%0A%0A I inviting you for {{$pkg->pkg_name}}.%0A%0A Click above link to check package information.">
                                                <i class="fa fa-envelope"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <span><font style="font-size:18px">{{ $pkg->events->event_name }}</font></span> 
                        </div>
                        <div class="entry-meta">
                            <span class="entry-date"><i class="fa fa-clock-o"></i> <a href="#" title="Start Date & Time">{{ $pkg->start_datetime }}</a></span>
                            <span class="entry-author"><i class="fa fa-clock-o"></i> <a href="#" title="End Date & Time">{{ $pkg->end_datetime  }}</a></span>
                        </div>
                        
                        <h3 class="entry-title"><a href="eventDetail/{{$pkg->id}}" title="The big leagues our turn Straightnin">{{$pkg->pkg_name}}</a></h3>
                        <div class="entry-content">
                            <p style="width: 100%;">{{ $pkg->description}} </p>
                        </div>
                        <div class="entry-meta-bottom">
                            <div class="blog-social">
                                <a href="eventDetail/{{$pkg->id}}"><b>Registration End Date-Time : {{date('Y-m-d',strtotime(@$pkg->registration_end_date))}}</b></a>
                                   
                            </div>
                            <span class="entry-category"> Comments: <a href="eventDetail/{{$pkg->id}}" title="Comment" ><i class="fa fa-commenting" style="font-size:17px; color: darkgreen;"></i> <font style="font-size:17px; color: darkgreen;">  {{ $pkg->count_review($pkg->id) }}</font></a></span>
                            <a href="eventDetail/{{$pkg->id}}" class="btn btn-chos hvr-shutter-in-horizontal" title="read">read more</a>
                        </div>
                    </article> 
                    @empty
                    <div class="entry-content">
                        <p><strong><h3 style="color:red;"><i class="fa fa-meh-o" style="margin-right:10px;font-size: 38px;"></i><font style="font-size: 30px">O</font><font>ops...!!</font></strong>
                        Not Found</p>
                    </div>
                    @endforelse
                    {{-- end of events repeated --}}

                    <div style="">
                         {{ $packages->appends(['search_type' =>  Request::get('search_type'),'search' => Request::get('search') ])->links() }} 
                    </div>
                </div>
                <!--/ Blog Area -->
                <!-- Widget Area -->
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 widget-area">
                    <!-- Widget Search -->
                    <form method="get" action="{{ route('eventsearch') }}">
                        <aside class="widget widget-search">
                        <!-- input-group -->
                        <div class="input-group" style="width:100%;">
                            <select class="form-control col-lg-3" name="search_type">
                                <option value="-1" disabled>Search By</option>
                                <option value="1">Events Name</option>
                                <option value="2" @if (app('request')->input('search_type') == 2)
                                    selected
                                @endif>Package Details</option>
                            </select>
                        </div> 
                        <div class="input-group">
                        <input class="form-control" placeholder="Search" type="text" name="search" value="{{ app('request')->input('search') }}" title="Search By Either Event Name or Either Package Details Wise(Like Price Per Person,City,Date Wise..">
                            <span class="input-group-btn">
								<button type="submit"><i class="fa fa-search"></i></button>
							</span>
                        </div> 
                        </aside>
                    </form>
                        <!-- /input-group -->
                        {{-- <form method="post" action="/mysearch">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input class="form-control" placeholder="Search" type="text">
                                    </div>
                                   		<div class="col-md-6">
					                        <button class="btn btn-success">Search</button>
				                    </div>
                                </div>
                        </form> --}}
                    
                    <!-- Widget Search /- -->
                    <!-- Post Categories -->
                    {{-- <aside class="widget widget-post-categories">
                        <h3 class="widget-title">Post Categories</h3>
                        <ul class="categories-type">
                            <li>
                                <a href="#" title="Business">Business</a>
                            </li>
                            <li>
                                <a href="#" title="Wordpress">Wordpress</a>
                            </li>
                            <li>
                                <a href="#" title="Theme Forest">Theme Forest</a>
                            </li>
                            <li>
                                <a href="#" title="Web Developement">Web Developement</a>
                            </li>
                            <li>
                                <a href="#" title="Statistics">Statistics</a>
                            </li>
                        </ul>
                    </aside> --}}
                    <!-- Post Categories /- -->
                    <!-- Recent Post -->
                    {{-- <aside class="widget wiget-recent-post">
                        <h3 class="widget-title">Recent Post</h3>
                        <div class="recent-post-box">
                            <div class="recent-img">
                                <a href="http://ttheme.website/tidytheme/Jonvent/preview/single-blog.html"><img alt="recent-1" src="eventlist_files/css/recent-1.jpg">
                                </a>
                            </div>
                            <div class="recent-title">
                                <a href="http://ttheme.website/tidytheme/Jonvent/preview/single-blog.html">This is latest Pic</a>
                                <p>25th Dec 2015</p>
                            </div>
                        </div>
                        <div class="recent-post-box">
                            <div class="recent-img">
                                <a href="http://ttheme.website/tidytheme/Jonvent/preview/single-blog.html"><img alt="recent-1" src="eventlist_files/css/recent-2.jpg">
                                </a>
                            </div>
                            <div class="recent-title">
                                <a href="http://ttheme.website/tidytheme/Jonvent/preview/single-blog.html">This is latest Video</a>
                                <p>25th Dec 2015</p>
                            </div>
                        </div>
                        <div class="recent-post-box">
                            <div class="recent-img">
                                <a href="http://ttheme.website/tidytheme/Jonvent/preview/single-blog.html"><img alt="recent-1" src="eventlist_files/css/recent-3.jpg">
                                </a>
                            </div>
                            <div class="recent-title">
                                <a href="http://ttheme.website/tidytheme/Jonvent/preview/single-blog.html">Weekly Celebrate</a>
                                <p>25th Dec 2015</p>
                            </div>
                        </div>
                    </aside> --}}
                    <!-- Recent Post /- -->
                    {{-- <aside class="widget widget-post-categories widget-archives">
                        <h3 class="widget-title">Archives</h3>
                        <ul class="categories-type">
                            <li>
                                <a href="#" title="Jan-Feb 2015">Jan-Feb 2015</a>
                            </li>
                            <li>
                                <a href="#" title="Jan-Feb 2015">Jan-Feb 2015</a>
                            </li>
                            <li>
                                <a href="#" title="Mar-Apr 2015">Mar-Apr 2015</a>
                            </li>
                        </ul>
                    </aside> --}}
                    {{-- <!-- Archives /- -->
                    <!-- Widget Tags -->
                    <aside class="widget widget-tags">
                        <h3 class="widget-title">Top Tags</h3><a href="#" title="Install">Install</a> <a href="#" title="Design">Design</a> <a href="#" title="Video">Video</a> <a href="#" title="Branding">Branding</a> <a href="#" title="Pakaging">Pakaging</a>
                    </aside>
                    <!-- Widget Tags /- -->
                    <!-- Archives --> --}}

                </div>
                <!-- Widget Area /- -->
            </div>
        </div>
        <!-- Container /- -->
    </div>
    <!-- End: Page-content  
==================================================-->
@endsection