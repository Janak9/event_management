@extends('home.header')

@section('main-content')
<section class="agenda-section text-center" >
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>    
            <div class="col-md-8">
                <div class="card">
                    <h1>Edit Profile</h1>
                    
                    @isset($user)
                    {{-- {{ dd($user) }} --}}
                    {{-- <div class="card-header" style="font-size:30px;text-align:center">{{ __('Register') }}</div> --}}
                    <form method="POST" action="{{ route('profile-edit-post') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="fname" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>
                            <div class="col-md-6">
                                <input id="fname" type="text" placeholder="FirstName" class="form-control @error('fname') is-invalid @enderror" name="fname" value="{{ old('fname', @$user->fname) }}" maxlength="15" pattern="[a-zA-Z]+" required autocomplete="fname" autofocus >
                                @error('fname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                                <label for="lname" class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>
                                <div class="col-md-6">
                                    <input id="lname" type="text" placeholder="LastName" class="form-control @error('lname') is-invalid @enderror" name="lname" value="{{ old('lname', @$user->lname) }}" maxlength="15" pattern="[a-zA-Z]+" required autocomplete="lname" autofocus>
                                    @error('lname')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                            <div class="col-md-6">
                                <input id="email" type="email" placeholder="E-mail" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email', @$user->email) }}" maxlength="50" required autocomplete="email" readonly>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="dob" class="col-md-4 col-form-label text-md-right">{{ __('DateOfBirth') }}</label>
                            <div class="col-md-6">
                                <input id="dob" type="date" placeholder="Date Of Birth" class="form-control @error('dob') is-invalid @enderror" name="dob" value="{{ old('dob', @$user->dob) }}"  autocomplete="dob" autofocus>
                                @error('dob')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>
                            <div class="col-md-6">
                                <textarea id="address" placeholder="Address" class="form-control @error('address') is-invalid @enderror" name="address" maxlength="70" autocomplete="address">{{ old('address', @$user->address) }}</textarea>
                                @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="country" class="col-md-4 col-form-label text-md-right">{{ __('Country') }}</label>
                            <div class="col-md-6">
                                <input id="country" type="text" placeholder="Country" class="form-control @error('country') is-invalid @enderror" name="country" maxlength="20" pattern="[a-zA-Z ]+" value="{{ old('country', @$user->country) }}" autocomplete="country" autofocus>
                                @error('country')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="state" class="col-md-4 col-form-label text-md-right">{{ __('State') }}</label>
                            <div class="col-md-6">
                                <input id="state" type="text" placeholder="State" class="form-control @error('state') is-invalid @enderror" name="state" maxlength="20" pattern="[a-zA-Z ]+" value="{{ old('state', @$user->state) }}" autocomplete="state" autofocus>
                                @error('state')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="city" class="col-md-4 col-form-label text-md-right">{{ __('City') }}</label>
                            <div class="col-md-6">
                                <input id="city" type="text" placeholder="City" class="form-control @error('city') is-invalid @enderror" name="city" maxlength="20" value="{{ old('city', @$user->city) }}" pattern="[a-zA-Z ]+" autocomplete="city" autofocus>
                                @error('city')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="contact_no" class="col-md-4 col-form-label text-md-right">{{ __('Contact No') }}</label>
                            <div class="col-md-6">
                                <input id="contact_no" type="text" placeholder="Contact No" class="form-control @error('contact_no') is-invalid @enderror" name="contact_no" maxlength="17"  pattern="[0-9]+" value="{{ old('contact_no', @$user->contact_no) }}" required autocomplete="contact_no" autofocus>
                                @error('contact_no')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="payumoney_key" class="col-md-4 col-form-label text-md-right">{{ __('PayUMoney Key') }}</label>
                            <div class="col-md-6">
                                <input id="payumoney_key" type="text" placeholder="PayUMoney Key" class="form-control @error('payumoney_key') is-invalid @enderror" name="payumoney_key" maxlength="15"  value="{{ old('payumoney_key', @$user->payumoney_key) }}" autocomplete="payumoney_key" autofocus>
                                @error('payumoney_key')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="payumoney_salt" class="col-md-4 col-form-label text-md-right">{{ __('PayUMoney Salt') }}</label>
                            <div class="col-md-6">
                                <input id="payumoney_salt" type="text" placeholder="PayUMoney Salt" class="form-control @error('payumoney_salt') is-invalid @enderror" name="payumoney_salt" maxlength="15" value="{{ old('payumoney_salt', @$user->payumoney_salt) }}" autocomplete="payumoney_salt" autofocus>
                                @error('payumoney_salt')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="profile" class="col-md-4 col-form-label text-md-right">{{ __('Profile') }}</label>
                            <div class="col-md-6">
                                <input id="profile" type="file" class="@error('profile') is-invalid @enderror" name="profile" value="{{$user->profile}}" autocomplete="profile" autofocus onchange="loadImage(event, 'preview')">
                                @error('profile')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div><br>
                        <img id="preview" src="home_res/images/user/{{$user->profile}}" height="200px" width="200px" />
                        <div class="form-group row mb-0">
                            <div class="col-md-12 offset-md-4">
                                <input type="reset" class="btn btn-primary" value="Reset" name="reset" />
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Edit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    @endisset
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
