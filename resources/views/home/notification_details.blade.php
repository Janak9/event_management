@extends('home.header')

@section('main-content')
<section class="agenda-section text-center" >
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>    
            <div class="col-md-8">
                <div class="card">
                <h1>Notification Details</h1>
                <center>
                    <table border="2">
                        <tr>
                            <td colspan="2"><a href="/eventDetail/{{$package->id}}"><img src="{{ URL::to('home_res/images/package/'.$package->image) }}" style="width:100%;max-height:300px;" /></a></td>
                        </tr>
                        <tr>
                            <td>Package Name</td>
                            <td><a href="/eventDetail/{{$package->id}}">{{ $package->pkg_name }}</a></td>
                        </tr>
                        <tr>
                            <td>Package Type</td>
                            <td>{{ $package->type }}</td>
                        </tr>
                        <tr>
                            <td>Package Date</td>
                            <td>{{ $package->start_datetime }}</td>
                        </tr>
                        <tr>
                            <td>User Name</td>
                            <td>{{ $notification->from_user->full_name }}</td>
                        </tr>
                        <tr>
                            <td>User Email</td>
                            <td>{{ $notification->from_user->email }}</td>
                        </tr>
                        <tr>
                            <td>User Contact No</td>
                            <td>{{ $notification->from_user->contact_no }}</td>
                        </tr>
                        <tr>
                            <td>Total Person</td>
                            <td>{{ $notification->pkg_booking->total_persons }}</td>
                        </tr>
                        <tr>
                            <td>Total Amount</td>
                            <td>{{ $notification->pkg_booking->total_amount }}</td>
                        </tr>
                        <tr>
                            <td>Deposite</td>
                            <td>{{ $notification->pkg_booking->deposite }}</td>
                        </tr>
                        <tr>
                            <td>Payment Status</td>
                            <td>@if ($notification->pkg_booking->payment_status == 0)
                                Pending
                            @else
                                Paid
                            @endif</td>
                        </tr>
                        <tr>
                            <td>Your Status</td>
                            <td>@if ($notification->pkg_booking->status == 0)
                                Pending
                            @elseif ($notification->pkg_booking->status == 1)
                                Accepted
                            @elseif ($notification->pkg_booking->status == 2)
                                Denied
                            @elseif ($notification->pkg_booking->status == 3)
                                Cancelled by user
                            @endif</td>
                        </tr>
                        @if ($notification->pkg_booking->payment_status == 0 && ($notification->pkg_booking->status != 3))
                            <tr>
                                <td colspan="2" align="center">
                                    <form method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group row mb-0">
                                            <div class="col-md-12 offset-md-4">
                                                <button type="submit" class="btn btn-primary" name="btn_submit" value="1">Accept</button>
                                                <button type="submit" class="btn btn-primary" name="btn_submit" value="2">Deny</button>
                                            </div>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endif
                    </table>
                </center>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
