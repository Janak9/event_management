@extends('home.header')
@section('main-content')
<!-- Start: Header Section
=============================================-->
    <!-- header -->
    <header id="page-top" class="blog-banner">
        <!-- Start: Header Content -->
        <div class="container" id="blog">
            <div class="row text-center wow fadeInUp" data-wow-delay="0.5s">
                <div class="blog-header">
                    <!-- Headline Goes Here -->
                    <h3>Contact Us<br><small> Home - Contact</small> </h3>
                </div>
            </div>
            <!-- End: .row -->
        </div>
        <!-- End: Header Content -->
    </header>
    <!--/. header -->

    <!-- navigation bar -->
    <nav class="navbar navbar-default" id="aboutSectionTestPurposes">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button class="navbar-toggle" data-target=".js-navbar-collapse" data-toggle="collapse" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand scroll" href="index.html">
                    <img alt="JonVent" class="" src="images/logo.jpg">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse js-navbar-collapse " id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="dropdown dropdown-large">
                        <a class="dropdown-toggle" data-delay="10" data-hover="dropdown" data-toggle="dropdown" href="#">Home</a>
                        <!-- Menu Name-->
                    </li>
                    <li class="dropdown dropdown-large">
                        <a class="dropdown-toggle" data-delay="10" data-hover="dropdown" data-toggle="dropdown" href="#">Page</a>
                        <!-- Menu Name-->
                        <ul class="dropdown-menu dropdown-menu-large row col-lg-6 col-sm-7">
                            <li class="col-lg-4 col-sm-4">
                                <ul>
                                    <!-- Sub Menu Name-->
                                    <li class="dropdown-header">All Pages</li>
                                    <li>
                                        <a href="blog.html">Blog Page</a>
                                    </li>
                                    <li>
                                        <a href="single-blog.html">single blog</a>
                                    </li>
                                    <li>
                                        <a href="gallery.html">gallery</a>
                                    </li>
                                    <li>
                                        <a href="accommodation.html">accommodation</a>
                                    </li>
                                    <li>
                                        <a href="about.html">about Page</a>
                                    </li>
                                    <li>
                                        <a href="contact.html">Contact Page</a>
                                    </li>
                                    <li>
                                        <a href="single-gallery.html">single gallery</a>
                                    </li>
                                </ul>
                            </li>
                            <!-- Sub Menu Name-->
                            <li class="col-lg-4 col-sm-4">
                                <ul>
                                    <li class="dropdown-header">Feature</li>
                                    <li>
                                        <a href="#">Basic example</a>
                                    </li>
                                    <li>
                                        <a href="#">Sizing</a>
                                    </li>
                                    <li>
                                        <a href="#">Checkboxes</a>
                                    </li>
                                    <li>
                                        <a href="#">Tabs</a>
                                    </li>
                                    <li>
                                        <a href="#">Pills</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="col-lg-4 col-sm-4">
                                <ul>
                                    <!-- Sub Menu Name-->
                                    <li class="dropdown-header">Element </li>
                                    <li>
                                        <a href="#">navbar</a>
                                    </li>
                                    <li>
                                        <a href="#">Buttons</a>
                                    </li>
                                    <li>
                                        <a href="#">Text</a>
                                    </li>
                                    <li>
                                        <a href="#">links</a>
                                    </li>
                                    <li>
                                        <a href="#">alignment</a>
                                    </li>
                                    <li>
                                        <a href="#">top</a>
                                    </li>
                                    <li>
                                        <a href="#">bottom</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown dropdown-large">
                        <a class="dropdown-toggle" data-delay="10" data-hover="dropdown" data-toggle="dropdown" href="#">About </a>
                        <!-- Menu Name-->
                    </li>
                    <li class="dropdown dropdown-large">
                        <a class="dropdown-toggle" data-delay="10" data-hover="dropdown" data-toggle="dropdown" href="#">Agenda</a>
                        <!-- Menu Name-->
                    </li>
                    <li class="dropdown dropdown-large">
                        <a class="dropdown-toggle" data-delay="10" data-hover="dropdown" data-toggle="dropdown" href="#">Speaker</a>
                        <!-- Menu Name-->
                    </li>
                    <li class="dropdown dropdown-large">
                        <a class="dropdown-toggle" data-delay="10" data-hover="dropdown" data-toggle="dropdown" href="#">Blog</a>
                        <!-- Menu Name-->
                    </li>
                    <li class="dropdown dropdown-large">
                        <a class="dropdown-toggle" data-delay="10" data-hover="dropdown" data-toggle="dropdown" href="#">Contact</a>
                        <!-- Menu Name-->
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    <!--/. navigation bar -->
    <div class="header" id="home"></div>
    <!--End: Header Section
==================================================-->

    <!-- Start:Contact Section 
==================================================-->
    <section class="contact-section" id="contact">
        <div class="container">
            <div class="title-header">
                <span>01</span>
                <h3>Get In Tuch</h3>
                <p>There are many variations of passages of Lorem Ipsum available There are many variations of passages of Lorem Ipsum available There are many variations of passages of Lorem Ipsum available</p>
            </div>
            <!-- End: Heading -->
            <div class="row">
                <!--  Content  -->
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="col-lg-5 col-sm-5 col-xs-12 contact-info">
                        <!-- Start: Heading -->
                        <div class="contact-addrs">
                            <h3 class="small">Address Information</h3>
                            <p>Email :myemail@mail.com</p>
                        </div>
                        <div class="contact-addrs">
                            <h3 class="small">New York</h3>
                            <p>Address :New York, Glory Street NY</p>
                        </div>
                        <div class="contact-addrs">
                            <h3 class="small">France</h3>
                            <p>Address :France, 35000 Glory Street</p>
                        </div>
                    </div>
                    <!-- End:contact-info  -->
                    <div class="contact-form-warper">
                        <div class="col-lg-7 col-sm-7 col-xs-12 contact-warper">
                            <!--  Contact Form  -->
                            <div class="contact-form">
                                <form action="mailer.php" id="contactForm" method="post" name="contactForm">
                                    <div class="col-lg-5">
                                        <div class="form-group col-lg-12">
                                            <input class="form-control" id="Name" name="Name" placeholder="Name :" type="text">
                                        </div>
                                        <div class="form-group col-lg-12">
                                            <input class="form-control" id="Email" name="Email" placeholder="Email :" type="text">
                                        </div>
                                        <div class="form-group col-lg-12">
                                            <input class="form-control" id="Tel" name="Tel" placeholder="Number :" type="text">
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <div class="form-group col-lg-12">
                                            <textarea class="form-control" id="Message" name="Message" placeholder="Message :"></textarea>
                                        </div>
                                    </div>
                                    <input class="submit-button btn" name="submit" type="submit" value="Submit">
                                </form>
                            </div>
                            <!-- End:Contact Form  -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- row /- -->
        </div>
        <!-- container /- -->
    </section>
    <!-- End:Contact Section 
========================================-->
@endsection
   