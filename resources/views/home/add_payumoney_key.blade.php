@extends('home.header')

@section('main-content')
<section class="agenda-section text-center" >
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>    
            <div class="col-md-8">
                <div class="card">
                <h1>Add PayUMoney Key</h1>
                    <form method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="payumoney_key" class="col-md-4 col-form-label text-md-right">{{ __('PayUMoney Key') }}</label>
                            <div class="col-md-6">
                                <input id="payumoney_key" type="text" class="form-control @error('payumoney_key') is-invalid @enderror" name="payumoney_key" required autocomplete="payumoney_key" autofocus>
                                @error('payumoney_key')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="payumoney_salt" class="col-md-4 col-form-label text-md-right">{{ __('PayUMoney Salt') }}</label>
                            <div class="col-md-6">
                                <input id="payumoney_salt" type="text" class="form-control @error('payumoney_salt') is-invalid @enderror" name="payumoney_salt" required autocomplete="payumoney_salt" autofocus>
                                @error('payumoney_salt')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <br>
                        <div class="form-group row mb-0">
                            <div class="col-md-12 offset-md-4">
                                <input type="reset" class="btn btn-primary" value="Reset" name="reset" />
                                <button type="submit" class="btn btn-primary" name="btn_submit">
                                    {{ __('Edit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    
                    <div class="" style="background-color: #eee;padding-top: 34px;
                    padding-bottom: 17px;padding-left: 1px;">
                        <h4><b>Translate Language to</b></h4>
                        <center>
                        <div class="form-group" style="width: 30%;">
                            <select onchange="change_lang(this.value)" class="form-control">
                                <option disabled>Select Languages</option>
                                <option value="en">English</option>
                                <option value="guj">Gujarati</option>
                                <option value="fr">French</option>
                            </select>
                        </div>
                        </center>
                        <div id="localization_data">
                            <h3><b>Instruction For Add PayUMoney Key</b></h3><br>
                            <strong>Payment Only Through PayUmoney.</strong><br>
                            <small><em>You Must Have To Create An Account In PayUmoney.</em></small><br><br>
                            <b>Follow The Following Step To Create Account And Get Merchant Key and Salt :</b><br><br>
        
                            <b>Step 1 :</b><p> Click Here <a href="https://www.payumoney.com/">PayUmoney</a></p>
                            <b>Step 2 :</b><p> Click On Create Acount And Enter Your Email And Click On Next.</p>
                            <b>Step 3 :</b><p> Select "Become A Merchant" Option. </p>
                            <b>Step 4 :</b><p> Now Fill Your Information. And Press Next.</p>
                            <b>Step 5 :</b><p> Enter OTP You Recieved.</p>
                            <b>Step 6 :</b><p> Now Click On "Start Integration" Option.</p>
                            <b>Step 7 :</b><p> Their Is Merchant Key And Merchant Salt Are Display simply Copy It And Paste It.</p>
                            <br><br>
                            <b><p style="color:red;">Please Check Your Key And Salt That You Entered.</p>
                                <p style="color:red;"> If You Enter Wrong Key Then You Can't Get Payment. It's Your Responsiblity To Check It.</p></b>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    function change_lang(lang){
        $.ajax({
            method: 'get',
            url: '/localization/'+lang,
            success: function(response){
                $('#localization_data').html(response);
            }
        });
    }
</script>
@endsection
