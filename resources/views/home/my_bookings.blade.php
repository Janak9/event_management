@extends('home.header')

@section('main-content')
<section class="agenda-section text-center" >
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>    
            <div class="col-md-8">
                <div class="card">
                    <h1>My Bookings</h1>
                    @foreach($all_booking as $booking )
                        {{-- <div class="card-header" style="font-size:30px;text-align:center">{{ __('Register') }}</div> --}}
                        <div style="clear:both;"></div>
                        <div class="row entry-content" style="text-align:left;border: 1px outset;box-shadow: 3px 4px 5px;background: #f9f4f4;">
                            <div style="width: 30%;float: left;margin: 10px;height: 160px;">
                                <a href="eventDetail/{{$booking->pid}}"><img src="{{ URL::to('home_res/images/package/'.@$booking->packages->image) }}" style="height:100%;width:100%;" /></a>
                            </div>
                            <div style="margin-left: 20px;width: 63%;float: left;">
                                <h5>Package Name : <a href="eventDetail/{{$booking->pid}}">{{@$booking->packages->pkg_name}}</a></h5>
                                <h5>Total Person : {{$booking->total_persons}}</h5>
                                <h5>Total Amount : {{$booking->total_amount}}</h5> 
                                <h5>Payable Amount (Deposite) : {{$booking->deposite}}</h5> 
                                <div>
                                    @if (($booking->status==0 && $booking->payment_status==0) || ($booking->status==1 && $booking->payment_status==0))
                                        <a href="/booking/cancel/{{$booking->id}}"><button class="btn btn-orange" style="background:#FF542E;">Cancel</button></a>
                                    @endif
                                    @if($booking->status==0 && $booking->payment_status==0)
                                        <span class='label label-default'>Pending</span>
                                    @elseif ($booking->status==1 && $booking->payment_status==0)
                                        {{-- $pay=$booking->deposite 
                                        $client_id=$booking->uid
                                        $tbl_id=$booking->id  --}}
                                        <a target='blank' class='btn btn-info' href="/payment/{{ $booking->uid }}/{{ $booking->id }}">Make Payment</a>
                                        <span class='label label-primary'>Accepted</span>
                                    @elseif ($booking->status==2 && $booking->payment_status==0)
                                        <span class='label label-danger'>Denied</span>
                                    @elseif ($booking->status==3 && $booking->payment_status==0)
                                        <span class='label label-warning'>Cancelled</span>
                                    @elseif ($booking->status==1 && $booking->payment_status==1)
                                        <span class='label label-success'>Complete</span>
                                        <a href="/invoice/{{$booking->id}}" class='btn btn-info' target="blank">View Invoice</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <br>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
