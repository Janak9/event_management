@extends('home.header')
@section('main-content')
<!-- Start: Header Section
=============================================-->
    <!-- header -->
    <header id="page-top" class="blog-banner">
        <!-- Start: Header Content -->
        <div class="container" id="blog">
            <div class="row text-center wow fadeInUp animated" data-wow-delay="0.5s" style="visibility: visible;-webkit-animation-delay: 0.5s; -moz-animation-delay: 0.5s; animation-delay: 0.5s;">
                <div class="blog-header">
                    <!-- Headline Goes Here -->
                    <h3>About Us <br><small> Home - About</small> </h3>
                </div>
            </div>
            <!-- End: .row -->
        </div>
        <!-- End: Header Content -->
    </header>
    <!--/. header -->

    <!-- navigation bar -->
    {{-- <nav class="navbar navbar-default" id="aboutSectionTestPurposes" style="z-index: 0;">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button class="navbar-toggle" data-target=".js-navbar-collapse" data-toggle="collapse" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand scroll" href="http://ttheme.website/tidytheme/Jonvent/preview/index.html">
                    <img alt="JonVent" class="" src="./jonvent - Multipurpose Template_files/logo.jpg">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse js-navbar-collapse " id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="dropdown dropdown-large">
                        <a class="dropdown-toggle" data-delay="10" data-hover="dropdown" data-toggle="dropdown" href="http://ttheme.website/tidytheme/Jonvent/preview/about.html#">Home</a>
                        <!-- Menu Name-->
                    </li>
                    <li class="dropdown dropdown-large">
                        <a class="dropdown-toggle" data-delay="10" data-hover="dropdown" data-toggle="dropdown" href="http://ttheme.website/tidytheme/Jonvent/preview/about.html#">Page</a>
                        <!-- Menu Name-->
                        <ul class="dropdown-menu dropdown-menu-large row col-lg-6 col-sm-7">
                            <li class="col-lg-4 col-sm-4">
                                <ul>
                                    <!-- Sub Menu Name-->
                                    <li class="dropdown-header">All Pages</li>
                                    <li>
                                        <a href="http://ttheme.website/tidytheme/Jonvent/preview/blog.html">Blog Page</a>
                                    </li>
                                    <li>
                                        <a href="http://ttheme.website/tidytheme/Jonvent/preview/single-blog.html">single blog</a>
                                    </li>
                                    <li>
                                        <a href="http://ttheme.website/tidytheme/Jonvent/preview/gallery.html">gallery</a>
                                    </li>
                                    <li>
                                        <a href="http://ttheme.website/tidytheme/Jonvent/preview/accommodation.html">accommodation</a>
                                    </li>
                                    <li>
                                        <a href="http://ttheme.website/tidytheme/Jonvent/preview/about.html">about Page</a>
                                    </li>
                                    <li>
                                        <a href="http://ttheme.website/tidytheme/Jonvent/preview/contact.html">Contact Page</a>
                                    </li>
                                    <li>
                                        <a href="http://ttheme.website/tidytheme/Jonvent/preview/single-gallery.html">single gallery</a>
                                    </li>
                                </ul>
                            </li>
                            <!-- Sub Menu Name-->
                            <li class="col-lg-4 col-sm-4">
                                <ul>
                                    <li class="dropdown-header">Feature</li>
                                    <li>
                                        <a href="http://ttheme.website/tidytheme/Jonvent/preview/about.html#">Basic example</a>
                                    </li>
                                    <li>
                                        <a href="http://ttheme.website/tidytheme/Jonvent/preview/about.html#">Sizing</a>
                                    </li>
                                    <li>
                                        <a href="http://ttheme.website/tidytheme/Jonvent/preview/about.html#">Checkboxes</a>
                                    </li>
                                    <li>
                                        <a href="http://ttheme.website/tidytheme/Jonvent/preview/about.html#">Tabs</a>
                                    </li>
                                    <li>
                                        <a href="http://ttheme.website/tidytheme/Jonvent/preview/about.html#">Pills</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="col-lg-4 col-sm-4">
                                <ul>
                                    <!-- Sub Menu Name-->
                                    <li class="dropdown-header">Element </li>
                                    <li>
                                        <a href="http://ttheme.website/tidytheme/Jonvent/preview/about.html#">navbar</a>
                                    </li>
                                    <li>
                                        <a href="http://ttheme.website/tidytheme/Jonvent/preview/about.html#">Buttons</a>
                                    </li>
                                    <li>
                                        <a href="http://ttheme.website/tidytheme/Jonvent/preview/about.html#">Text</a>
                                    </li>
                                    <li>
                                        <a href="http://ttheme.website/tidytheme/Jonvent/preview/about.html#">links</a>
                                    </li>
                                    <li>
                                        <a href="http://ttheme.website/tidytheme/Jonvent/preview/about.html#">alignment</a>
                                    </li>
                                    <li>
                                        <a href="http://ttheme.website/tidytheme/Jonvent/preview/about.html#">top</a>
                                    </li>
                                    <li>
                                        <a href="http://ttheme.website/tidytheme/Jonvent/preview/about.html#">bottom</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown dropdown-large">
                        <a class="dropdown-toggle" data-delay="10" data-hover="dropdown" data-toggle="dropdown" href="http://ttheme.website/tidytheme/Jonvent/preview/about.html#" aria-expanded="false">About </a>
                        <!-- Menu Name-->
                    </li>
                    <li class="dropdown dropdown-large">
                        <a class="dropdown-toggle" data-delay="10" data-hover="dropdown" data-toggle="dropdown" href="http://ttheme.website/tidytheme/Jonvent/preview/about.html#" aria-expanded="false">Agenda</a>
                        <!-- Menu Name-->
                    </li>
                    <li class="dropdown dropdown-large">
                        <a class="dropdown-toggle" data-delay="10" data-hover="dropdown" data-toggle="dropdown" href="http://ttheme.website/tidytheme/Jonvent/preview/about.html#" aria-expanded="false">Speaker</a>
                        <!-- Menu Name-->
                    </li>
                    <li class="dropdown dropdown-large">
                        <a class="dropdown-toggle" data-delay="10" data-hover="dropdown" data-toggle="dropdown" href="http://ttheme.website/tidytheme/Jonvent/preview/about.html#">Blog</a>
                        <!-- Menu Name-->
                    </li>
                    <li class="dropdown dropdown-large">
                        <a class="dropdown-toggle" data-delay="10" data-hover="dropdown" data-toggle="dropdown" href="http://ttheme.website/tidytheme/Jonvent/preview/about.html#">Contact</a>
                        <!-- Menu Name-->
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav> --}}
    <!--/. navigation bar -->
    {{-- <div class="header" id="home"></div> --}}
    <!--End: Header Section
==================================================-->


    <!-- Start:About Section 
==================================================-->
    <section class="about-section" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-sm-8 col-xs-8 wow fadeInRight animated" data-wow-delay="0.3s" style="visibility: visible;-webkit-animation-delay: 0.3s; -moz-animation-delay: 0.3s; animation-delay: 0.3s;">
                    <!--About Left-->
                    <div class="about-left">
                        <span>01</span>
                        <h3>About The Event</h3>
                        <p>The Online Event Management System is used for Online Event Package Creation and Booking. So user can save the time and book package at home easily.
                            In our project, user can create their own event package accordong to their choice.
                            User can also Book their event package according to his availability (like date, time, number of seat etc.).
                            We can provide two types of package : 1) public  2) private
                            Public events can any one join.
                            Private events can only booked when user who create it will accept the booking request. 
                            </p>
                        {{-- <a class="btn btn-chos hvr-shutter-in-horizontal" href="http://ttheme.website/tidytheme/Jonvent/preview/about.html#">Read More <i class="fa fa-long-arrow-right"></i></a> --}}
                    </div>
                    <!--/About Left-->
                </div>
                <!--/.col-lg-6 col-md-4 col-sm-5-->
            </div>
            <!--/.row-->
        </div>
        <!--/container-->
    </section>
    <!-- End:About Section 
==================================================-->

    <!-- Start: Achieve  Section 
==================================================-->
    <section class="achieve-section" id="achieve-section">
        <!-- container -->
        <div class="container">
            <!-- col-lg-3 col-md-3 col-sm-6 col-xs-6 -->
            <div class="col-lg-3 col-sm-3 col-xs-6">
                <div class="col-lg-4 icon-lay">
                    <i class="fa fa-edge" aria-hidden="true"></i>
                </div>
                <div class="col-lg-8">
                    <h2 class="stat-count count">{{ $total_events }}</h2>
                    <h5>Events</h5>
                </div>
            </div>
            <!-- col-lg-3 col-md-3 col-sm-6 col-xs-6 /- -->
            <!-- col-lg-3 col-md-3 col-sm-6 col-xs-6 -->
            <div class="col-lg-3 col-sm-3 col-xs-6">
                <div class="col-lg-4 icon-lay">
                    <i class="fa fa-suitcase" aria-hidden="true"></i>
                </div>
                <div class="col-lg-8">
                    <h2 class="stat-count count">{{ $total_packages }}</h2>
                    <h5>Packages</h5>
                </div>
            </div>
            <!-- col-lg-3 col-md-3 col-sm-6 col-xs-6 /- -->
            <!-- col-lg-3 col-md-3 col-sm-6 col-xs-6 -->
            <div class="col-lg-3 col-sm-3 col-xs-6">
                <div class="col-lg-4 icon-lay">
                    <i class="fa fa-users" aria-hidden="true"></i>
                </div>
                <div class="col-lg-8">
                    <h2 class="stat-count count">{{ $total_users }}</h2>
                    <h5>Users</h5>
                </div>
            </div>
            <!-- col-lg-3 col-md-3 col-sm-6 col-xs-6 /- -->
            <!-- col-lg-3 col-md-3 col-sm-6 col-xs-6 -->
            <div class="col-lg-3 col-sm-3 col-xs-6">
                <div class="col-lg-4 icon-lay">
                    <i class="fa fa-credit-card" aria-hidden="true"></i>
                </div>
                <div class="col-lg-8">
                    <h2 class="stat-count count">{{ $total_payments }}</h2>
                    <h5>Bookings</h5>
                </div>
            </div>
            <!-- col-lg-3 col-md-3 col-sm-6 col-xs-6 /- -->
        </div>
        <!-- container /- -->
    </section>
    <!-- End: Achive Section 
==================================================-->

	<!-- START : Switcher 
========================================-->
	<div class="demo_changer">
		<div class="demo-icon">
			<i class="icon_cog"></i>
		</div>
		<div class="form_holder">
			<div class="layout_styles">
				<p>LAYOUT STYLE</p>
				<ul>
					<li style="color: #D4AF68;font-weight: 700;font-size: 15px;letter-spacing: 3px;">Multipurpose</li>
					<li>
						<a href="http://ttheme.website/tidytheme/Jonvent/preview/index.html"> Home Page 1</a>
					</li>
					<li>
						<a href="http://ttheme.website/tidytheme/Jonvent/preview/index-slideshow.html"> Home Page 2</a>
					</li>
					<li>
						<a href="http://ttheme.website/tidytheme/Jonvent/preview/index-video.html"> Home Page 3</a>
					</li>
					
					<li style="color: #D4AF68;font-weight: 700;font-size: 15px;letter-spacing: 3px;">Landing Page</li>
					<li>
						<a href="http://ttheme.website/tidytheme/Jonvent/preview/index-landing.html"> Home Page 1</a>
					</li>
					<li>
						<a href="http://ttheme.website/tidytheme/Jonvent/preview/index-landing-slidshow.html"> Home Page 2</a>
					</li>
					<li>
						<a href="http://ttheme.website/tidytheme/Jonvent/preview/index-landing-video.html"> Home Page 3</a>
					</li>

					
					<li style="color: #D4AF68;font-weight: 700;font-size: 15px;letter-spacing: 3px;">Single Page</li>
					
					<li>
						<a href="http://ttheme.website/tidytheme/Jonvent/preview/blog.html">Blog Page</a>
					</li>
					<li>
						<a href="http://ttheme.website/tidytheme/Jonvent/preview/single-blog.html">Single Blog Page</a>
					</li>
					<li>
						<a href="http://ttheme.website/tidytheme/Jonvent/preview/gallery.html">Gallery Page</a>
					</li>
					<li>
						<a href="http://ttheme.website/tidytheme/Jonvent/preview/single-gallery.html">Single Gallery Page</a>
					</li>
					<li>
						<a href="http://ttheme.website/tidytheme/Jonvent/preview/about.html">about Page</a>
					</li>
					<li>
						<a href="http://ttheme.website/tidytheme/Jonvent/preview/contact.html">Contact Page</a>
					</li>
					<li>
						<a href="http://ttheme.website/tidytheme/Jonvent/preview/accommodation.html">Accommodation Page</a>
					</li>
				</ul>
				<div class="line"></div>
				<p>CHOOSE YOUR COLOR</p>
				<div class="predefined_styles">
					<a class="styleswitch" href="http://ttheme.website/tidytheme/Jonvent/preview/about.html" rel="alternate">
						<img alt="" src="./jonvent - Multipurpose Template_files/c1.jpg">
					</a> 
					<a class="styleswitch" href="http://ttheme.website/tidytheme/Jonvent/preview/about.html" rel="next">
						<img alt="" src="./jonvent - Multipurpose Template_files/c2.jpg">
					</a> 
					<a class="styleswitch" href="http://ttheme.website/tidytheme/Jonvent/preview/about.html" rel="author">
						<img alt="" src="./jonvent - Multipurpose Template_files/c3.jpg">
					</a> 
					<a class="styleswitch" href="http://ttheme.website/tidytheme/Jonvent/preview/about.html" rel="bookmark">
						<img alt="" src="./jonvent - Multipurpose Template_files/c4.jpg">
					</a> 
				</div>
				<span>
					<a class="styleswitch" href="http://ttheme.website/tidytheme/Jonvent/preview/about.html?default=true" rel="tag">RESET STYLE</a>
				</span>
			</div>
		</div>
	</div>
	<!-- END Switcher 
========================================-->	
@endsection
   

    