@extends('home.header')

@section('main-content')
<section class="agenda-section text-center" >
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>    
            <div class="col-md-8">
                <div class="card">
                    <h1> Book Now </h1>
                    <form method="POST" action="" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Event:</label>
                            <div class="col-md-6">
                                <input id="event" type="text" class="form-control" name="event" value="{{ $package->events->event_name }}" readonly autocomplete="event" autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Package:</label>
                            <div class="col-md-6">
                                <input id="package" type="text" class="form-control" name="package" value="{{ $package->pkg_name }}" readonly autocomplete="package" autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="price_per_person" class="col-md-4 col-form-label text-md-right">Amount Per Person:</label>
                            <div class="col-md-6">
                                <input id="price_per_person" type="text" class="form-control" name="price_per_person" value="{{ $package->price_per_person }}" readonly autocomplete="price_per_person" autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="deposite" class="col-md-4 col-form-label text-md-right">Deposite Per Person:</label>
                            <div class="col-md-6">
                                <input id="deposite" type="text" class="form-control" name="deposite" value="{{ $package->deposite_per_person }}" readonly autocomplete="deposite" autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="deposite" class="col-md-4 col-form-label text-md-right">Available Seats:</label>
                            <div class="col-md-6">
                                <input id="available_seat" type="text" class="form-control" name="available_seat" value="{{ $package->available_seat }}" readonly autocomplete="deposite" autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Total Persons:</label>
                            <div class="col-md-6">
                                <input id="totalper" type="number" min="0" max="{{ $package->available_seat }}" class="form-control @error('totalper') is-invalid @enderror" name="totalper" value="{{ old('totalper') }}" required autocomplete="totalper" title="Please Enter Number Of Person" oninput="calculate()" autofocus>

                                @if (session()->get('msg'))
                                    <span class="alert-danger" role="alert">
                                        <strong>{{ session()->get('msg') }}</strong>
                                    </span>    
                                @endif
                            </div>
                        </div><br>
                        <div class="row">
                            <p id="demo" style="font-size:20px;font-weight:bold;margin:0;"></p>
                        </div><br>
                        @if ($package->available_seat == 0)
                            <div class="entry-content">
                                <p><strong><h3 style="color:red;"><i class="fa fa-meh-o" style="margin-right:10px;font-size: 38px;"></i><font style="font-size: 30px">O</font><font>ops...!!</font></strong>
                                No Seats Are Available....</p>
                            </div>
                        @else    
                            <div class="form-group row mb-0">
                                <div class="col-md-12 offset-md-4">
                                    <input type="reset" class="btn btn-primary" value="Reset" name="reset" />
                                    <input type="submit" name="btn_sub" class="btn btn-primary" value="Submit" />
                                </div>
                            </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
<script>
        function calculate() {
          var no_of_person = document.getElementById("totalper").value;
          var price_per_person = document.getElementById("price_per_person").value;
          var deposite_per_person = document.getElementById("deposite").value;
          document.getElementById("demo").innerHTML = "Your Total Amount is: " + no_of_person*price_per_person+" And Your Total Deposite is: " + no_of_person*deposite_per_person;
       // alert("Your Total Amount is: " + no_of_person*price_per_person+" And Your Total Deposite is: " + no_of_person*deposite_per_person);
        }
</script>