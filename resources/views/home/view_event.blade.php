@extends('home.header')

@section('main-content')
<section class="agenda-section text-center" >
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>    
            <div class="col-md-8">
                <div class="card">
                    @if (session()->get('msg'))
                        <p class="alert alert-success">{{ session()->get('msg') }}</p>
                    @endif
                    <h1>View Events</h1>
                    <a class="btn btn-info" href="{{ route('events.create') }}">Add Event</a>
                    <a class="btn btn-info" href="{{ route('events.trashed_events') }}">View Trash</a>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="text-center" scope="col">Id</th>
                                <th class="text-center" scope="col">Name</th>
                                <th class="text-center" scope="col" colspan="2">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @isset($events)
                            @foreach ($events as $row)
                                <tr>
                                    <td scope="row">{{ $loop->iteration }}</td>
                                    <td>{{ $row->event_name }}</td>
                                    <td><a class="btn btn-primary" href="{{ route('events.edit', $row->id) }}">Edit</a>
                                        <a class="btn btn-danger" href="{{ route('events.destroy', $row->id) }}">Delete</a></td>
                                </tr>
                            @endforeach
                            @endisset
                            @isset($trashed_events)
                            @foreach ($trashed_events as $row)
                                <tr>
                                    <td scope="row">{{ $loop->iteration }}</td>
                                    <td>{{ $row->event_name }}</td>
                                    <td><a class="btn btn-primary" href="{{ route('events.restore_events', $row->id) }}">Restore</a>
                                        <a class="btn btn-danger" href="{{ route('events.forcedelete_events', $row->id) }}">Force Delete</a>
                                </tr>
                            @endforeach
                            @endisset

                        </tbody>
                    </table>
                    @isset($events)
                            <div style="">
                                {{ $events->links() }} 
                            </div>
                    @endisset
                    @isset($trashed_events)
                            <div style="">
                                {{ $trashed_events->links() }} 
                            </div>
                    @endisset
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
