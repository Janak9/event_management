@extends('home.header')

@section('slider')
<!-- header -->
<header id="page-top" class="blog-banner">
    <!-- Start: Header Content -->
    <div class="container" id="blog">
        <div class="row text-center wow fadeInUp animated" data-wow-delay="0.5s"
            style="visibility: visible;-webkit-animation-delay: 0.5s; -moz-animation-delay: 0.5s; animation-delay: 0.5s;">
            <div class="blog-header">
                <!-- Headline Goes Here -->
                <h3>Our Events <br><small> Home - Events Details</small> </h3>
            </div>
        </div>
        <!-- End: .row -->
    </div>
    <!-- End: Header Content -->
</header>
<!--/. header -->
@endsection

@section('main-content')

<!-- Page content 
==================================================-->
<div class="container-fluid no-padding page-content">
    <div class="container">
        <div class="row">
            <!-- Blog Area -->
            <div class="col-md-9 col-sm-8 col-xs-12 blog-area">
                <article class="blog-post-list wow fadeInLeft animated" data-wow-delay="0.3s"
                    style="visibility: visible;-webkit-animation-delay: 0.3s; -moz-animation-delay: 0.3s; animation-delay: 0.3s;">
                    <div class="entry-cover">
                        <img alt="blog-1" src="{{ URL::to('home_res/images/package/'.$pkg_details->image) }}">
                        <a href="/booking/{{$pkg_details->id}}" style="position: relative;margin-top: -30%;margin-left: 30%;margin-right: 44%;height: 49px;width: 144px;line-height: 32px;
                            font-size: 16px;" class="btn btn-chos hvr-shutter-in-horizontal" title="read">Booking</a>
                    </div>
                    <div class="entry-meta" style="top:-480px;padding:10px;">
                        <div id="socialHolder">
                            <div id="socialShare" class="btn-group share-group">
                                <a data-toggle="dropdown" class="btn btn-info">
                                    <i class="fa fa-share-alt fa-inverse"></i>
                                </a>
                                <ul class="dropdown-menu"
                                    style="min-width: 0px;padding: 0px;background-color: transparent;">
                                    <li>
                                        <a target="blank" title="WhatsApp" data-action="share/whatsapp/share" href="https://api.whatsapp.com/send?text={{ url('/') }}/eventDetail/{{$pkg_details->id}}%0A%0A I inviting you for {{$pkg_details->pkg_name}}.%0A%0A Click above link to check package information." class="btn btn-whatsapp">
                                            <i class="fa fa-whatsapp"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a title="Email" class="btn btn-mail"
                                            target="blank"
                                            href="mailto:?subject=Invitation&body={{ url('/') }}/eventDetail/{{$pkg_details->id}}%0A%0A I inviting you for {{$pkg_details->pkg_name}}.%0A%0A Click above link to check package information.">
                                            <i class="fa fa-envelope"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <span>
                            <font style="font-size:18px">{{ $pkg_details->events->event_name }}</font>
                        </span>
                        <span>
                            <font style="font-size:18pxont-size: 14px;padding-left: 238px;">Registration End Date:
                                {{ date('Y-m-d',strtotime(@$pkg_details->registration_end_date)) }}</font>
                        </span>
                    </div>
                    <div class="entry-meta" style="top:-81px">
                        <span class="entry-date"><i class="fa fa-clock-o"></i><a href="#">Start
                                Time-{{ $pkg_details->start_datetime }}</a></span>
                        <span class="entry-author"><i class="fa fa-clock-o"></i><a href="#">End
                                Time-{{ $pkg_details->end_datetime }}</a></span>
                    </div>
                    <h3 class="entry-title"><a href="#"
                            title="The big leagues our turn Straightnin">{{ $pkg_details->pkg_name }}</a></h3>

                    <div class="entry-content">
                        <p style="width: 100%;">{{ $pkg_details->description }} </p>
                    </div>

                    <h3 class="entry-title"><a href="#" title="The big leagues our turn Straightnin">Facilities</a></h3>

                    <div class=""
                        style="border: 1px outset;box-shadow: 3px 4px 5px;width: 41%;height: max-content;background: #f9f4f4;">
                        <p style="width: 100%;padding-left: 20px;">
                            @foreach ($features as $item)
                            <i class="fa fa-{{ $item->icon }}"></i> {{ $item->description }}<br>
                            @endforeach
                        </p>
                    </div>

                </article>
                <!--/ article -->
                <!--comments list -->

                <div class="list-comments mb70">
                    <div class="section-title mb30">
                        <h3>Comments</h3>
                    </div>
                    <!-- .section-title .mb30 -->

                    @forelse($reviews as $review)
                    <ul class="comments">
                        <li>
                            <div class="comment clearfix">
                                {{-- users is a function name like class instance.method in oops --}}
                                <img src="{{ URL::to('home_res/images/user/'.$review->users->profile) }}" style="width: 92px;
                                        height: 74px;" alt="{{ URL::to('home_res/images/user/user.jpg') }}"
                                    class="comment-avatar">
                                <strong class="commenter-title">Name : {{ $review->users->fname }}
                                    {{$review->users->lname}}</strong>
                                {{-- <span class="comment-date">25 June 2014</span> --}}
                                <p style="padding: 0;margin-top: 0%;">Rating : {{ $review->rating }}<br>Review :
                                    {{ $review->review }}</p>
                            </div>
                            <!-- .comment .clearfix -->
                        </li>
                    </ul>
                    @empty
                    <div class="entry-content">
                        <p><strong>
                                <h3 style="color:red;"><i class="fa fa-meh-o"
                                        style="margin-right:10px;font-size: 38px;"></i>
                                    <font style="font-size: 30px">O</font>
                                    <font>ops...!!</font>
                            </strong>
                            No any reviews are given for this package....</p>
                    </div>
                    @endforelse

                    <!--/ .comments -->
                </div>
                <!--/comments list -->

                <!-- comment box   -->
                <div class="contact">
                    <!-- Title -->
                    <div class="section-title">
                        <h3>Leave a reply</h3>
                    </div>
                    <!--/ Title -->
                    <div class="contact-form-warper ">
                        <div class="col-lg-12 col-sm-12 col-xs-12 contact-warper">
                            <!--  Contact Form  -->
                            @isset($msg)
                                <script type="text/javascript">
                                    alert("{{ $msg }}");
                                </script>
                            @endisset
                            <div class="contact-form">
                                <form method="post" action="" id="contactForm" method="post" name="contactForm">
                                    @csrf
                                    <b style="font-size:20px;margin-left: -54%;">Your Rating:</b>
                                    <div class="col-lg-7">
                                        <div class="form-group col-lg-12" style="margin: 3% 0 0 85%;">
                                            <div class="rate" style="margin-left: -46%;margin-top: -5%;">
                                                <fieldset class="rating">
                                                    <input type="radio" id="star5" name="rating" value="5" /><label class="full" for="star5" title="Awesome - 5 stars"></label>
                                                    <input type="radio" id="star4half" name="rating" value="4.5" checked /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                                                    <input type="radio" id="star4" name="rating" value="4" /><label class="full" for="star4" title="Pretty good - 4 stars"></label>
                                                    <input type="radio" id="star3half" name="rating" value="3.5" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                                                    <input type="radio" id="star3" name="rating" value="3" /><label class="full" for="star3" title="Meh - 3 stars"></label>
                                                    <input type="radio" id="star2half" name="rating" value="2.5" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                                                    <input type="radio" id="star2" name="rating" value="2" /><label class="full" for="star2" title="Kinda bad - 2 stars"></label>
                                                    <input type="radio" id="star1half" name="rating" value="1.5" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                                                    <input type="radio" id="star1" name="rating" value="1" /><label class="full" for="star1" title="Sucks big time - 1 star"></label>
                                                    <input type="radio" id="starhalf" name="rating" value="0.5" required /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <div class="form-group col-lg-12">
                                            <textarea class="form-control" id="Message" name="review"
                                                placeholder="Message :" required></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <div class="form-group col-lg-12">
                                            <input class="submit-button btn" style="margin-left: 42%;margin-top: 7%;"
                                                name="reviewsubmit" value="Submit" type="submit">
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <!-- End:Contact Form  -->
                        </div>
                    </div>
                </div>
                <!--/.comment box-->
            </div>

            <!--/ Blog Area -->
            <!-- Widget Area -->
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 widget-area">
                <!-- Post Categories -->
                <aside class="widget widget-post-categories" style="color:#333;border: 1px inset;
box-shadow: 3px 4px 5px;">
                    <h3 class="widget-title">Information</h3>
                    <ul class="categories-type" style="color:#333;">
                        <li style="color:#333;">
                            Package Type : {{$pkg_details->type}}
                        </li>
                        <li style="color:#333;">
                            Price Per Person : {{$pkg_details->price_per_person}}
                        </li>
                        <li style="color:#333;">
                            Deposite Per Person : {{$pkg_details->deposite_per_person}}
                        </li>
                        <li style="color:#333;">
                            Person Capacity : {{$pkg_details->person_capacity}}
                        </li>
                    </ul>
                </aside>
                <!-- Post Categories /- -->
                <!-- Recent Post -->
                {{-- <aside class="widget wiget-recent-post">
                        <h3 class="widget-title">Recent Post</h3>
                        <div class="recent-post-box">
                            <div class="recent-img">
                                <a href="http://ttheme.website/tidytheme/Jonvent/preview/single-blog.html"><img alt="recent-1" src="event_details_files/recent-1.jpg">
                                </a>
                            </div>
                            <div class="recent-title">
                                <a href="http://ttheme.website/tidytheme/Jonvent/preview/single-blog.html">This is latest Pic</a>
                                <p>25th Dec 2015</p>
                            </div>
                        </div>
                        <div class="recent-post-box">
                            <div class="recent-img">
                                <a href="http://ttheme.website/tidytheme/Jonvent/preview/single-blog.html"><img alt="recent-1" src="event_details_files/recent-2.jpg">
                                </a>
                            </div>
                            <div class="recent-title">
                                <a href="http://ttheme.website/tidytheme/Jonvent/preview/single-blog.html">This is latest Video</a>
                                <p>25th Dec 2015</p>
                            </div>
                        </div>
                        <div class="recent-post-box">
                            <div class="recent-img">
                                <a href="http://ttheme.website/tidytheme/Jonvent/preview/single-blog.html"><img alt="recent-1" src="event_details_files/recent-3.jpg">
                                </a>
                            </div>
                            <div class="recent-title">
                                <a href="http://ttheme.website/tidytheme/Jonvent/preview/single-blog.html">Weekly Celebration</a>
                                <p>25th Dec 2015</p>
                            </div>
                        </div>
                    </aside> --}}
                <!-- Recent Post /- -->
                <aside class="widget widget-post-categories widget-archives"
                    style="border: 1px outset;box-shadow: 3px 4px 5px;">
                    <h3 class="widget-title"> Address</h3>
                    <ul class="categories-type">
                        <li>
                            Country : {{ $pkg_details->country }}
                        </li>
                        <li>
                            State : {{ $pkg_details->state }}
                        </li>
                        <li>
                            City : {{ $pkg_details->city }}
                        </li>
                        <li>
                            Address : {{ $pkg_details->address }}
                        </li>
                        <li>
                            Landmark : {{ $pkg_details->landmark }}
                        </li>

                    </ul>
                </aside>
                <!-- Archives /- -->
                <!-- Widget Tags -->
                <aside class="widget widget-tags" style="border: 1px outset;box-shadow: 3px 4px 5px;">
                    <h4 class="widget-title">Contact</h4>
                    {{$pkg_details->contact_no}} <br>
                    {{$pkg_details->users->email}}
                </aside>
                <!-- Widget Tags /- -->
                <!-- Archives -->

            </div>
            <!-- Widget Area /- -->

        </div>
    </div>
    <!-- Container /- -->
</div>
<!-- End: Page-content  
==================================================-->

@endsection