@extends('home.header')

@section('main-content')
<section class="agenda-section text-center" >
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>    
            <div class="col-md-8">
                <div class="card">
                    <h1>{{ $task }} Package</h1>
                    {{-- <div class="card-header" style="font-size:30px;text-align:center">{{ __('Register') }}</div> --}}
                    <form method="POST" action="{{ $submit_route }}" enctype="multipart/form-data">
                        @csrf
                        @if ($task == 'Edit')
                            @method('PUT')
                        @endif
                        <div class="form-group row">
                            <label for="type" class="col-md-4 col-form-label text-md-right">Event</label>
                            <div class="col-md-6">
                                <select name="event" class="form-control">
                                    <option value="" disabled>Select Event</option>
                                    @foreach ($events as $item)
                                        <option value="{{ $item->id }}"
                                            @if (isset($package) && $item->id == $package->eid)
                                                selected
                                            @endif>{{ $item->event_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="type" class="col-md-4 col-form-label text-md-right">Package Type</label>
                            <div class="col-md-6">
                                <select name="type" class="form-control">
                                        <option value="" disabled>Select Type</option>
                                        <option value="private" {{ old('private', @$package->type) == "private" ? 'selected' : '' }}>Private</option>
                                        <option value="public" {{ old('public', @$package->type) == "public" ? 'selected' : '' }}>Public</option>
                                </select>
                                 @error('type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror 
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="pkg_name" class="col-md-4 col-form-label text-md-right">Package Name</label>
                            <div class="col-md-6">
                                <input id="pkg_name" type="text" placeholder="Package Name" class="form-control @error('pkg_name') is-invalid @enderror" name="pkg_name" value="{{old('pkg_name', @$package->pkg_name)}}" pattern="[a-zA-Z0-9 ]+" maxlength="50" required autocomplete="pkg_name" autofocus>

                                @error('pkg_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">Description</label>
                            <div class="col-md-6">
                                <textarea id="description" type="text" placeholder="About Package" class="form-control @error('description') is-invalid @enderror" name="description" required autocomplete="description" rows="5" autofocus>{{old('description', @$package->description)}}</textarea>
                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="address" class="col-md-4 col-form-label text-md-right">Address</label>
                            <div class="col-md-6">
                                <textarea id="address" type="text" placeholder="Address" class="form-control @error('address') is-invalid @enderror" name="address" maxlength="100" required autocomplete="address" autofocus>{{old('address', @$package->address)}}</textarea>
                                @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                 
                        <div class="form-group row">
                            <label for="landmark" class="col-md-4 col-form-label text-md-right">Landmark</label>
                            <div class="col-md-6">
                                <input id="landmark" type="text" placeholder="Landmark" class="form-control @error('landmark') is-invalid @enderror" name="landmark" maxlength="30" value="{{ old('landmark', @$package->landmark) }}" required autocomplete="landmark" autofocus />
                                @error('landmark')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="country" class="col-md-4 col-form-label text-md-right">country</label>
                            <div class="col-md-6">
                                <input id="country" type="text" placeholder="Country" class="form-control @error('country') is-invalid @enderror" name="country" maxlength="20" value="{{old('country', @$package->country)}}" pattern="[a-zA-Z ]+" required autocomplete="country" autofocus>
                                @error('country')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="state" class="col-md-4 col-form-label text-md-right">State</label>
                            <div class="col-md-6">
                                <input id="state" type="text" placeholder="State"  class="form-control @error('state') is-invalid @enderror" name="state" maxlength="20" value="{{old('state', @$package->state)}}" pattern="[a-zA-Z ]+" required autocomplete="state" autofocus>
                                @error('state')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="city" class="col-md-4 col-form-label text-md-right">City</label>
                            <div class="col-md-6">
                                <input id="city" type="text"  placeholder="City"  class="form-control @error('city') is-invalid @enderror" name="city" maxlength="20" value="{{old('city', @$package->city)}}" pattern="[a-zA-Z ]+" required autocomplete="city" autofocus>
                                @error('city')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="contact_no" class="col-md-4 col-form-label text-md-right">Contact no</label>
                            <div class="col-md-6">
                                <input id="contact_no" type="text" placeholder="Contact No"  class="form-control @error('contact_no') is-invalid @enderror" name="contact_no" maxlength="17" pattern="[0-9]+" value="{{old('contact_no', @$package->contact_no)}}" required autocomplete="contact_no" autofocus>
                                @error('contact_no')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="price_per_person" class="col-md-4 col-form-label text-md-right">price per person</label>
                            <div class="col-md-6">
                                <input id="price_per_person" type="number" placeholder="Price Per Person" min="0" class="form-control @error('price_per_person') is-invalid @enderror" name="price_per_person" value="{{old('price_per_person', @$package->price_per_person)}}" required autocomplete="price_per_person" autofocus>
                                @error('price_per_person')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="deposite_per_person" class="col-md-4 col-form-label text-md-right">deposite per person</label>
                            <div class="col-md-6">
                                <input id="deposite_per_person" type="number" placeholder="Deposite Per Person" min="0" class="form-control @error('deposite_per_person') is-invalid @enderror" name="deposite_per_person" value="{{old('deposite_per_person', @$package->deposite_per_person)}}" required autocomplete="deposite_per_person" autofocus>
                                @error('deposite_per_person')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="person_capacity" class="col-md-4 col-form-label text-md-right">Person Capacity</label>
                            <div class="col-md-6">
                                <input id="person_capacity" type="number" placeholder="Person Capacity" min="10" class="form-control @error('person_capacity') is-invalid @enderror" name="person_capacity" value="{{old('person_capacity', @$package->person_capacity)}}" required autocomplete="person_capacity" autofocus>
                                @error('person_capacity')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="registration_end_date" class="col-md-4 col-form-label text-md-right">Registration End Date</label>
                            <div class="col-md-6">
                                <input id="registration_end_date" type="date" class="form-control @error('registration_end_date') is-invalid @enderror" name="registration_end_date" value="{{ old('registration_end_date', date('Y-m-d',  strtotime(@$package->registration_end_date))) }}" required autocomplete="registration_end_date" autofocus
                                @if (! isset($package))
                                    min="{{ date('Y-m-d') }}"
                                @endif />
                                @error('registration_end_date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="start_datetime" class="col-md-4 col-form-label text-md-right">Event Start Date</label>
                            <div class="col-md-6">
                                <input id="start_datetime" type="datetime-local" class="form-control @error('start_datetime') is-invalid @enderror" name="start_datetime" value="{{old('start_datetime', date('Y-m-d\Th:m:s',  strtotime(@$package->start_datetime)))}}" required autocomplete="start_datetime" autofocus>
                                @error('start_datetime')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="end_datetime" class="col-md-4 col-form-label text-md-right">Event End Date</label>
                            <div class="col-md-6">
                                <input id="end_datetime" type="datetime-local" class="form-control @error('end_datetime') is-invalid @enderror" name="end_datetime" value="{{old('end_datetime', date('Y-m-d\Th:m:s',  strtotime(@$package->end_datetime)))}}" required autocomplete="end_datetime" autofocus>
                                @error('end_datetime')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                                <label for="image" class="col-md-4 col-form-label text-md-right">image</label>
                                <div class="col-md-6">
                                    <input id="image" type="file" class="form-control @error('image') is-invalid @enderror" name="image" value="{{ old('image') }}" autocomplete="image" autofocus onchange="loadImage(event, 'preview')">
                                    @error('image')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                        </div><br>
                        <img id="preview" src="{{ URL::to('home_res/images/package/'.@$package->image) }}" height="200px" width="250px" />
                        <div class="form-group row mb-0">
                            <div class="col-md-12 offset-md-4">
                                <input type="reset" class="btn btn-primary" value="Reset" name="reset" />
                                <input type="submit" name="btn_sub" class="btn btn-primary" value="Submit" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
