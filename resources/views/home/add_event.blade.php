@extends('home.header')

@section('main-content')
<section class="agenda-section text-center" >
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>    
            <div class="col-md-8">
                <div class="card">
                    <h1>{{ $task }} Event</h1>
                    {{-- <div class="card-header" style="font-size:30px;text-align:center">{{ __('Register') }}</div> --}}
                    <form method="POST" action="{{ $submit_route }}" enctype="multipart/form-data">
                        @csrf
                        @if ($task == 'Edit')
                            @method('PUT')
                        @endif
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Event Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" placeholder="Enter Event Name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{old('name', @$event->event_name)}}" pattern="[a-zA-Z0-9 ]+" maxlength="50" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div><br>
                        <div class="form-group row mb-0">
                            <div class="col-md-12 offset-md-4">
                                <input type="reset" class="btn btn-primary" value="Reset" name="reset" />
                                <input type="submit" name="btn_sub" class="btn btn-primary" value="Submit" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
