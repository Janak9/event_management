<html>
<head>
    <title>EventManagement</title>
    <link rel="icon" type="image/png" href="{{ URL::to('images/favicon.png') }}">
    <link rel="icon" href="{{ URL::to('images/favicon.png') }}">
    <meta charset="UTF-8">
    <style type="text/css" media="all">
        td {
            padding: 5px;
        }
        table {
            width: 100%;
        }
        .bgcolor {
            background-color: #dbdde0 !important;
        }
        .tcenter {
            text-align: center;
        }
        body {
            -webkit-print-color-adjust: exact;
        }
        @page {
            size: auto;
            /* auto is the initial value */
            margin: 0;
            /* this affects the margin in the printer settings */
        }
        .btnprint {
            margin: 10px;
            padding: 10px;
        }
        .border {
            border: 1px solid #a6a7a8;
        }
        .ucase {
            text-transform: uppercase;
        }
    </style>

    <style type="text/css" media="print">
        .btnprint {
            display: none;
        }
    </style>

    <script type="text/javascript">
        function printPage(obj) {
            //document.getElementById("hide").style.display="none";
            window.print();
        }
    </script>
</head>

<body>
    <div id="hide">
        <input type="button" class="btnprint" name="" value="PRINT" onclick="printPage(this)">
    </div>

    <table class="tmain">
        <tr>
            <!--header-->
            <table class="border bgcolor">
                <tr>
                    <td width="10%"></td>
                    <td width="70%" align="center">
                        <span class="ucase" style="font-size: 30px;"><span style="color:#FF542E;">Event</span>
                            Management</span>
                    </td>
                    <td width="20%">
                        Event1270@gmail.com<br>
                        +7067890223
                    </td>
                </tr>
            </table>
        </tr>

        <tr>
            <!--company details-->
            <table class="border">
                <tr>
                    <td></td>
                    <td align="right">Invoice Date:{{ $payment->created_at }}</td>
                </tr>
                <tr>
                    <td>Invoice Serial Number: {{ $payment->id }}</td>
                    <td align="right">Tax Is Payable On Reverse Charge: No</td>
                </tr>
            </table>
        </tr>

        <tr>
            <!--customer shiping details-->
            <table>
                <tr class="bgcolor">
                    <td class="border">Details of Client</td>
                </tr>
                <tr>
                    <td class="border">
                        <table>
                            <!--customer add-->
                            <tr>
                                <td>Name:</td>
                                <td>{{ $user->fname }} {{ $user->lname }}</td>
                            </tr>
                            <tr>
                                <td>Mobile No:</td>
                                <td>{{ $user->contact_no }}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </tr>
        <tr>
            <!--product details-->
            <table class="border">
                <tr class="bgcolor">
                    <td>Booked Info :</td>
                    <td>Payment Of {{ @$pkg->pkg_name }} Package Booking.</td>
                </tr>
                <tr class="bgcolor">
                    <td>TOTAL</td>
                    <td>Rs. {{ $amt = $payment->amount }} /-</td>
                </tr>
                <tr class="bgcolor">
                    <td>TOTAL(in words)</td>
                    <td>{{ getIndianCurrency($amt) }}</td>
                </tr>
            </table>
        </tr>

        <tr>
            <!--footer pmode signature ern-->
            <table>
                <tr class="bgcolor tcenter">
                    <td class="border">Payment Mode</td>
                    <td class="border">Electronic Reference Number</td>
                </tr>
                <tr class="tcenter">
                    <td class="border">PayUmoney</td>
                    <td class="border">{{ $payment->txn_id }}</td>
                </tr>
            </table>

            <table>
                <tr style="text-align:right;">
                    <td class="border"> <br>Signature:<u>Event
                            Management</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
            </table>
        </tr>

    </table>

</body>

</html>
<?php
function getIndianCurrency($number)
{
    $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(
        0 => '', 1 => 'one', 2 => 'two',
        3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
        7 => 'seven', 8 => 'eight', 9 => 'nine',
        10 => 'ten', 11 => 'eleven', 12 => 'twelve',
        13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
        16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
        19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
        40 => 'forty', 50 => 'fifty', 60 => 'sixty',
        70 => 'seventy', 80 => 'eighty', 90 => 'ninety'
    );
    $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
    while ($i < $digits_length) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str[] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural . ' ' . $hundred : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] . ' ' . $digits[$counter] . $plural . ' ' . $hundred;
        } else $str[] = null;
    }
    $Rupees = implode('', array_reverse($str));
    $paise = ($decimal) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    return ($Rupees ? $Rupees . 'Rupees ' : '') . $paise;
}
?>
