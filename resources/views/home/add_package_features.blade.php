@extends('home.header')

@section('main-content')
<link href="{{ URL::to('home_res/css/style_icon_search.css') }}" rel="stylesheet">
<section class="agenda-section text-center" >
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>    
            <div class="col-md-8">
                <div class="card">
                    <h1>{{ $task }} Package Features</h1>
                    {{-- <div class="card-header" style="font-size:30px;text-align:center">{{ __('Register') }}</div> --}}
                    <form method="POST" action="{{ $submit_route }}" enctype="multipart/form-data">
                        @csrf
                        @if ($task == 'Edit')
                            @method('PUT')
                        @endif
                        <div class="form-group row">
                            <label for="type" class="col-md-4 col-form-label text-md-right">Package</label>
                            <div class="col-md-6">
                                <select name="package" class="form-control" required>
                                    <option value="" disabled>Select Package</option>
                                    @foreach ($packages as $item)
                                        <option value="{{ $item->id }}"
                                            @if (isset($package_features) && $item->id == $package_features->pid)
                                                selected
                                            @endif>{{ $item->pkg_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">Description</label>
                            <div class="col-md-6">
                                <input id="description" type="text" placeholder="Description" class="form-control @error('description') is-invalid @enderror" name="description" value="{{old('description', @$package_features->description )}}" maxlength="50" required autocomplete="name" autofocus>
                                <p class="help-block">eg. provide food, DJ, etc</p>
                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                        <label for="icon" class="col-md-4 col-form-label text-md-right">Icon <i id="display_icon" class="fa fa-{{ @$package_features->icon }}"></i></label>
                            <div class="col-md-6">
                                <input id="icon" type="hidden" name="icon" value="{{old('icon', @$package_features->icon)}}" autofocus>
                                
                                <input type="text" class="form-control @error('icon') is-invalid @enderror" id="search" placeholder="Icon Search ..." value="{{old('icon', @$package_features->icon)}}" autocomplete="off" autofocus />
                                <select name="catagory" id="catagory" class="catagory form-control">
                                    <option value="">All Categories</option>
                                </select>
                                @error('icon')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <br>
                        <div class="form-group row mb-0">
                            <div class="col-md-12 offset-md-4">
                                <input type="reset" class="btn btn-primary" value="Reset" name="reset" />
                                <input type="submit" name="btn_sub" class="btn btn-primary" value="Submit" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="list">
                    <ul class="icons"></ul>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- <script src="{{ URL::to('home_res/js/jquery-1.10.2.js') }}"></script> --}}
<script src="{{ URL::to('home_res/js/icons.js') }}"></script>
<script src="{{ URL::to('home_res/js/aliases.js') }}"></script>
<script src="{{ URL::to('home_res/js/script.js') }}"></script>
@endsection
