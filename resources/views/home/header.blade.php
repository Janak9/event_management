<!DOCTYPE html>
<!-- saved from url=(0068)index-slideshow.html -->
<html lang="en"
    class="js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers no-applicationcache svg inlinesvg smil svgclippaths gr__ttheme_website">
<!--
<![endif]-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
        .gm-style .gm-style-mtc label,
        .gm-style .gm-style-mtc div {
            font-weight: 400
        }
        .gm-control-active>img {
            box-sizing: content-box;
            display: none;
            left: 50%;
            pointer-events: none;
            position: absolute;
            top: 50%;
            transform: translate(-50%, -50%)
        }

        .gm-control-active>img:nth-child(1) {
            display: block
        }

        .gm-control-active:hover>img:nth-child(1),
        .gm-control-active:active>img:nth-child(1) {
            display: none
        }

        .gm-control-active:hover>img:nth-child(2),
        .gm-control-active:active>img:nth-child(3) {
            display: block
        }

        .gm-ui-hover-effect {
            opacity: .6
        }

        .gm-ui-hover-effect:hover {
            opacity: 1
        }

        .gm-style .gm-style-cc span,
        .gm-style .gm-style-cc a,
        .gm-style .gm-style-mtc div {
            font-size: 10px;
            box-sizing: border-box
        }

        @media print {

            .gm-style .gmnoprint,
            .gmnoprint {
                display: none
            }
        }

        @media screen {

            .gm-style .gmnoscreen,
            .gmnoscreen {
                display: none
            }
        }

        .gm-style-pbc {
            transition: opacity ease-in-out;
            background-color: rgba(0, 0, 0, 0.45);
            text-align: center
        }

        .gm-style-pbt {
            font-size: 22px;
            color: white;
            font-family: Roboto, Arial, sans-serif;
            position: relative;
            margin: 0;
            top: 50%;
            -webkit-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            transform: translateY(-50%)
        }

        .gm-style img {
            max-width: none;
        }

        .gm-style {
            font: 400 11px Roboto, Arial, sans-serif;
            text-decoration: none;
        }
    </style>
    <!-- TITLE OF SITE -->
    <title>Event Management</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Multipurpose Template">
    <meta name="keywords"
        content="jonvent, parallax, Corporate, Agency, one page, multipage, responsive, landing, html template">
    <meta name="author" content="Tidytheme">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Favicons -->
    <link rel="icon" type="image/png" href="{{ URL::to('images/favicon.png') }}">
    <link rel="icon" href="{{ URL::to('images/favicon.png') }}">
    <link rel="apple-touch-icon" sizes="72x72"
        href="images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114"
        href="images/apple-touch-icon-114x114.png">


    <!-- CSS Begins
    ================================================== -->
    <!-- Google Font And Custom Font Begins -->
    <link type="text/css" rel="stylesheet" href="{{ URL::to('home_res/css/css.css') }}">
    <link href="{{ URL::to('home_res/css/css(1).css') }}" rel="stylesheet">

    <!-- FONT ICONS -->
    <!-- font-awesome -->
    <link href="{{ URL::to('home_res/css/font-awesome-4.6.min.css') }}" rel="stylesheet">
    <!-- IonIcons -->
    {{-- <link href="{{ URL::to('home_res/css/ionicons.css') }}" rel="stylesheet"> --}}
    <link href="{{ URL::to('home_res/css/ionicons.min.css') }}" rel="stylesheet">

    <!-- Elegant Icons -->
    <link href="{{ URL::to('home_res/css/style.css') }}" rel="stylesheet">
    <!--/ FONT ICONS -->

    <!--Animate Effect-->
    <link href="{{ URL::to('home_res/css/animate.css') }}" rel="stylesheet">
    <link href="{{ URL::to('home_res/css/hover.css') }}" rel="stylesheet">

    <!--Owl Carousel -->
    <link href="{{ URL::to('home_res/css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ URL::to('home_res/css/owl.theme.css') }}" rel="stylesheet">
    <link href="{{ URL::to('home_res/css/owl.transitions.css') }}" rel="stylesheet">

    <!--BootStrap -->
    <link href="{{ URL::to('home_res/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::to('home_res/css/normalize.css') }}" rel="stylesheet">

    <!-- nivo slider CSS -->
    <link rel="stylesheet" href="{{ URL::to('home_res/css/nivo-slider.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::to('home_res/css/nivo-preview.css') }}" type="text/css" media="screen">

    <!--Preloader -->
    <link href="{{ URL::to('home_res/css/preloader.css') }}" rel="stylesheet">

    <!-- Main Style -->
    <link href="{{ URL::to('home_res/css/style(1).css') }}" rel="stylesheet">
    <link href="{{ URL::to('home_res/css/responsive.css') }}" rel="stylesheet">

    <!--Replace Your Color-->
    <link rel="stylesheet" href="{{ URL::to('home_res/css/default_color.css') }}">
    <!--Replace Your Color Ends-->

    <!-- Switcher Styles-->
    <link rel="stylesheet" id="switcher-css" type="text/css" href="{{ URL::to('home_res/css/switcher.css') }}"
        media="all">
    <!-- END Switcher Styles -->

    <!-- Template Color Demo Examples -->
    <link rel="alternate stylesheet" type="text/css" href="{{ URL::to('home_res/css/color1.css') }}" title="alternate"
        media="all">
    <link rel="alternate stylesheet" type="text/css" href="{{ URL::to('home_res/css/color2.css') }}" title="next"
        media="all" disabled="">
    <link rel="alternate stylesheet" type="text/css" href="{{ URL::to('home_res/css/color3.css') }}" title="author"
        media="all" disabled="">
    <link rel="alternate stylesheet" type="text/css" href="{{ URL::to('home_res/css/color4.css') }}" title="bookmark"
        media="all" disabled="">

    <link rel="stylesheet" href="{{ URL::to('home_res/css/events.css') }}">
    <!-- END Template Color Demo Examples -->
    <script src="{{ URL::to('home_res/js/jquery-1.12.1.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::to('home_res/js/jquery-ui.js') }}" type="text/javascript"></script>

    {{-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script> --}}

    {{-- <script type="text/javascript" charset="UTF-8" src="{{ URL::to('home_res/js/common.js') }}"></script> --}}
    {{-- <script type="text/javascript" charset="UTF-8" src="{{ URL::to('home_res/js/util.js') }}"></script> --}}
    {{-- <script type="text/javascript" charset="UTF-8" src="{{ URL::to('home_res/js/map.js') }}"></script> --}}
    {{-- <script type="text/javascript" charset="UTF-8" src="{{ URL::to('home_res/js/marker.js') }}"></script> --}}
    {{-- <script type="text/javascript" charset="UTF-8" src="{{ URL::to('home_res/js/onion.js') }}"></script>
    <script type="text/javascript" charset="UTF-8" src="{{ URL::to('home_res/js/stats.js') }}"></script>
    <script type="text/javascript" charset="UTF-8" src="{{ URL::to('home_res/js/infowindow.js') }}"></script> --}}
    <style type="text/css">
        @-webkit-keyframes _gm4298 {
            0% {
                -webkit-transform: translate3d(0px, -500px, 0);
                -webkit-animation-timing-function: ease-in;
            }

            50% {
                -webkit-transform: translate3d(0px, 0px, 0);
                -webkit-animation-timing-function: ease-out;
            }

            75% {
                -webkit-transform: translate3d(0px, -20px, 0);
                -webkit-animation-timing-function: ease-in;
            }

            100% {
                -webkit-transform: translate3d(0px, 0px, 0);
                -webkit-animation-timing-function: ease-out;
            }
        }
    </style>
    <script type="text/javascript">
        function loadImage(event, id){
            var op = document.getElementById(id);
            op.src = URL.createObjectURL(event.target.files[0]);
        }
    </script>
    {{-- <script type="text/javascript" charset="UTF-8" src="{{ URL::to('home_res/js/controls.js') }}"></script> --}}
</head>

<body data-gr-c-s-loaded="true">
    <!-- Start: Header Section
=============================================-->

    @yield('slider')

    <!-- navigation bar -->
    <nav class="navbar navbar-default" id="aboutSectionTestPurposes">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button class="navbar-toggle" data-target=".js-navbar-collapse" data-toggle="collapse" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand scroll" href="/">
                    <img alt="Event" class="" src="{{ URL::to('images/logo.png') }}" style="margin-left: -71px;width: 230px;">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse js-navbar-collapse " id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="dropdown dropdown-large">
                        <a href="/">Home</a>
                    </li>
                    {{-- Login code --}}
                    <li class="dropdown dropdown-large">
                        <a href="/eventlist">Events</a>
                    </li>
                    {{-- <li class="dropdown dropdown-large">
                        <a href="/contactus">Contact</a>
                    </li> --}}
                    <li class="dropdown dropdown-large">
                        <a href="/aboutus">About Us</a>
                    </li>    
                    @if (Route::has('login'))
                    @auth
                        <li class="dropdown dropdown-large">
                            <a class="dropdown-toggle" data-delay="10" data-hover="dropdown" data-toggle="dropdown"
                                href="">{{ Auth::user()->fname }} {{ Auth::user()->lname }}</a>
                            <!-- Menu Name-->
                            <ul class="dropdown-menu dropdown-menu-large row col-lg-6 col-sm-7">
                                <li class="col-lg-4 col-sm-4">
                                    <ul>
                                        <!-- Sub Menu Name-->
                                        <li class="dropdown-header">Dashboard</li>
                                        <li>
                                            <a href="{{ route('events.index') }}">event</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('packages.index') }}">package</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('package_features.index') }}">package feature</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('my-bookings') }}">My Bookings</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="col-lg-4 col-sm-4">
                                    <ul>
                                        <!-- Sub Menu Name-->
                                        <li class="dropdown-header">Information</li>
                                        <li>
                                            <a href="{{ route('profile-edit') }}">Profile</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('change-pwd') }}">Change Password?</a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        
                        <li class="dropdown dropdown-large">
                            <a class="dropdown-toggle" data-delay="10" data-hover="dropdown" data-toggle="dropdown"
                                href=""><div class="notification show-count" data-count="0"></div></a>
                            <!-- Menu Name-->
                            <ul class="dropdown-menu dropdown-menu-large row col-lg-5 col-sm-5" style="overflow-y: auto;max-height: 400px;">
                                <li class="col-lg-12 col-sm-12" style="padding: 0px;">
                                    <ul id="notification_list">
                                        <!-- Sub Menu Name-->
                                        <li>no notifications!</li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        
                        {{-- <button onclick="bell()">Increment Notifications</button> --}}
                        <script type="text/javascript">
                            var el = document.querySelector('.notification');
                            function bell(){
                                el.classList.remove('notify');
                                el.offsetWidth = el.offsetWidth;
                                el.classList.add('notify');
                            }

                            function timeSince(date) {
                                date = new Date(date);
                                var seconds = Math.floor((new Date() - date) / 1000);
                                
                                var interval = Math.floor(seconds / 86400);
                                if (interval >= 1) {
                                    return date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear();
                                }
                                interval = Math.floor(seconds / 3600);
                                if (interval >= 1) {
                                    return interval + " hours ago";
                                }
                                interval = Math.floor(seconds / 60);
                                if (interval >= 1) {
                                    return interval + " min ago";
                                }
                                return Math.floor(seconds) + " sec ago";
                            }
                            
                            function notify(){
                                setInterval(function(){
                                    $.ajax({
                                        type: 'get',
                                        url: '/get_notification/{{ Auth::user()->id }}',
                                        accepts:'json',
                                        success: function(response){
                                            res = JSON.parse(response);
                                            var str = "";
                                            let unread_count = 0;
                                            console.log(res);
                                            if(res.length != 0){
                                                res.forEach(element => {
                                                    let cls = "";
                                                    if(element.read_status == 0)
                                                    {
                                                        cls = "unread";
                                                        unread_count++;
                                                    }
                                                    let t = timeSince(element.created_at);
                                                    str += "<a href='/notification_details/"+ element.id +"'><li class="+ cls +"><div class='notification-item'><p>"+ element.msg +"</p><p>"+ t +"</p></div></li></a>";
                                                });
                                                el.setAttribute('data-count', unread_count);
                                                if(unread_count > 0)
                                                    bell();
                                            }else{
                                                str = "<li>no notifications!</li>";
                                            }
                                            document.getElementById("notification_list").innerHTML = str;
                                        },
                                        error: function(err){
                                            console.log(err);
                                        }
                                    })
                                },2000);
                            }
                            notify();
                        </script>
                    @else
                        <li class="dropdown dropdown-large">
                           <a href="{{ route('login') }}">Login</a>
                        </li>
                        {{-- Registration code --}}
                        <li class="dropdown dropdown-large">                                
                            <a href="{{ route('register') }}">Register</a>
                        </li>
                    @endauth
                    @endif
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    <!--/. navigation bar -->
    <div class="header" id="home"></div>
    <!--End: Header Section
==================================================-->
<div style="clear:both;"></div>
@yield('main-content')

@include('home.footer')
