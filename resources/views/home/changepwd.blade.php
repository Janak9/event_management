@extends('home.header')

@section('main-content')
<section class="agenda-section text-center" >
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>    
            <div class="col-md-8">
                <div class="card">
                    <h1>Change Password</h1>

                    <div class="card-body">
                        @if (session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                        @endif
                        @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                        @endif
                    <form method="POST" action="/pwd-change-post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row{{ $errors->has('current-password') ? ' has-error' : '' }}">
                            <label for="current-password" class="col-md-4 col-form-label text-md-right">{{ __('Current Password') }}</label>
                            <div class="col-md-6">
                                <input id="current-password" type="password" placeholder="Current Password" class="form-control" name="current-password" pattern="[a-zA-Z0-9]+" minlength="8" maxlength="10" required autocomplete="current-password" autofocus >
                                <p align="left"><small style="color:grey">Your Current Password Must be Match</small></p>
                                @if ($errors->has('current-password'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('current-password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row{{ $errors->has('new-password') ? ' has-error' : '' }}">
                            <label for="new-password" class="col-md-4 col-form-label text-md-right">{{ __('New Password') }}</label>
                            <div class="col-md-6">
                                <input id="new-password" type="password" placeholder="New Password" class="form-control" name="new-password" value="" pattern="[a-zA-Z0-9]+" minlength="8" maxlength="10" required autocomplete="new-password" autofocus >
                                <p align="left"><small style="color:grey">New Password cannot be same as your current password</small></p>
                                @if ($errors->has('new-password'))
                                     <span class="help-block">
                                     <strong>{{ $errors->first('new-password') }}</strong>
                                     </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                                <label for="new-password-confirm" class="col-md-4 control-label">Confirm New Password</label>

                                <div class="col-md-6">
                                    <input id="new-password-confirm" type="password" placeholder="Confirm Password" class="form-control" name="new-password_confirmation" pattern="[a-zA-Z0-9]+" minlength="8" maxlength="10" required>
                                    <p align="left"><small style="color:grey">New Password and Confirm password must be same</small></p>
                                </div>
                            </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-12 offset-md-4">
                                <input type="reset" class="btn btn-primary" value="Reset" name="reset" />
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Change Password') }}
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
