<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Event Management</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="shortcut icon" href="">
	<link rel="stylesheet" href="{{ URL::to('home_res/css/bootstrap4.min.css') }}">
	<link rel="stylesheet" href="{{ URL::to('home_res/js/bootstrap.min.js') }}" />
	
</head>
<body>

<!-- Bootstrap 4 Navbar  -->
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
	<a href="#" class="navbar-brand">PayUMoney Gateway</a>

	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

	<div class="collapse navbar-collapse" id="navbarsExampleDefault">
		<ul class="navbar-nav ml-auto">
			<li class="nav-item">
				<a href="/" class="nav-link">Home</a>
			</li>
		</ul>
	</div>
</nav>
<!-- End Bootstrap 4 Navbar -->

<div class="container mt-5">
	<div class="row">
        <div class="col-md-2"></div>  
        <div class="col-md-8">
        	<div class="card">
			<h5 class="card-header bg-success text-white">Checkout Confirmation</h5>
        		<div class="card-body">
					{{-- <form action="{{$action;}}/_payment" method="post" id="payuForm" name="payuForm">--}}
        			<form action="{{ $data['action'] }}/_payment" method="post" id="payuForm" name="payuForm">
		                <input type="hidden" name="key" value="{{ $data['mkey'] }}" />
		                <input type="hidden" name="hash" value="{{ $data['hash'] }}"/>
		                <input type="hidden" name="txnid" value="{{ $data['tid'] }}" />
		                <div class="form-group">
		                    <label class="control-label">Total Payable Amount</label>
		                    <input class="form-control" name="amount" value="{{ $data['amount'] }}"  readonly/>
		                </div>
		                <div class="form-group">
		                    <label class="control-label">Your Name</label>
						<input class="form-control" name="firstname" id="firstname" value="{{ $data['name'] }}" readonly/>
		                </div>
		                <div class="form-group">
		                    <label class="control-label">Email</label>
		                    <input class="form-control" name="email" id="email" value="{{ $data['mailid'] }}" readonly/>
		                </div>
		                <div class="form-group">
		                    <label class="control-label">Phone</label>
		                    <input class="form-control" name="phone" value="{{ $data['phoneno'] }}" readonly />
		                </div>
		                <div class="form-group">
							<label class="control-label">Package Name</label>
		                    <input class="form-control" name="productinfo" value="{{ $data['productinfo'] }}" readonly>
		                </div>
		                <div class="form-group">
		                    <input class="form-control" type="hidden" name="address1" value="{{ $data['address'] }}" readonly/>     
		                </div>
		                <div class="form-group">
		                    <input name="surl" value="{{ $data['success'] }}" size="64" type="hidden" />
		                    <input name="furl" value="{{ $data['failure'] }}" size="64" type="hidden" />  
		                    <!--for test environment comment  service provider   -->
		                    <input type="hidden" name="service_provider" value="payu_paisa" size="64" />
		                    <input name="curl" value="{{ $data['cancel'] }}" type="hidden" />
						</div>
						@if ($err)
							<div class="alert alert-danger">{{ $err }}</div>
							<a href="/mybookings" class='btn btn-sm float-left btn-info'>Go Back</a>
						@else
							<div class="form-group float-right">
								<input type="submit" value="Pay Now" class="btn btn-success" />
							</div>	
						@endif
		                
		            </form> 
        		</div>
        	</div>
        </div>
        <div class="col-md-2"></div>
    </div>
	<!-- Footer -->
	<footer>
		<hr>
		<p>Copyright &copy; {{ date('Y')}}  </p>
	</footer>
</div> 
</body>
</html>
