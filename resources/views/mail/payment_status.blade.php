<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Payment</title>
</head>
<body>
    <b>User Payment is Successfull</b>
    <br><b>Package Name :</b> {{$package->pkg_name}}
    <br><b>User Name :</b> {{$user->fname}}
    <br><b>User Email :</b> {{$user->email}}
    <br><b>Amount :</b> {{$amount}}
    <br><b>User txn id :</b> {{$txnid}}
</body>
</html>