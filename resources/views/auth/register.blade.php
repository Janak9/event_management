@extends('home.header')

@section('main-content')
<section class="agenda-section text-center" >
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>    
            <div class="col-md-8">
                <div class="card">
                    <h1>Register</h1>
                    {{-- <div class="card-header" style="font-size:30px;text-align:center">{{ __('Register') }}</div> --}}
                    <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="fname" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>
                            <div class="col-md-6">
                                <input id="fname" type="text" placeholder="FirstName" class="form-control @error('fname') is-invalid @enderror" name="fname" value="{{ old('fname') }}" maxlength="15" autocomplete="fname" autofocus>
                                @error('fname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                                <label for="lname" class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>
                                <div class="col-md-6">
                                    <input id="lname" type="text" placeholder="LastName" class="form-control @error('lname') is-invalid @enderror" name="lname" value="{{ old('lname') }}"  maxlength="15" autocomplete="lname" autofocus>
                                    @error('lname')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                            <div class="col-md-6">
                                <input id="email" type="email" placeholder="E-mail" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" maxlength="50" required autocomplete="email">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                            <div class="col-md-6">
                                <input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" minlength="8" maxlength="10" required autocomplete="new-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" placeholder="Confirm Password" class="form-control" name="password_confirmation" minlength="8" maxlength="10" required autocomplete="new-password">
                            </div>
                        </div>
                        <div class="form-group row">
                                <label for="contact_no" class="col-md-4 col-form-label text-md-right">{{ __('Contact No') }}</label>
                                <div class="col-md-6">
                                    <input id="contact_no" type="text" placeholder="Contact No" class="form-control @error('contact_no') is-invalid @enderror" name="contact_no" value="{{ old('contact_no') }}" minlength="10" maxlength="17" pattern="[0-9]+" autocomplete="contact_no" autofocus>
                                    @error('contact_no')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                        </div>
                        <div class="form-group row">
                                <label for="profile" class="col-md-4 col-form-label text-md-right">{{ __('Profile') }}</label>
                                <div class="col-md-6">
                                    <input id="profile" type="file" class="@error('profile') is-invalid @enderror" name="profile" value="{{ old('profile') }}" required autocomplete="profile" autofocus>
                                    @error('profile')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                        </div><br>
                        <div class="form-group row mb-0">
                            <div class="col-md-12 offset-md-4">
                                <input type="reset" class="btn btn-primary" value="Reset" name="reset" />
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
