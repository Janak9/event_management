<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('uid');
            $table->foreign('uid')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('eid');
            $table->foreign('eid')->references('id')->on('events')->onDelete('cascade')->onUpdate('cascade');
            $table->string('type',8);
            $table->string('pkg_name',50);
            $table->text('description');
            $table->string('address',100);
            $table->string('landmark',30);
            $table->string('country',20);
            $table->string('state',20);
            $table->string('city',20);
            $table->string('contact_no',17);
            $table->float('price_per_person',8,2);
            $table->float('deposite_per_person',8,2)->default(0);
            $table->integer('person_capacity')->length(4);
            $table->dateTime('registration_end_date');
            $table->dateTime('start_datetime');
            $table->dateTime('end_datetime');
            $table->text('image');
            $table->integer('status')->length(1);
            $table->softDeletes()->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
