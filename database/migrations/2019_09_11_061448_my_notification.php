<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MyNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('my_notifications', function (Blueprint $table) {
            $table->bigIncrements('id'); 
            $table->unsignedBigInteger('from_uid');
            $table->foreign('from_uid')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('to_uid');
            $table->foreign('to_uid')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('pkg_book_id');
            $table->foreign('pkg_book_id')->references('id')->on('pkg_bookings')->onDelete('cascade')->onUpdate('cascade');
            $table->string("msg");
            $table->integer('read_status')->length(1)->default(0);
            $table->softDeletes()->nullable();
            // $table->timestamps();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('my_notifications');
    }
}
