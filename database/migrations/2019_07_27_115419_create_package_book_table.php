<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageBookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pkg_bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('uid');
            $table->foreign('uid')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('pid');
            $table->foreign('pid')->references('id')->on('packages')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('total_persons')->length(4);
            $table->float('total_amount',8,2);
            $table->float('deposite',8,2);
            $table->integer('payment_status')->length(1)->default(0);
            $table->integer('status')->length(1)->default(0);
            $table->softDeletes()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pkg_books');
    }
}
