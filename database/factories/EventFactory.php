<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use App\models\Events;
use Faker\Generator as Faker;

$factory->define(Events::class, function (Faker $faker) {
    return [
        //  
        'uid' => '1',
        'event_name' => $faker->name,

    ];
});
