<?php

use Illuminate\Database\Seeder;
use App\models\Events;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $event = factory(Events::class, 5)->create()->each(function($u){
            $u->save();
        });
    }
}
