## requirements
php >=7.2
laravel >=5.8

## step
1. clone project
2. composer install
3. php artisan key:generate
4. php artisan migrate
5. php artisan serve

## clear cache:-
1. php artisan route:clear
2. php artisan config:clear
3. php artisan cache:clear
4. php artisan config:cache

