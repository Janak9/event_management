<?php

namespace App\Console\Commands;
use App\models\Events;

use Illuminate\Console\Command;

class add extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:event';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->line("hello");
        //insert 
        Events::insert([
            'event_name' => 'xyz',
            'uid' => 1,
        ]);

    }
}
