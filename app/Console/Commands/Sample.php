<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Sample extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Sample:start {user} {--difficulty=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->line("Information");
        $this->info("Information");
        $this->comment("This is Comment");
        $this->question("This Question");
        $this->error("this is error");

         //ask() use for only question type statements
         $email=$this->ask("What is your Email address??");
         $this->line($email);//it will print email

         $winner = $this->choice(
            'Who is the best football team?',
            ['Gators', 'Wolverines']
            );
            $this->line($winner);


        $ans=$this->anticipate("its anticipate?",["radhe","shradhdha"]);
        $this->line($ans);

        if($this->confirm("Do you want to confirm?"))
        {
            $this->line("confirm");
        }
        else
        {
            $this->line("not confirm");
        }

        $difficulty=$this->option('difficulty');
        $this->line($difficulty);

        $user=$this->argument('user');
        $this->line($user);
       // $this->line('welcome'.$this->argument('user').",starting test in difficulty:".$difficulty);
  
       $headers = ['Name', 'Email'];
       $data = [
       ['Dhriti', 'dhriti@amrit.com'],
       ['Moses', 'moses@gutierez.com']
       ];

       $this->table($headers, $data);

       $totalUnits = 20;
       $this->output->progressStart($totalUnits);
       for ($i = 0; $i < $totalUnits; $i++) {
           sleep(1);
           $this->output->progressAdvance();
       }
       $this->output->progressFinish();
  


    }
}
