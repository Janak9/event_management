<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use App\User;

class UserDetails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UserDetails:show';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userlist = DB::select('select fname,id from users');
        $this->line("**********************");
        $this->line("Users List");
        $this->line("**********************");
            // foreach($userlist as $u)
            // {
            //     $this->line($u->fname);
            // }
        //$fnames = array_column($userlist,'fname');
        //$ids = array_column($userlist,'id');
        //var_dump($ids);
        $display_name = array_map(function ($data){
            return $data->fname."-".$data->id ;
        },$userlist);
        $name_index = $this->choice('Which user details you want to see?', $display_name);
        $id=substr(strrchr($name_index,'-'),1);
        $sel_option = $this->choice('choose options?', ['event','packages']);
        if($sel_option=='event')
        {
            $this->line("-----------Events List-----------");
            $eventlist = DB::select('select event_name from events where uid='.$id);
            foreach($eventlist as $e)
            {
                $this->line($e->event_name);
            }
        }
        if($sel_option=='packages')
        {
            $this->line("-----------Packages List-----------");
            $pkglist = DB::select('select pkg_name from packages where uid='.$id);
            foreach($pkglist as $pkg)
            {
                $this->line($pkg->pkg_name);
            }
        }

    }
}
