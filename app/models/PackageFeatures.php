<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PackageFeatures extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable=[
        'uid', 'pid', 'icon', 'description', 'deleted_at'
    ];


    public function user()
    {
        return $this->belongsTo('App\User','uid');
    }

    public function packages()
    {
        return $this->belongsTo('App\models\Packages','pid');
    }
}
