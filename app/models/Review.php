<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable=[
        'uid','pid','rating','review','deleted_at'
    ];

    public function packages()
    {
        return $this->belongsTo('App\models\Packages','pid');
    }
    public function users()
    {
        return $this->belongsTo('App\User','uid');
    }

}
