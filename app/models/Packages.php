<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;
use App\models\Review;

class Packages extends Model
{
    use SoftDeletes;
    use SearchableTrait;
    protected $dates = ['deleted_at'];

    protected $fillable=[
        'uid','eid','type','pkg_name','description','address','landmark','country','state','city','contact_no','price_per_person','deposite_per_person','person_capacity', 'available_seat', 'registration_end_date','start_datetime','end_datetime','status','deleted_at'
    ];

    // laravel scout
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
             'pkg_name' => 13,
             'description' => 12,
             'country' => 11,
             'state' => 10,
             'city' => 9,
             'type' => 8,
             'address' => 7,
             'landmark' => 6,
             'price_per_person' => 6,
             'person_capacity' => 6,
             'registration_end_date' => 6,
             'start_datetime' => 6,
             'end_datetime' => 6
        ]

    ];

    public function events()
    {
        return $this->belongsTo('App\models\Events','eid');
    }
    
    public function package_features()
    {
        return $this->hasMany('App\models\PackageFeatures', 'pid');
    }

    public function reviews()
    {
        return $this->hasMany('App\models\Review');
    }

    public function pkg_bookings()
    {
        return $this->hasMany('App\models\Booking');
    }

    public function count_review($pid)
    {
        return Review::where('pid','=',$pid)->count();
    }

    public function users()
    {
        return $this->belongsTo('App\User','uid');
    }
}
