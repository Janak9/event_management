<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Events extends Model
{
    //to get deleted data
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [ 'uid', 'event_name','deleted_at' ];

    public function packages()
    {
        return $this->hasMany('App\models\Packages');

    }
    public function pkg_bookings()
    {
        return $this->hasMany('App\models\Booking');

    }
}
