<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable=[
        'uid',
        'pkg_book_id',
        'txn_id',
        'amount',
        'status'  
    ];

    public function users()
    {
        return $this->belongsTo('App\User','uid');
    }

    public function pkg_bookings()
    {
        return $this->belongsTo('App\models\Booking','pkg_book_id');
    }


}
