<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Booking extends Model
{
    //to get deleted data
    protected $table='pkg_bookings';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [ 'uid', 'pid','total_persons','total_amount','deposite','payment_status','status','deleted_at' ];
    
    public function packages()
    {
        return $this->belongsTo('App\models\Packages','pid');
    } 
    public function events()
    {
        return $this->belongsTo('App\models\Events','eid');
    }
    public function users()
    {
        return $this->belongsTo('App\User','uid');
    }
    public function payments()
    {
        return $this->hasOne('App\models\Payment','pkg_book_id');
    }




}
