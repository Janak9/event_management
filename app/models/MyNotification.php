<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MyNotification extends Model
{
    //to get deleted data
    use SoftDeletes;

    public $timestamps = false;
    
    protected $dates = ['deleted_at'];

    protected $fillable = [ 'from_uid', 'to_uid', 'pkg_book_id', 'msg', 'deleted_at' ];

    public function from_user()
    {
        return $this->belongsTo('App\User','from_uid');
    }

    public function to_user()
    {
        return $this->belongsTo('App\User','to_uid');
    }

    public function pkg_booking()
    {
        return $this->belongsTo('App\models\Booking','pkg_book_id');
    }
    
}
