<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    // internaally call by laravel and import from MustVerifyEmail Class 
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fname','lname','email','password','dob','address','city','state','country','contact_no','profile','payumoney_key', 'payumoney_salt',
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    // this array field is not display when we fetch data/print
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    public function getFullNameAttribute() {
        return $this->fname . ' ' . $this->lname;
    }
    
    public function events()
    {
        return $this->hasMany('App\models\Events','uid');
    }

    public function packages()
    {
        return $this->hasMany('App\models\Packages','uid');
    }
    
    public function package_features()
    {
        return $this->hasMany('App\models\PackageFeatures','uid');
    }

    public function review()
    {
        return $this->hasMany('App\models\Review','uid');
    }

    public function pkg_bookings()
    {
        return $this->hasMany('App\models\Booking','uid');
    }

    public function payments()
    {
        return $this->hasMany('App\models\Payment','uid');
    }

}
