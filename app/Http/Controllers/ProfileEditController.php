<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ProfileEditController extends Controller
{
    public function index(Request $request)
    {
        $user=Auth::user();
        return view("home/profile_edit",['user' => $user]);
    }

   /* public function update(Request $request)
    {
        $id=Auth::user()->id;
        $profile = User::find($id)->update($request->all());
        return redirect("/"); 
    }*/


    public function update(Request $request)
    {
        $request->validate([
            'fname' => ['required','alpha','max:20'],
            'lname' => ['required','alpha','max:20'],
            'email' => ['required','string','email','max:30'],
            'contact_no' => ['required','digits:10'],   
            'profile' => ['mimes:jpeg,bmp,png,jpg,svg'],
        ],$message=[
            'required' => 'The :attribute field is required.',
            'size'    => 'The :attribute must be exactly :size.',
            'between' => 'The :attribute value :input is not between :min - :max.',
        ]);

        $id=Auth::user()->id;
        $user=User::find($id);
        $user->fname=$request->fname;
        $user->lname=$request->lname;
        $user->email=$request->email;
        $user->dob=$request->dob;
        $user->address=$request->address;
        $user->country=$request->country;
        $user->state=$request->state; 
        $user->city=$request->city;
        $user->contact_no=$request->contact_no;
        $user->payumoney_key = $request->payumoney_key;
        $user->payumoney_salt = $request->payumoney_salt;
         if($request->hasFile('profile')){
            $imagename = time().'.'.request()->profile->getClientOriginalExtension();
            request()->profile->move(public_path('home_res/images/user/'),$imagename);
            if(!empty($user->profile)){
                unlink(public_path('home_res/images/user/').$user->profile);
            }
            $user->profile = $imagename;
        }
        //request()->except(['password','profile']);
        $user->save();
      //  $profile = User::find($id)->update($user);
        return redirect("/");  
    }

    public function add_payUMoney_key(Request $request)
    {
        if(@$request->payumoney_key){
            $user = Auth::user();
            $user->payumoney_key = $request->payumoney_key;
            $user->payumoney_salt = $request->payumoney_salt;
            $user->save();
            return redirect('/');
        }
        return view("home/add_payumoney_key", compact('prev'));
    }

}

?>