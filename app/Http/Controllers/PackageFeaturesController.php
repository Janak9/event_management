<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\PackageFeatures;
use Illuminate\Support\Facades\Auth;
use App\models\Packages;

class PackageFeaturesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $package_features = PackageFeatures::where("uid","=",Auth::user()->id)->orderBy('id', 'DESC')->paginate(5);
        return view("home.view_package_features", compact('package_features'));
    }
    
    public function trashed_PackageFeatures()
    {
        $trashed_packagefeatures = PackageFeatures::where("uid","=",Auth::user()->id)->orderBy('deleted_at', 'DESC')->onlyTrashed()->paginate(5);
        return view("home.view_package_features", compact('trashed_packagefeatures'));
    }

    public function restore_PackageFeatures($id)
    {
        $restore_packagefeatures = PackageFeatures::where("id","=",$id)->onlyTrashed()->restore();
        return redirect("package_features")->with(['msg' => 'Restored Successfully']);
    }
    
    public function forcedel_PackageFeatures($id)
    {
        $forcedelete_packagefeatures = PackageFeatures::where("id","=",$id)->onlyTrashed()->forceDelete();
        return redirect("package_features")->with(['msg' => 'Force Delete Successfully']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $task = 'Add';
        $packages = Packages::where("uid","=",Auth::user()->id)->get();
        $submit_route = route('package_features.store');
        return view("home.add_package_features", compact('submit_route', 'task','packages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'package' => 'required',
            'description' => 'required',
            'icon' => 'required'
        ],$message=[
            'required' => 'The :attribute field is required.',
        ]);
        $package_features = new PackageFeatures();
        $package_features->uid = Auth::user()->id;
        $package_features->pid = $request->get('package');
        $package_features->icon = $request->get('icon');
        $package_features->description = $request->get('description');
        $package_features->save();
        return redirect("package_features")->with(['msg' => 'created successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $package_features = PackageFeatures::find($id);
        return view("home.view_package_features", compact('package_features'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $packages = Packages::where("uid","=",Auth::user()->id)->get();
        $package_features = PackageFeatures::find($id);
        $task = 'Edit';
        $submit_route = route('package_features.update', ['id' => $id]);
        return view("home.add_package_features", compact('package_features', 'task', 'submit_route', 'packages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'package' => 'required',
            'description' => 'required',
            'icon' => 'required'
        ],$message=[
            'required' => 'The :attribute field is required.',
        ]);
        $package_features = PackageFeatures::find($id);
        $package_features->pid = $request->get('package');
        $package_features->icon = $request->get('icon');
        $package_features->description = $request->get('description');
        $package_features->save();
        return redirect("package_features")->with(['msg' => 'Updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $package_features = PackageFeatures::find($id);
        $package_features->delete();
        return redirect("package_features")->with(['msg' => 'Deleted successfully']);;
    }
}
