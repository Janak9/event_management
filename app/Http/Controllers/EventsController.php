<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\Events;
use Illuminate\Support\Facades\Auth;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Events::where("uid","=",Auth::user()->id)->orderBy("id", "DESC")->paginate(3);
        return view("home.view_event", compact('events'));
    }

    public function trashed_event()
    {
        $trashed_events = Events::where("uid","=",Auth::user()->id)->orderBy('deleted_at', 'DESC')->onlyTrashed()->paginate(3);
        return view("home.view_event", compact('trashed_events'));
    }

    public function restore_event($id)
    {
        $restore_events = Events::where("id","=",$id)->onlyTrashed()->restore();
        return redirect("events")->with(['msg' => 'Restored Successfully']);
    }
    
    public function forcedel_event($id)
    {
        $forcedelete_events = Events::where("id","=",$id)->onlyTrashed()->forceDelete();
        return redirect("events")->with(['msg' => 'Force Delete Successfully']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $task = 'Add';
        $submit_route = route('events.store');
        return view("home.add_event", compact('submit_route', 'task'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ],$message=[
            'required' => 'The :attribute field is required.',
        ]);
        $event = new Events();
        $event->uid = Auth::user()->id;
        $event->event_name = $request->get('name');
        $event->save();
        return redirect("events")->with(['msg' => 'created successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $events = Events::find($id);
        return view("home.view_event", compact('events'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Events::find($id);
        $task = 'Edit';
        $submit_route = route('events.update', ['id' => $id]);
        return view("home.add_event", compact('event', 'task', 'submit_route'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
        ],$message=[
            'required' => 'The :attribute field is required.',
       ]);
        $event = Events::find($id);
        $event->event_name = $request->get('name');
        $event->save();
        return redirect("events")->with(['msg' => 'Updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Events::find($id);
        $event->delete();
        return redirect("events")->with(['msg' => 'Deleted successfully']);;
    }
}
