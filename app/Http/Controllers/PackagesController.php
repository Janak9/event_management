<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\Packages;
use App\models\Events;
use Illuminate\Support\Facades\Auth;

class PackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = Packages::where("uid","=",Auth::user()->id)->orderBy('id', 'DESC')->paginate(3);
        return view("home.view_package", compact('packages'));
    }
    public function trashed_pkg()
    {
        $trashed_packages = Packages::where("uid","=",Auth::user()->id)->orderBy('deleted_at', 'DESC')->onlyTrashed()->paginate(3);
        return view("home.view_package", compact('trashed_packages'));
    }
    public function restore_pkg($id)
    {
        $restore_packages = Packages::where("id","=",$id)->onlyTrashed()->restore();
        return redirect("packages")->with(['msg' => 'Restored Successfully']);
        //return view("home.view_package", compact('trashed_packages'));
        //$submit_route = route('packages.index');
        //$packages = Packages::where("uid","=",Auth::user()->id)->get();
        //return view("home.view_package", compact('submit_route','packages'));
        //return redirect('packages.index');
    }
    public function forcedel_pkg($id)
    {
        $forcedelete_packages = Packages::where("id","=",$id)->onlyTrashed()->forceDelete();
        return redirect("packages")->with(['msg' => 'Force Delete Successfully']);
        // dd($forcedelete_packages);
       // $submit_route = route('packages.index');
       // $packages = Packages::where("uid","=",Auth::user()->id)->get();
       // return view("home.view_package", compact('submit_route','packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $task = 'Add';
        $events = Events::where("uid","=",Auth::user()->id)->get();
        $submit_route = route('packages.store');
        return view("home.add_package", compact('submit_route', 'task', 'events'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'type' => 'required',
            'pkg_name' => 'required',
            'description' => 'required',
            'address' => 'required',
            'landmark'  => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'contact_no' => 'required|digits:10',
            'price_per_person' => 'required',
            'deposite_per_person' => 'required|lte:price_per_person',
            'person_capacity' => 'required',
            'registration_end_date' => 'required|date|before:start_datetime',
            'start_datetime' => 'required|before:end_datetime',
            'end_datetime' => 'required|after:start_datetime',
            'image' => ['required','mimes:jpeg,bmp,png,jpg,svg'],
        ],$message=[
            'required' => 'The :attribute field is required.',
            'size'    => 'The :attribute must be exactly :size.',
            'between' => 'The :attribute value :input is not between :min - :max.',
        ]);

        $imagename = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('home_res/images/package/'),$imagename);

        $package = new Packages();
        $package->uid = Auth::user()->id;
        $package->eid = $request->get('event');
        $package->type = $request->get('type');
        $package->pkg_name = $request->get('pkg_name');
        $package->description = $request->get('description');
        $package->address = $request->get('address');
        $package->landmark = $request->get('landmark');
        $package->country = $request->get('country');
        $package->state = $request->get('state');
        $package->city = $request->get('city');
        $package->contact_no = $request->get('contact_no');
        $package->price_per_person = $request->get('price_per_person');
        $package->deposite_per_person = $request->get('deposite_per_person');
        $package->person_capacity = $request->get('person_capacity');
        $package->available_seat = $request->get('person_capacity');
        $package->registration_end_date = $request->get('registration_end_date');
        $package->start_datetime = $request->get('start_datetime');
        $package->end_datetime = $request->get('end_datetime');
        $package->image = $imagename;
        $package->status = 0;
        $package->save();
        return redirect("packages")->with(['msg' => 'created successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package = Packages::find($id);
       // dd($package);
        $task = 'Edit';
        $events = Events::where("uid","=",Auth::user()->id)->get();
        $submit_route = route('packages.update', ['id' => $id]);
        return view("home.add_package", compact('package', 'task', 'submit_route', 'events'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'type' => 'required',
            'pkg_name' => 'required',
            'description' => 'required',
            'address' => 'required',
            'landmark'  => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'contact_no' => 'required|digits:10',
            'price_per_person' => 'required',
            'deposite_per_person' => 'required|lte:price_per_person',
            'person_capacity' => 'required',
            'registration_end_date' => 'required|date|before:start_datetime',
            'start_datetime' => 'required|before:end_datetime',
            'end_datetime' => 'required|after:start_datetime',
            'image' => ['mimes:jpeg,bmp,png,jpg,svg'],
        ],$message=[
            'required' => 'The :attribute field is required.',
            'size'    => 'The :attribute must be exactly :size.',
            'between' => 'The :attribute value :input is not between :min - :max.',
        ]);

        $package = Packages::find($id);
        $package->eid = $request->get('event');
        $package->type = $request->get('type');
        $package->pkg_name = $request->get('pkg_name');
        $package->description = $request->get('description');
        $package->address = $request->get('address');
        $package->landmark = $request->get('landmark');
        $package->country = $request->get('country');
        $package->state = $request->get('state');
        $package->city = $request->get('city');
        $package->contact_no = $request->get('contact_no');
        $package->price_per_person = $request->get('price_per_person');
        $package->deposite_per_person = $request->get('deposite_per_person');
        $package->person_capacity = $request->get('person_capacity');
        $package->available_seat = $request->get('person_capacity');
        $package->registration_end_date = $request->get('registration_end_date');
        $package->start_datetime = $request->get('start_datetime');$package->end_datetime = $request->get('end_datetime');
        if($request->hasFile('image')){
            $imagename = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('home_res/images/package/'),$imagename);
            if(!empty($package->image)){
                unlink(public_path('home_res/images/package/').$package->image);
            }
            $package->image = $imagename;
        }
        $package->save();
        return redirect("packages")->with(['msg' => 'Updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $package = Packages::find($id);
        $package->delete();
        return redirect("packages")->with(['msg' => 'Deleted successfully']);
    }
}
