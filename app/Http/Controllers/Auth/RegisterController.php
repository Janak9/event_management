<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'fname' => ['required','alpha','max:20'],
            'lname' => ['required','alpha','max:20'],
            'email' => ['required','string','email','max:30','unique:users'],
            'password' => ['required','string','min:8','confirmed'],
            'contact_no' => ['required'],   
            'profile' => ['required','mimes:jpeg,bmp,png,jpg,svg'],
        ],$message=[
            'required' => 'The :attribute field is required.',
            'size'    => 'The :attribute must be exactly :size.',
            'between' => 'The :attribute value :input is not between :min - :max.',
        ]);

        $attributeNames = array(
            'fname' => 'First Name',
            'lname' => 'Last Name',
            'contact_no' => 'Contact No',
        );

        $validator->setAttributeNames($attributeNames);
        return $validator;
    }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $imagename = time().'.'.request()->profile->getClientOriginalExtension();
        request()->profile->move(public_path('home_res/images/user/'),$imagename);
        return User::create([     
            'fname' => $data['fname'],
            'lname' => $data['lname'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'contact_no' => $data['contact_no'],
            'profile' =>  $imagename,
        ]);
    }
}
