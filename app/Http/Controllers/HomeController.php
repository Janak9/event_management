<?php

namespace App\Http\Controllers;
use App\User;
use App\models\Packages;
use App\models\Events;
use App\models\Review;
use App\models\Booking;
use App\models\MyNotification;
use App\models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Mail;
// use App\Mail\VerificationMail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $packages = Packages::all();
        return view("home");
    }

    public function home()
    {
        $packages = Packages::with('events')->whereRaw('registration_end_date >= curdate()')->limit(3)->orderBy('price_per_person','DESC')->orderBy('registration_end_date', 'ASC')->get();
        //dd($packages);
        $images = Packages::get();
        $reviews = Review::all();
        $total_events = Events::all()->count();
        $total_payments = Payment::all()->count();
        $total_users = User::all()->count();
        $total_packages = Packages::all()->count();
        return view("home/index",compact('packages','reviews','images','total_users','total_packages','total_events','total_payments'));
    }

    public function mail_demo(){
        $data = array();
        $user = User::find(9);
        // Mail::to($user['email'])->send(new VerificationMail($user));

        $data['user'] = $user;
        $data['token'] = \Illuminate\Support\Str::uuid();
        Mail::send('mail.demo', $data, function($message) {
            $message->to('vaghelajanak97@gmail.com', 'Janak Vaghela')
                    ->subject('Test');
        });
 
        if (Mail::failures())
           return 'Sorry! Please try again later';
        else
           return 'Great! Successfully send in your mail';
    }

    public function EventList()
    {
        $packages = Packages::with('events')->whereRaw('registration_end_date >= curdate()')->orderBy('price_per_person','DESC')->orderBy('registration_end_date', 'ASC')->paginate(4);
        return view("home/event_list",compact('packages'));
    }

    public function eventDetail(Request $request,$id)
    {
        if($request->get('reviewsubmit'))
        {
            // review can only give by paid user
            // $booked = Booking::where('payment_status','=',1)->where('pid','=',$id)->where('uid','=', Auth::user()->id)->count();
            
            // review can only give by user who booked package
            $booked = Booking::where('pid','=',$id)->where('uid','=', Auth::user()->id)->count();
            if($booked > 0){
                $review = new Review();
                $review->uid = Auth::user()->id;
                $review->pid = $id;
                $review->rating = $request->get('rating');
                $review->review = $request->get('review');
                $review->save();
            }else{
                $msg = "You can not give review until you book it...!";
            }
        }

        $reviews = Review::where('pid','=',$id)->with('users')->orderBy('rating','DESC')->get();
        $pkg_details  = Packages::with('users')->find($id);
        $features = $pkg_details->package_features;
        
        return view("home/view_eventDetail",compact('pkg_details','reviews','features', 'msg'));
    }

    //for search
    public function eventSearch(Request $request)
    {
        //it will return value which we search
        if($request->has('search')){
            if($request->get('search_type') == "1"){
                $packages = Packages::whereRaw('registration_end_date >= curdate()')->whereHas('events', function ($query) use ($request) {
                    $query->where('event_name', 'like', "%{$request->get('search')}%");
                })->paginate(2);
            }elseif($request->get('search_type') == "2"){
                $packages = Packages::search($request->get('search'),null,true)->whereRaw('registration_end_date >= curdate()')->paginate(2);
            }
        }else{
            $packages = Packages::paginate(2);
        }
        return view("home/event_list", compact('packages'));
    }

    public function contact(Request $request)
    {
        return view("home/contact_us"); 
    }

    public function aboutus(Request $request)
    {
        $total_events = Events::all()->count();
        $total_payments = Payment::all()->count();
        $total_users = User::all()->count();
        $total_packages = Packages::all()->count();
        return view("home/about_us",compact('total_users','total_packages','total_events','total_payments')); 
    }

    public function booking(Request $request,$id)
    {
        $package =  Packages::find($id);
        if($request->get('btn_sub'))
        {
            $user = Auth::user();
            if($package->available_seat >= $request->get('totalper')){
                $booking = new Booking();
                $booking->uid = $user->id;
                $booking->pid = $id;
                $booking->total_persons = $request->get('totalper');
                $totalamt=$request->get('totalper')*$request->get('price_per_person');
                $booking->total_amount = $totalamt;
                $booking->deposite = $request->get('deposite')*$request->get('totalper');
                if($package->type == "public")
                    $booking->status = 1;
                $booking->save();

                $notify = new MyNotification();
                $notify->from_uid = $user->id;
                $notify->to_uid = $package->uid;
                $notify->pkg_book_id = $booking->id;
                $notify->msg = $package->pkg_name ." was booked by ". $user->full_name;
                $notify->save();
                return redirect("/mybookings");
            }else{
                return back()->with('msg', $request->get('totalper').' seats are not available...!')->withInput();
            }
        }
        return view("home/booking",compact('package'));
    }
 
    public function booking_cancel(Request $request, $id)
    {
        $booking =  Booking::with('packages')->find($id);
        $booking->status = 3;
        $booking->save();

        $notify = new MyNotification();
        $notify->from_uid = Auth::user()->id;
        $notify->to_uid = $booking->packages->uid;
        $notify->pkg_book_id = $id;
        $notify->msg = $booking->packages->pkg_name ." was canceled by ". Auth::user()->full_name;
        $notify->save();
        return redirect("/mybookings");
    }
 
    public function myBookings(Request $request)
    {
        $all_booking = Booking::where('uid','=',Auth::user()->id)->with('users')->orderBy('created_at','Desc')->get();
        return view("home/my_bookings",compact('all_booking'));
    }
    
    public function invoice(Request $request, $id)
    {
        $booking = Booking::find($id);
        $user = User::find($booking->uid);
        $pkg = Packages::find($booking->pid);
        $payment = Payment::where('pkg_book_id', '=', $booking->id)->first();
        return view("home/invoice",compact('booking','user', 'pkg', 'payment'));
    }
    
    public function get_notification(Request $request, $id){
        $notifications = MyNotification::where('to_uid', '=', $id)->orderBy('created_at', 'desc')->get();
        return $notifications->toJson();
    }

    public function notification_details(Request $request, $id){
        $notification = MyNotification::with('pkg_booking')->with('from_user')->find($id);
        $package = Packages::find($notification->pkg_booking->pid);
        if($notification->read_status == 0){
            $notification->read_status = 1;
            $notification->save();
        }

        if($package->uid != Auth::user()->id)
            return redirect("/mybookings");

        if($request->get('btn_submit'))
        {
            $pkg_booked = Booking::find($notification->pkg_booking->id);
            $pkg_booked->status = $request->get('btn_submit');
            $pkg_booked->save();
            $notification->pkg_booking->status = $request->get('btn_submit');

            $notify = new MyNotification();
            $notify->from_uid = $notification->to_uid;
            $notify->to_uid = $notification->from_uid;
            $notify->pkg_book_id = $notification->pkg_book_id;
            $status = "";
            if ($request->get('btn_submit') == 1)
                $status = "accepted";
            else
                $status = "denied";
            $notify->msg = $package->pkg_name ." was " . $status;
            $notify->save();
        }

        return view("home/notification_details", compact('notification', 'package'));
    }
    
}
