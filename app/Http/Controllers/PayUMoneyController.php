<?php

namespace App\Http\Controllers;
use Mail;
use App\User;
use App\models\Payment;
use App\models\Booking;
use App\models\Packages;
use App\models\MyNotification;
use Illuminate\Http\Request;
use DateTime;
class PayUMoneyController extends Controller
{
  	public function index(Request $request, $uid, $pkg_book_id){
		$request->session()->put("uid",$uid);
		$request->session()->put("pkg_book_id",$pkg_book_id);
		
		$booking = Booking::find($pkg_book_id);
		$package = Packages::find($booking->pid);
		$err = NULL;

		date_default_timezone_set("Asia/Kolkata");
		$now = new DateTime();
		$now = date_format($now,"Y-m-d H:i:s"); // must be same as db		
		if($package->registration_end_date < $now){
			$err = "Sorry, Registration time is over :(";
		}else if($package->available_seat < $booking->total_persons){
			$err = $booking->total_persons . " seats are not available...! Now only " . $package->available_seat . " seats are available. :)";
		}else{
			$package->available_seat -= $booking->total_persons;
			$package->save();
		}
		
		$pkg_owner = User::find($booking->packages->uid);
		$user = User::find($uid);

		$amount = $booking->deposite;
		$product_info = $booking->packages->pkg_name;
		$customer_name = $user->fname;
		$customer_email = $user->email;
		$customer_mobile = $user->contact_no;
		$customer_address = "xyz";
		
		$MERCHANT_KEY = $pkg_owner->payumoney_key; //change  merchant with yours
		$SALT = $pkg_owner->payumoney_salt;  //change salt with yours 

		$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		//optional udf values 
		$udf1 = '';
		$udf2 = '';
		$udf3 = '';
		$udf4 = '';
		$udf5 = '';
		
		$hashstring = $MERCHANT_KEY . '|' . $txnid . '|' . $amount . '|' . $product_info . '|' . $customer_name . '|' . $customer_email . '|' . $udf1 . '|' . $udf2 . '|' . $udf3 . '|' . $udf4 . '|' . $udf5 . '||||||' . $SALT;
		$hash = strtolower(hash('sha512', $hashstring));
		
		$success = route('payment-status');
		$fail =  route('payment-status');
		$cancel =  route('payment-status');
		
		$data = array(
			'mkey' => $MERCHANT_KEY,
			'tid' => $txnid,
			'hash' => $hash,
			'amount' => $amount,           
			'name' => $customer_name,
			'productinfo' => $product_info,
			'mailid' => $customer_email,
			'phoneno' => $customer_mobile,
			'address' => $customer_address,
			'action' => "https://sandboxsecure.payu.in", //for live change action  https://secure.payu.in
			'success' => $success,
			'failure' => $fail,
			'cancel' => $cancel,
			'booking' => $booking,
		);
		//  dd($data);
      	return view("confirmation",compact('data','err'));
    }

    public function status(Request $request){
        $status = $request->get('status');
        if (empty($status)) {
			return "Not authorized!";
		}
		$pkg_book_id = $request->session()->get("pkg_book_id");
		$booking = Booking::find($pkg_book_id);
		$package = Packages::find($booking->pid);
		
		/*
		$firstname = $request->get('firstname');
		$posted_hash = $request->get('hash');
		$key = $request->get('key');
		$productinfo = $request->get('productinfo');
		$email = $request->get('email');
		$salt = "qS5v3GYk46"; //  Your salt
		$add = $request->get('additionalCharges');
		if (isset($add)) {
			$additionalCharges = $request->get('additionalCharges');
			$retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
		} 
		else {
			$retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
		}
		$data['hash'] = hash("sha512", $retHashSeq);
		$data['amount'] = $amount;
		$data['txnid'] = $txnid;
		$data['posted_hash'] = $posted_hash;
		$data['status'] = $status;
		*/
		if($status == 'success'){
			$pkg_owner = User::find($booking->packages->uid);
			$uid = $request->session()->get("uid");
		
			$amount = $request->get('amount');
			$txnid = $request->get('txnid');		
			$arr=array(
				"amount"=>$amount,
				"uid"=>$uid,
				"pkg_book_id"=>$pkg_book_id,
				"txn_id"=>$txnid,
				"status"=>1
			);
			$payment=Payment::create($arr);

			$user = User::find($uid);
			$user_email = $user->email;
			
			// mail send to user about payment status
			$data = array();
			$data['user'] = $user;
			$data['package'] = $package;
			$data['amount'] = $amount;
			$data['txnid'] = $txnid;
			$emails = [$user_email, $pkg_owner->email];
			Mail::send('mail.payment_status', $data, function($message) use ($emails) {
				$message->to($emails)->subject('Payment Status');
			});
			
			// send Notification to pkg owner
			$notify = new MyNotification();
			$notify->from_uid = $uid;
			$notify->to_uid = $pkg_owner->id;
			$notify->pkg_book_id = $pkg_book_id;
			$notify->msg = $user->full_name . " paid for " . $package->pkg_name;
			$notify->save();

			$booking = Booking::where('id','=',$pkg_book_id)->update(['payment_status'=>1]);
			$request->session()->forget("uid");
			$request->session()->forget("pkg_book_id");
			return view('success',compact('data'));
		}
		else{
			// transaction failed so release seats
			$package->available_seat += $booking->total_persons;
			$package->save();
			return view('failure',compact('data')); 
		}     
    }
}
