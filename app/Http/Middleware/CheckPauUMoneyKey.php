<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckPauUMoneyKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if((Auth::user()->payumoney_key == NULL || Auth::user()->payumoney_key == "") && 
        (Auth::user()->payumoney_salt == NULL || Auth::user()->payumoney_salt == ""))
            return redirect('payumoney_key');
        
        return $next($request);
    }
}
