-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 20, 2019 at 09:54 PM
-- Server version: 5.7.27-0ubuntu0.19.04.1
-- PHP Version: 7.2.19-0ubuntu0.19.04.2

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Event`
--

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `uid`, `event_name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'Birthday', NULL, '2019-10-12 08:15:47', '2019-10-12 08:15:47'),
(2, 1, 'Anniversary', NULL, '2019-10-12 08:16:30', '2019-10-12 08:16:30'),
(3, 3, 'Fresher', NULL, '2019-10-12 10:09:18', '2019-10-12 10:09:18'),
(4, 3, 'Baby Shower', NULL, '2019-10-12 10:09:45', '2019-10-12 10:09:45'),
(5, 2, 'Anniversary', NULL, '2019-10-13 07:07:03', '2019-10-13 07:07:03'),
(6, 2, 'Music Party', NULL, '2019-10-13 08:05:48', '2019-10-13 08:45:28'),
(7, 2, 'Wedding', NULL, '2019-10-13 08:46:30', '2019-10-13 08:46:56'),
(8, 4, 'Navratri', NULL, '2019-10-13 13:35:30', '2019-10-13 13:35:30'),
(9, 4, 'New Year Party', NULL, '2019-10-13 13:37:56', '2019-10-13 13:37:56'),
(10, 1, 'College Events', NULL, '2019-10-18 01:11:15', '2019-10-18 01:11:15'),
(11, 3, 'Business Conference', NULL, '2019-10-18 01:12:12', '2019-10-18 01:12:12'),
(12, 2, 'Farewell', NULL, '2019-10-18 01:55:25', '2019-10-18 01:55:25'),
(14, 3, 'DJ party', '2019-10-20 06:41:40', '2019-10-20 06:41:32', '2019-10-20 06:41:40'),
(15, 3, 'bday', '2019-10-20 06:42:35', '2019-10-20 06:42:32', '2019-10-20 06:42:35');

--
-- Dumping data for table `my_notifications`
--

INSERT INTO `my_notifications` (`id`, `from_uid`, `to_uid`, `pkg_book_id`, `msg`, `read_status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 9, 1, 1, '20th Birthday Party was booked by Dhruv Kanapara', 1, NULL, '2019-10-19 07:20:16', '2019-10-19 07:20:16'),
(2, 9, 1, 1, 'Dhruv Kanapara paid for 20th Birthday Party', 1, NULL, '2019-10-19 07:21:39', '2019-10-19 07:21:39'),
(3, 7, 1, 2, '20th Birthday Party was booked by Sagar Dhamesiya', 1, NULL, '2019-10-20 06:01:47', '2019-10-20 06:01:47'),
(4, 1, 7, 2, '20th Birthday Party was accepted', 1, NULL, '2019-10-20 06:02:04', '2019-10-20 06:02:04'),
(5, 7, 1, 2, 'Sagar Dhamesiya paid for 20th Birthday Party', 1, NULL, '2019-10-20 06:03:16', '2019-10-20 06:03:16'),
(6, 8, 2, 3, 'Dream Wedding was booked by Gaurav Thoriya', 1, NULL, '2019-10-20 06:33:10', '2019-10-20 06:33:10'),
(7, 2, 8, 3, 'Dream Wedding was accepted', 1, NULL, '2019-10-20 06:34:29', '2019-10-20 06:34:29'),
(8, 8, 2, 3, 'Gaurav Thoriya paid for Dream Wedding', 1, NULL, '2019-10-20 06:35:35', '2019-10-20 06:35:35'),
(9, 8, 1, 4, '20th Birthday Party was booked by Gaurav Thoriya', 1, NULL, '2019-10-20 06:39:47', '2019-10-20 06:39:47'),
(10, 1, 8, 4, '20th Birthday Party was accepted', 0, NULL, '2019-10-20 06:40:27', '2019-10-20 06:40:27'),
(11, 8, 1, 4, 'Gaurav Thoriya paid for 20th Birthday Party', 0, NULL, '2019-10-20 06:41:19', '2019-10-20 06:41:19'),
(12, 7, 2, 5, 'DJ Night was booked by Sagar Dhameshiya', 0, NULL, '2019-10-20 06:48:03', '2019-10-20 06:48:03'),
(13, 7, 2, 5, 'DJ Night was canceled by Sagar Dhameshiya', 1, NULL, '2019-10-20 06:48:36', '2019-10-20 06:48:36'),
(14, 7, 3, 6, 'Baby Shower was booked by Sagar Dhameshiya', 1, NULL, '2019-10-20 06:50:22', '2019-10-20 06:50:22'),
(15, 3, 7, 6, 'Baby Shower was denied', 1, NULL, '2019-10-20 06:51:34', '2019-10-20 06:51:34'),
(16, 7, 4, 7, 'New Year Celebration was booked by Sagar Dhameshiya', 0, NULL, '2019-10-20 08:45:37', '2019-10-20 08:45:37'),
(17, 7, 4, 7, 'Sagar Dhameshiya paid for New Year Celebration', 1, NULL, '2019-10-20 08:46:14', '2019-10-20 08:46:14'),
(18, 3, 2, 8, 'Dream Wedding was booked by Shradhdha Vekariya', 1, NULL, '2019-10-20 08:53:33', '2019-10-20 08:53:33'),
(19, 2, 3, 8, 'Dream Wedding was accepted', 0, NULL, '2019-10-20 08:54:23', '2019-10-20 08:54:23'),
(20, 3, 2, 8, 'Shradhdha Vekariya paid for Dream Wedding', 0, NULL, '2019-10-20 08:55:08', '2019-10-20 08:55:08'),
(21, 3, 2, 9, 'DJ Night was booked by Shradhdha Vekariya', 1, NULL, '2019-10-20 09:01:11', '2019-10-20 09:01:11'),
(22, 3, 2, 9, 'Shradhdha Vekariya paid for DJ Night', 0, NULL, '2019-10-20 09:02:06', '2019-10-20 09:02:06'),
(23, 3, 3, 10, 'Conference Meeting was booked by Shradhdha Vekariya', 0, NULL, '2019-10-20 09:05:32', '2019-10-20 09:05:32'),
(24, 3, 3, 10, 'Conference Meeting was canceled by Shradhdha Vekariya', 1, NULL, '2019-10-20 09:06:01', '2019-10-20 09:06:01'),
(25, 3, 1, 11, '20th Birthday Party was booked by Shradhdha Vekariya', 1, NULL, '2019-10-20 09:07:32', '2019-10-20 09:07:32'),
(26, 1, 3, 11, '20th Birthday Party was accepted', 0, NULL, '2019-10-20 09:08:02', '2019-10-20 09:08:02'),
(27, 1, 3, 12, 'Fresher Party was booked by Nehal Thoriya', 1, NULL, '2019-10-20 09:09:20', '2019-10-20 09:09:20'),
(28, 3, 1, 12, 'Fresher Party was accepted', 0, NULL, '2019-10-20 09:09:29', '2019-10-20 09:09:29'),
(29, 1, 3, 12, 'Nehal Thoriya paid for Fresher Party', 1, NULL, '2019-10-20 09:10:39', '2019-10-20 09:10:39'),
(30, 1, 3, 13, 'Baby Shower Function was booked by Nehal Thoriya', 1, NULL, '2019-10-20 09:13:59', '2019-10-20 09:13:59'),
(31, 3, 1, 13, 'Baby Shower Function was accepted', 1, NULL, '2019-10-20 09:14:16', '2019-10-20 09:14:16'),
(32, 1, 3, 13, 'Nehal Thoriya paid for Baby Shower Function', 0, NULL, '2019-10-20 09:14:53', '2019-10-20 09:14:53'),
(33, 1, 3, 14, 'Conference Meeting was booked by Nehal Thoriya', 1, NULL, '2019-10-20 09:32:04', '2019-10-20 09:32:04'),
(34, 3, 1, 14, 'Conference Meeting was accepted', 0, NULL, '2019-10-20 09:32:16', '2019-10-20 09:32:16'),
(35, 3, 1, 15, '20th Birthday Party was booked by Shradhdha Vekariya', 1, NULL, '2019-10-20 09:33:38', '2019-10-20 09:33:38'),
(36, 1, 3, 15, '20th Birthday Party was accepted', 0, NULL, '2019-10-20 09:33:48', '2019-10-20 09:33:48'),
(37, 3, 1, 11, '20th Birthday Party was canceled by Shradhdha Vekariya', 1, NULL, '2019-10-20 09:33:58', '2019-10-20 09:33:58'),
(38, 3, 1, 16, '1st Birthday Celebration was booked by Shradhdha Vekariya', 1, NULL, '2019-10-20 09:35:37', '2019-10-20 09:35:37'),
(39, 1, 3, 16, '1st Birthday Celebration was accepted', 0, NULL, '2019-10-20 09:35:48', '2019-10-20 09:35:48'),
(40, 3, 1, 16, 'Shradhdha Vekariya paid for 1st Birthday Celebration', 1, NULL, '2019-10-20 09:36:27', '2019-10-20 09:36:27'),
(41, 3, 1, 17, 'Morning Party was booked by Shradhdha Vekariya', 0, NULL, '2019-10-20 09:40:07', '2019-10-20 09:40:07'),
(45, 3, 1, 19, '25th Anniversary Celebration was booked by Shradhdha Vekariya', 1, NULL, '2019-10-20 09:51:54', '2019-10-20 09:51:54'),
(46, 1, 3, 19, '25th Anniversary Celebration was denied', 0, NULL, '2019-10-20 09:52:25', '2019-10-20 09:52:25'),
(47, 1, 2, 20, 'Wedding Anniversary was booked by Nehal Thoriya', 1, NULL, '2019-10-20 10:00:30', '2019-10-20 10:00:30'),
(48, 2, 1, 20, 'Wedding Anniversary was denied', 1, NULL, '2019-10-20 10:00:48', '2019-10-20 10:00:48'),
(49, 1, 2, 21, 'Memorable Farewell was booked by Nehal Thoriya', 1, NULL, '2019-10-20 10:03:23', '2019-10-20 10:03:23'),
(50, 1, 2, 21, 'Memorable Farewell was canceled by Nehal Thoriya', 1, NULL, '2019-10-20 10:03:57', '2019-10-20 10:03:57'),
(51, 1, 2, 22, 'Get Together was booked by Nehal Thoriya', 1, NULL, '2019-10-20 10:04:38', '2019-10-20 10:04:38'),
(52, 2, 3, 23, 'Baby Shower was booked by Janak Vaghela', 1, NULL, '2019-10-20 10:10:21', '2019-10-20 10:10:21'),
(53, 3, 2, 23, 'Baby Shower was accepted', 0, NULL, '2019-10-20 10:10:52', '2019-10-20 10:10:52'),
(54, 2, 1, 24, 'Annual Fuction was booked by Janak Vaghela', 0, NULL, '2019-10-20 10:11:38', '2019-10-20 10:11:38'),
(55, 2, 1, 24, 'Janak Vaghela paid for Annual Fuction', 1, NULL, '2019-10-20 10:12:18', '2019-10-20 10:12:18'),
(56, 2, 1, 25, 'Evening Party was booked by Janak Vaghela', 0, NULL, '2019-10-20 10:18:38', '2019-10-20 10:18:38'),
(57, 2, 1, 25, 'Evening Party was canceled by Janak Vaghela', 1, NULL, '2019-10-20 10:18:45', '2019-10-20 10:18:45'),
(58, 2, 3, 26, 'Conference Meeting was booked by Janak Vaghela', 1, NULL, '2019-10-20 10:19:37', '2019-10-20 10:19:37'),
(59, 3, 2, 26, 'Conference Meeting was denied', 1, NULL, '2019-10-20 10:19:46', '2019-10-20 10:19:46'),
(60, 2, 4, 27, 'New Year Celebration was booked by Janak Vaghela', 1, NULL, '2019-10-20 10:20:25', '2019-10-20 10:20:25'),
(61, 2, 4, 27, 'Janak Vaghela paid for New Year Celebration', 1, NULL, '2019-10-20 10:21:07', '2019-10-20 10:21:07'),
(62, 2, 2, 28, 'Memorable Farewell was booked by Janak Vaghela', 1, NULL, '2019-10-20 10:22:04', '2019-10-20 10:22:04'),
(63, 6, 1, 29, '20th Birthday Party was booked by Dhaval Variya', 1, NULL, '2019-10-20 10:30:13', '2019-10-20 10:30:13'),
(64, 1, 6, 29, '20th Birthday Party was accepted', 1, NULL, '2019-10-20 10:30:38', '2019-10-20 10:30:38'),
(65, 6, 1, 29, 'Dhaval Variya paid for 20th Birthday Party', 0, NULL, '2019-10-20 10:31:11', '2019-10-20 10:31:11'),
(66, 6, 2, 30, 'DJ Night was booked by Dhaval Variya', 0, NULL, '2019-10-20 10:35:13', '2019-10-20 10:35:13'),
(67, 6, 2, 31, 'Get Together was booked by Dhaval Variya', 1, NULL, '2019-10-20 10:37:27', '2019-10-20 10:37:27'),
(68, 6, 2, 31, 'Get Together was canceled by Dhaval Variya', 1, NULL, '2019-10-20 10:37:31', '2019-10-20 10:37:31'),
(69, 6, 3, 32, 'Conference Meeting was booked by Dhaval Variya', 1, NULL, '2019-10-20 10:39:21', '2019-10-20 10:39:21'),
(70, 3, 6, 32, 'Conference Meeting was accepted', 0, NULL, '2019-10-20 10:39:51', '2019-10-20 10:39:51'),
(71, 6, 3, 32, 'Dhaval Variya paid for Conference Meeting', 0, NULL, '2019-10-20 10:40:33', '2019-10-20 10:40:33'),
(72, 6, 3, 33, 'Fresher Party was booked by Dhaval Variya', 0, NULL, '2019-10-20 10:41:17', '2019-10-20 10:41:17'),
(73, 8, 2, 34, 'DJ Night was booked by Gaurav Thoriya', 0, NULL, '2019-10-20 10:48:04', '2019-10-20 10:48:04'),
(74, 8, 2, 34, 'Gaurav Thoriya paid for DJ Night', 1, NULL, '2019-10-20 10:49:27', '2019-10-20 10:49:27'),
(75, 8, 2, 35, 'Dream Wedding was booked by Gaurav Thoriya', 0, NULL, '2019-10-20 10:50:06', '2019-10-20 10:50:06'),
(76, 8, 1, 36, 'Morning Party was booked by Gaurav Thoriya', 0, NULL, '2019-10-20 10:51:26', '2019-10-20 10:51:26'),
(77, 2, 1, 37, '20th Birthday Party was booked by Janak Vaghela', 0, NULL, '2019-10-20 10:52:39', '2019-10-20 10:52:39'),
(78, 7, 1, 38, 'Evening Party was booked by Sagar Dhameshiya', 0, NULL, '2019-10-20 10:56:14', '2019-10-20 10:56:14'),
(79, 7, 2, 39, 'Wedding Anniversary was booked by Sagar Dhameshiya', 0, NULL, '2019-10-20 10:57:49', '2019-10-20 10:57:49'),
(80, 5, 2, 40, 'Dream Wedding was booked by Smita Sorthiya', 1, NULL, '2019-10-20 11:01:08', '2019-10-20 11:01:08'),
(81, 5, 2, 41, 'DJ Night was booked by Smita Sorthiya', 1, NULL, '2019-10-20 11:01:34', '2019-10-20 11:01:34'),
(82, 5, 2, 41, 'Smita Sorthiya paid for DJ Night', 0, NULL, '2019-10-20 11:02:07', '2019-10-20 11:02:07'),
(83, 5, 2, 42, 'Memorable Farewell was booked by Smita Sorthiya', 0, NULL, '2019-10-20 11:02:43', '2019-10-20 11:02:43'),
(84, 5, 3, 43, 'Baby Shower was booked by Smita Sorthiya', 1, NULL, '2019-10-20 11:03:28', '2019-10-20 11:03:28'),
(85, 3, 5, 43, 'Baby Shower was accepted', 1, NULL, '2019-10-20 11:03:55', '2019-10-20 11:03:55'),
(86, 5, 3, 43, 'Smita Sorthiya paid for Baby Shower', 0, NULL, '2019-10-20 11:04:51', '2019-10-20 11:04:51'),
(87, 5, 3, 44, 'Fresher Party was booked by Smita Sorthiya', 1, NULL, '2019-10-20 11:06:44', '2019-10-20 11:06:44'),
(88, 5, 3, 44, 'Fresher Party was canceled by Smita Sorthiya', 1, NULL, '2019-10-20 11:06:46', '2019-10-20 11:06:46'),
(89, 5, 1, 45, '1st Birthday Celebration was booked by Smita Sorthiya', 1, NULL, '2019-10-20 11:07:45', '2019-10-20 11:07:45'),
(90, 1, 5, 45, '1st Birthday Celebration was denied', 1, NULL, '2019-10-20 11:08:11', '2019-10-20 11:08:11'),
(91, 4, 2, 46, 'DJ Night was booked by Radhe Vekariya', 0, NULL, '2019-10-20 11:12:08', '2019-10-20 11:12:08'),
(92, 4, 2, 46, 'Radhe Vekariya paid for DJ Night', 0, NULL, '2019-10-20 11:13:25', '2019-10-20 11:13:25'),
(93, 4, 2, 47, 'Dream Wedding was booked by Radhe Vekariya', 0, NULL, '2019-10-20 11:14:21', '2019-10-20 11:14:21'),
(94, 4, 1, 48, '20th Birthday Party was booked by Radhe Vekariya', 1, NULL, '2019-10-20 11:14:39', '2019-10-20 11:14:39'),
(95, 4, 1, 49, '25th Anniversary Celebration was booked by Radhe Vekariya', 1, NULL, '2019-10-20 11:15:14', '2019-10-20 11:15:14'),
(96, 1, 4, 49, '25th Anniversary Celebration was accepted', 0, NULL, '2019-10-20 11:15:50', '2019-10-20 11:15:50'),
(97, 1, 4, 48, '20th Birthday Party was denied', 0, NULL, '2019-10-20 11:16:35', '2019-10-20 11:16:35'),
(98, 4, 3, 50, 'Baby Shower Function was booked by Radhe Vekariya', 0, NULL, '2019-10-20 11:17:54', '2019-10-20 11:17:54'),
(99, 4, 3, 50, 'Baby Shower Function was canceled by Radhe Vekariya', 0, NULL, '2019-10-20 11:18:02', '2019-10-20 11:18:02'),
(100, 8, 2, 51, 'Dream Wedding was booked by Gaurav Thoriya', 1, NULL, '2019-10-20 14:13:13', '2019-10-20 14:13:13'),
(101, 2, 8, 51, 'Dream Wedding was accepted', 1, NULL, '2019-10-20 14:13:53', '2019-10-20 14:13:53'),
(102, 8, 2, 51, 'Gaurav Thoriya paid for Dream Wedding', 0, NULL, '2019-10-20 14:14:43', '2019-10-20 14:14:43'),
(103, 8, 1, 36, 'Gaurav Thoriya paid for Morning Party', 0, NULL, '2019-10-20 14:55:21', '2019-10-20 14:55:21'),
(104, 2, 5, 40, 'Dream Wedding was accepted', 0, NULL, '2019-10-20 16:18:31', '2019-10-20 16:18:31');

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `uid`, `eid`, `type`, `pkg_name`, `description`, `address`, `landmark`, `country`, `state`, `city`, `contact_no`, `price_per_person`, `deposite_per_person`, `person_capacity`, `registration_end_date`, `start_datetime`, `end_datetime`, `image`, `status`, `deleted_at`, `created_at`, `updated_at`, `available_seat`) VALUES
(1, 1, 1, 'private', '1st Birthday Celebration', 'A dream birthday party is on everyone\'s bucket list! Gift them the surprise that\'s as special as the day. Whether you\'re into Superheroes or Princesses, we\'ll ensure you celebrate in your favourite fantasy world.', '4, Sultanpur Mandi Rd, Dera Village', 'Chattarpur', 'India', 'Gujarat', 'Surat', '9874455656', 200.00, 130.00, 400, '2019-12-25 00:00:00', '2020-01-22 00:00:00', '2020-01-23 07:00:00', '1570890059.jpeg', 0, NULL, '2019-10-12 08:50:59', '2019-10-20 09:35:53', 395),
(2, 1, 2, 'private', '25th Anniversary Celebration', 'Some days should never be forgotten, and this is one of them. Save your time for fun and celebrations  and give your loved ones the ultimate surprise party on their special day.', 'C-6/1, Ring Road, Near Mayapuri flyover,', 'Mayapuri', 'India', 'Gujarat', 'Baroda', '9475755520', 170.00, 100.00, 300, '2019-11-30 00:00:00', '2020-01-03 09:00:00', '2020-01-04 23:59:00', '1570890658.jpg', 0, NULL, '2019-10-12 09:00:58', '2019-10-12 09:00:58', 300),
(3, 1, 1, 'private', '20th Birthday Party', 'we had a birthday party at their banquet hall with DJ, what a place...perfect service, hospitality. mr anil is too good and so his support statff.', 'Hotel Royal Park, Patel Complex, Opposite Johnson and Johnson, Safed Pool', 'Dumas Road', 'India', 'Gujarat', 'Surat', '6352524101', 200.00, 100.00, 300, '2019-10-31 00:00:00', '2019-12-26 11:12:00', '2019-12-27 11:12:00', '1570892794.jpg', 0, NULL, '2019-10-12 09:36:34', '2019-10-20 10:30:42', 229),
(4, 3, 3, 'public', 'Fresher Party', 'To throw a party to celebrate a successful fresher party or just to arrange a get-together. We can provide ideal venue for delightfully banquets.', '42-43,Kusum Marg, Near DLF Phase 1 Police Station', 'DLF Phase 1, Gurgaon', 'India', 'Gujarat', 'Navsari', '8852045120', 150.00, 100.00, 350, '2019-10-31 00:00:00', '2019-12-20 18:00:00', '2019-12-22 22:00:00', '1570897192.jpg', 0, NULL, '2019-10-12 10:49:52', '2019-10-20 09:09:33', 330),
(5, 3, 4, 'private', 'Baby Shower Function', 'We creates the perfect way to toast to the health of mother and baby or congratulate mother to be on her upcoming arrival at one of our bespoke baby showers.', 'Next to Vishal E Square multiplex', 'Pimpri', 'India', 'Maharashtra', 'Pune', '9635252414', 150.00, 100.00, 200, '2019-12-15 00:00:00', '2020-02-12 10:02:00', '2020-02-13 10:02:00', '1570897685.png', 0, NULL, '2019-10-12 10:58:05', '2019-10-20 09:14:23', 194),
(6, 3, 4, 'private', 'Baby Shower', 'We makes your baby shower function ideally and memorable for you. We provides the services like food, balloons and lighting decoration, AC, chair, Music etc..', 'Swatantriyveer Savarkar Marg, Opp.Catering College', 'Dadar West', 'India', 'Maharashtra', 'Mumbai', '6352414141', 200.00, 100.00, 500, '2019-11-15 00:00:00', '2020-01-25 10:01:00', '2020-01-26 12:01:00', '1571587058.jpeg', 0, NULL, '2019-10-12 11:11:25', '2019-10-20 10:27:38', 500),
(7, 2, 5, 'private', 'Wedding Anniversary', 'Some days should never be forgotten, and this is one of them. Save your time for fun and celebrations  and give your loved ones the ultimate surprise party on their special day.', 'Plot No. D-359, Near Gaurav Tower', 'Malviya Nagar', 'India', 'Gujarat', 'Vapi', '7458552202', 150.00, 100.00, 450, '2019-12-31 00:00:00', '2020-02-02 06:02:00', '2020-02-04 10:02:00', '1570971170.jpg', 0, NULL, '2019-10-13 07:22:50', '2019-10-20 04:29:19', 450),
(8, 2, 6, 'public', 'DJ Night', 'DJ Night Party with RJ Russel is rocking and excited. Guys will enjoy these nights.', 'Plot No. 5-A, Sudder Street,Near Fire Brigade', 'Dumas', 'India', 'Gujarat', 'Surat', '9352525212', 200.00, 120.00, 350, '2019-10-31 00:00:00', '2020-01-10 06:00:00', '2020-01-13 23:59:00', '1570976113.jpg', 0, NULL, '2019-10-13 08:45:13', '2019-10-20 11:12:10', 281),
(9, 2, 7, 'private', 'Dream Wedding', 'No other day in your life compares to your wedding day. From the big fat Indian weddings to a small engagement ceremonies, we\'ll ensure your big day is full of celebration and revelry. Photography, Catering and Music we has got everything covered.', 'Swatantriyveer Savarkar Marg, Opp.Catering College', 'Dadar West', 'India', 'Maharashtra', 'Mumbai', '6352410001', 250.00, 100.00, 300, '2019-12-31 00:00:00', '2020-03-01 06:03:00', '2020-03-07 12:03:00', '1570977339.jpg', 0, NULL, '2019-10-13 09:05:39', '2019-10-20 14:14:02', 140),
(10, 2, 6, 'public', 'Get Together', 'a wide range of products and services to cater to the varied requirements of their customers. The staff at this establishment are courteous and prompt at providing any assistance.', '3rd Floor, Rahulraj Mall, Piplod', 'Dumas Road', 'India', 'Gujarat', 'Surat', '9856565520', 150.00, 100.00, 300, '2020-02-28 00:00:00', '2020-04-17 17:00:00', '2020-04-19 23:00:00', '1570992044.jpg', 0, NULL, '2019-10-13 13:10:44', '2019-10-20 10:08:51', 288),
(11, 4, 8, 'public', 'Navratri Celebration of 2K19', 'The garba nights at indoor stadium is known for their brilliant selection of songs and the DJ dance celebration at the end of every night. The most of the young crowd of the city turn up at here and celebrate the nine nights with full on enjoyment and madness.', 'Deendayal Upadhyay Indoor Stadium, Ghod Dod Road', 'Athwa lines', 'India', 'Gujarat', 'Surat', '9754541112', 250.00, 150.00, 1000, '2019-09-20 00:00:00', '2019-09-29 07:09:00', '2019-10-07 11:10:00', '1570995252.jpeg', 0, NULL, '2019-10-13 14:04:12', '2019-10-13 14:06:57', 1000),
(12, 4, 9, 'public', 'New Year Celebration', 'Get ready to beat the new year at the beautiful moonlight party in India. Make this New Year Eve 2020 a memorable one by going to a one of a kind bash!!! This new year, reverberate in the celebrations at your favourite destinations moonlight 2020 under the stars with live music, DJ, dance parties and alcohol.', 'VR Mall,Dumas Rd', 'Magdalla', 'India', 'Gujarat', 'Surat', '9565232311', 200.00, 100.00, 500, '2019-12-15 00:00:00', '2019-12-31 20:00:00', '2020-01-01 06:00:00', '1570997219.jpg', 0, NULL, '2019-10-13 14:36:59', '2019-10-20 10:20:38', 570),
(13, 3, 11, 'private', 'Conference Meeting', 'Our large meeting room / boardroom seats up to 50 people very comfortably and is stylized with formal furnishings and excellent acoustics for presentations.', 'Next to Vishal E Square multiplex', 'VR Mall,', 'India', 'Gujarat', 'Surat', '9635252410', 200.00, 100.00, 60, '2019-11-15 00:00:00', '2019-12-01 08:00:00', '2019-12-02 22:00:00', '1571382417.jpg', 0, NULL, '2019-10-18 01:36:57', '2019-10-20 10:40:02', 0),
(14, 2, 12, 'public', 'Memorable Farewell', 'To throw a party to celebrate a successful farewell party or just to arrange a get-together. We can provide ideal venue for delightfully banquets.', '1st Floor, Regency Plaza, Anand Nagar Cross Road', 'Near Rahul Tower', 'India', 'Gujarat', 'Surat', '9874563210', 170.00, 100.00, 300, '2019-12-30 00:00:00', '2020-02-12 06:02:00', '2020-02-13 08:02:00', '1571386137.jpg', 0, NULL, '2019-10-18 02:29:02', '2019-10-18 02:38:57', 300),
(15, 1, 10, 'public', 'Morning Party', 'To throw a party to celebrate a successful morning party or just to arrange a get-together. We can provide ideal venue for delightfully banquets.', 'SMC Community Hall', 'Adajan', 'India', 'Gujarat', 'Surat', '9856321420', 150.00, 100.00, 250, '2019-12-15 00:00:00', '2020-01-10 07:00:00', '2020-01-10 12:00:00', '1571463594.jpg', 0, NULL, '2019-10-19 00:09:54', '2019-10-20 14:54:47', 170),
(16, 1, 10, 'public', 'Evening Party', 'To throw a party to celebrate a successful evening party or just to arrange a get-together. We can provide ideal venue for delightfully banquets.', 'La Victoria Banquets, Near, Galaxy Cir', 'Adajan Gam, Pal', 'India', 'Gujarat', 'Surat', '9745656520', 170.00, 100.00, 350, '2019-12-20 00:00:00', '2020-01-22 06:01:00', '2020-01-22 10:01:00', '1571467838.jpg', 0, NULL, '2019-10-19 00:39:16', '2019-10-19 01:22:03', 350),
(17, 1, 10, 'public', 'Annual Fuction', 'Baghbaan Party Plot spacious outdoor venue with great ambiance. This is one of the most popular venues in the area and offers in-house decorators and caterers, AC green room, and excellent and assured spotless services. They offer large menu spread with a customization option. Valet parking facility is available too.', 'Baghbaan Party Plot, Palanpur Canal Road, Near Maruti Suzuki Workshop', 'Palanpur', 'India', 'Gujarat', 'Surat', '9745632111', 150.00, 100.00, 300, '2020-01-20 00:00:00', '2020-02-15 19:00:00', '2020-02-16 22:00:00', '1571467448.png', 0, NULL, '2019-10-19 01:14:08', '2019-10-20 10:11:40', 280),
(18, 3, 3, 'private', 'test', 'test pkg', '24, xyz', 'simada', 'india', 'gujarat', 'surat', '9988774455', 50.00, 20.00, 100, '2019-12-24 00:00:00', '2020-01-04 12:01:00', '2020-01-04 20:00:00', '1571574514.jpg', 0, '2019-10-20 06:58:41', '2019-10-20 06:58:34', '2019-10-20 06:58:41', 100);

--
-- Dumping data for table `package_features`
--

INSERT INTO `package_features` (`id`, `uid`, `pid`, `icon`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'birthday-cake', 'Food', NULL, '2019-10-12 09:38:42', '2019-10-12 09:38:42'),
(2, 1, 1, 'glass', 'Drinks', NULL, '2019-10-12 09:42:06', '2019-10-12 09:42:06'),
(3, 1, 1, 'music', 'Music', NULL, '2019-10-12 09:43:03', '2019-10-12 09:43:03'),
(4, 1, 1, 'cutlery', 'Food', NULL, '2019-10-12 09:49:29', '2019-10-12 09:49:29'),
(5, 1, 1, 'bolt', 'Decoration', NULL, '2019-10-12 09:54:01', '2019-10-19 00:12:20'),
(6, 1, 2, 'cutlery', 'Food', NULL, '2019-10-12 09:58:33', '2019-10-12 09:58:33'),
(7, 1, 2, 'music', 'Music', NULL, '2019-10-12 10:00:34', '2019-10-12 10:00:34'),
(8, 1, 2, 'beer', 'Drinks', NULL, '2019-10-12 10:01:00', '2019-10-12 10:01:29'),
(9, 1, 2, 'bolt', 'Decoration', NULL, '2019-10-12 10:02:18', '2019-10-19 00:12:07'),
(10, 1, 3, 'birthday-cake', 'Cake', NULL, '2019-10-12 10:02:59', '2019-10-12 10:02:59'),
(11, 1, 3, 'beer', 'Drinks', NULL, '2019-10-12 10:03:34', '2019-10-12 10:03:34'),
(12, 1, 3, 'music', 'Music', NULL, '2019-10-12 10:04:03', '2019-10-12 10:04:03'),
(13, 1, 3, 'bolt', 'Decoration', NULL, '2019-10-12 10:04:36', '2019-10-19 00:13:06'),
(14, 3, 4, 'music', 'DJ', NULL, '2019-10-12 11:12:14', '2019-10-12 11:12:14'),
(15, 3, 4, 'life-ring', 'AC', NULL, '2019-10-12 11:17:42', '2019-10-12 11:17:42'),
(16, 3, 4, 'glass', 'Drinks', NULL, '2019-10-12 11:18:56', '2019-10-12 11:18:56'),
(17, 3, 4, 'lightbulb-o', 'Lighting', NULL, '2019-10-12 11:19:38', '2019-10-12 11:19:38'),
(18, 3, 5, 'square-o', 'Table-chair', NULL, '2019-10-12 11:20:31', '2019-10-18 01:44:59'),
(19, 3, 5, 'lightbulb-o', 'Lighting with Balloons', NULL, '2019-10-12 11:21:44', '2019-10-12 11:21:44'),
(20, 3, 5, 'life-ring', 'AC', NULL, '2019-10-12 11:22:29', '2019-10-12 11:22:29'),
(21, 3, 5, 'cutlery', 'Food', NULL, '2019-10-12 11:23:03', '2019-10-12 11:23:03'),
(22, 3, 5, 'glass', 'Drinks', NULL, '2019-10-12 11:24:38', '2019-10-12 11:24:48'),
(23, 3, 6, 'cutlery', 'Food', NULL, '2019-10-12 11:25:25', '2019-10-12 11:25:25'),
(24, 3, 6, 'glass', 'Drinks', NULL, '2019-10-12 11:25:41', '2019-10-12 11:25:41'),
(25, 3, 6, 'lightbulb-o', 'Lighting with Balloons', NULL, '2019-10-12 11:26:26', '2019-10-12 11:27:56'),
(26, 3, 6, 'users', 'Seating Arrangements', NULL, '2019-10-12 11:27:44', '2019-10-12 11:28:11'),
(27, 3, 6, 'life-ring', 'AC', NULL, '2019-10-12 11:30:08', '2019-10-12 11:30:08'),
(28, 3, 6, 'music', 'Music', NULL, '2019-10-12 11:31:08', '2019-10-12 11:31:08'),
(29, 3, 5, 'users', 'Seating Arrangements', NULL, '2019-10-12 11:32:58', '2019-10-12 11:32:58'),
(30, 2, 7, 'cutlery', 'Food', NULL, '2019-10-13 09:07:26', '2019-10-13 09:07:26'),
(31, 2, 7, 'glass', 'Drinks', NULL, '2019-10-13 09:07:50', '2019-10-13 09:07:50'),
(32, 2, 7, 'life-ring', 'AC', NULL, '2019-10-13 09:08:14', '2019-10-13 09:08:14'),
(33, 2, 7, 'users', 'Seating Arrangements', NULL, '2019-10-13 09:08:36', '2019-10-13 09:08:36'),
(34, 2, 7, 'lightbulb-o', 'Lighting with Flowers & Balloons', NULL, '2019-10-13 09:15:30', '2019-10-13 09:18:52'),
(35, 2, 7, 'music', 'Music', NULL, '2019-10-13 09:16:47', '2019-10-13 09:16:47'),
(36, 2, 8, 'music', 'DJ', NULL, '2019-10-13 09:17:10', '2019-10-13 09:17:10'),
(37, 2, 8, 'lightbulb-o', 'Lighting', NULL, '2019-10-13 09:17:36', '2019-10-13 09:17:36'),
(38, 2, 8, 'beer', 'Drinks', NULL, '2019-10-13 09:17:54', '2019-10-13 09:17:54'),
(39, 2, 8, 'user', 'Speaker', NULL, '2019-10-13 09:21:57', '2019-10-13 09:22:08'),
(40, 2, 8, 'headphones', 'Headphones', NULL, '2019-10-13 09:22:36', '2019-10-13 09:24:15'),
(41, 2, 9, 'cutlery', 'Food', NULL, '2019-10-13 09:22:58', '2019-10-13 09:24:44'),
(42, 2, 9, 'glass', 'Drinks', NULL, '2019-10-13 09:25:14', '2019-10-13 09:25:14'),
(43, 2, 9, 'music', 'Music', NULL, '2019-10-13 09:25:42', '2019-10-13 09:25:42'),
(44, 2, 9, 'lightbulb-o', 'Decoration like Lighitng, Flowers etc', NULL, '2019-10-13 09:27:03', '2019-10-13 09:30:01'),
(45, 2, 9, 'life-ring', 'AC', NULL, '2019-10-13 09:27:45', '2019-10-13 09:27:45'),
(46, 2, 9, 'users', 'Seating Arrangements', NULL, '2019-10-13 09:28:13', '2019-10-13 09:28:13'),
(47, 2, 10, 'life-ring', 'AC', NULL, '2019-10-13 13:11:50', '2019-10-13 13:11:50'),
(48, 2, 10, 'lightbulb-o', 'Decoration like Lighting, Flowers etc', NULL, '2019-10-13 13:14:12', '2019-10-13 13:14:12'),
(49, 2, 10, 'users', 'Seating Arrangements', NULL, '2019-10-13 13:14:47', '2019-10-13 13:14:47'),
(50, 2, 10, 'glass', 'Drinks', NULL, '2019-10-13 13:15:08', '2019-10-13 13:17:00'),
(51, 2, 10, 'wifi', 'WiFi', NULL, '2019-10-13 13:16:40', '2019-10-13 13:16:40'),
(52, 4, 11, 'lightbulb-o', 'Decoration with Lightings', NULL, '2019-10-13 14:09:08', '2019-10-13 14:12:55'),
(53, 4, 11, 'glass', 'Drinks', NULL, '2019-10-13 14:10:00', '2019-10-13 14:10:00'),
(54, 4, 11, 'music', 'Sound System', NULL, '2019-10-13 14:11:29', '2019-10-13 14:11:29'),
(55, 4, 12, 'beer', 'Drinks', NULL, '2019-10-13 14:37:56', '2019-10-13 14:37:56'),
(56, 4, 12, 'lightbulb-o', 'Decoration with Lightings', NULL, '2019-10-13 14:38:31', '2019-10-13 14:38:31'),
(57, 4, 12, 'music', 'DJ', NULL, '2019-10-13 14:39:21', '2019-10-13 14:39:21'),
(58, 4, 12, 'bomb', 'Fireworks', NULL, '2019-10-13 14:43:15', '2019-10-13 14:43:15'),
(59, 3, 13, 'television', 'Screen', NULL, '2019-10-18 01:38:00', '2019-10-18 01:41:41'),
(60, 3, 13, 'wifi', 'WiFi', NULL, '2019-10-18 01:40:00', '2019-10-18 01:40:00'),
(61, 3, 13, 'square-o', 'Table-chair', NULL, '2019-10-18 01:44:21', '2019-10-18 01:45:16'),
(62, 3, 13, 'phone', 'Conference Phone', NULL, '2019-10-18 01:46:42', '2019-10-18 01:46:42'),
(63, 3, 13, 'print', 'Printer', NULL, '2019-10-18 01:47:07', '2019-10-18 01:47:07'),
(64, 3, 13, 'coffee', 'Coffee', NULL, '2019-10-18 01:47:50', '2019-10-18 01:47:50'),
(65, 2, 14, 'cutlery', 'Food', NULL, '2019-10-18 02:31:34', '2019-10-18 02:31:34'),
(66, 2, 14, 'glass', 'Drinks', NULL, '2019-10-18 02:32:03', '2019-10-18 02:32:03'),
(67, 2, 14, 'square-o', 'Table-chair', NULL, '2019-10-18 02:32:35', '2019-10-18 02:32:35'),
(68, 2, 14, 'bolt', 'Decoration like Lighitng, Balloons, Flowers etc', NULL, '2019-10-18 02:33:37', '2019-10-18 02:33:37'),
(69, 2, 14, 'music', 'DJ', NULL, '2019-10-18 02:34:36', '2019-10-18 02:48:32'),
(70, 2, 14, 'life-ring', 'AC', NULL, '2019-10-18 02:48:54', '2019-10-18 02:48:54'),
(71, 1, 15, 'music', 'DJ', NULL, '2019-10-19 00:10:27', '2019-10-19 00:10:27'),
(72, 1, 15, 'glass', 'Drinks', NULL, '2019-10-19 00:10:46', '2019-10-19 00:10:46'),
(73, 1, 15, 'cutlery', 'Food', NULL, '2019-10-19 00:14:54', '2019-10-19 00:14:54'),
(74, 1, 17, 'music', 'DJ', NULL, '2019-10-19 01:14:41', '2019-10-19 01:14:41'),
(75, 1, 17, 'glass', 'Drinks', NULL, '2019-10-19 01:15:01', '2019-10-19 01:15:01'),
(76, 1, 17, 'cutlery', 'Food', NULL, '2019-10-19 01:15:27', '2019-10-19 01:15:27'),
(77, 1, 17, 'bolt', 'Decoration with Lightings', NULL, '2019-10-19 01:16:34', '2019-10-19 01:16:43'),
(78, 1, 17, 'users', 'Seating Arrangements', NULL, '2019-10-19 01:17:53', '2019-10-19 01:19:02'),
(79, 3, 13, 'medkit', 'medicine kit', '2019-10-20 07:00:03', '2019-10-20 06:10:35', '2019-10-20 07:00:03'),
(80, 3, 4, 'bomb', 'Fireworks', '2019-10-20 07:00:01', '2019-10-20 06:13:12', '2019-10-20 07:00:01');

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `uid`, `pkg_book_id`, `txn_id`, `amount`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 9, 1, '1b87c31631bea462cf46', 2500.00, 1, NULL, '2019-10-19 01:51:34', '2019-10-19 01:51:34'),
(2, 7, 2, '4066d26eba4d71ca95cf', 3000.00, 1, NULL, '2019-10-20 00:33:09', '2019-10-20 00:33:09'),
(3, 8, 3, '11de41fd9830c780a0c6', 15000.00, 1, NULL, '2019-10-20 01:05:29', '2019-10-20 01:05:29'),
(4, 8, 4, '9f166837825528b32725', 1000.00, 1, NULL, '2019-10-20 01:11:13', '2019-10-20 01:11:13'),
(5, 7, 7, 'cd93115a6ed3caa05427', 1000.00, 1, NULL, '2019-10-20 03:16:09', '2019-10-20 03:16:09'),
(6, 3, 8, 'ce0be5ce2ed63855e50b', 500.00, 1, NULL, '2019-10-20 03:25:02', '2019-10-20 03:25:02'),
(7, 3, 9, 'edf7e9e578781e562eaf', 960.00, 1, NULL, '2019-10-20 03:32:00', '2019-10-20 03:32:00'),
(8, 1, 12, '75aaefde6a4093ddebe2', 2000.00, 1, NULL, '2019-10-20 03:40:34', '2019-10-20 03:40:34'),
(9, 1, 13, '8af0ace57ccc7cd30247', 600.00, 1, NULL, '2019-10-20 03:44:49', '2019-10-20 03:44:49'),
(10, 3, 16, '2bf6fbb070ab1d68e7c5', 650.00, 1, NULL, '2019-10-20 04:06:22', '2019-10-20 04:06:22'),
(11, 2, 24, '246431e795aefda173f2', 2000.00, 1, NULL, '2019-10-20 04:42:13', '2019-10-20 04:42:13'),
(12, 2, 27, 'cd93a6aed83976fef5f1', 2000.00, 1, NULL, '2019-10-20 04:51:02', '2019-10-20 04:51:02'),
(13, 6, 29, '9216e552e748dd7a9745', 600.00, 1, NULL, '2019-10-20 05:01:05', '2019-10-20 05:01:05'),
(14, 6, 32, '2aabe8dc603a93de3dc3', 6000.00, 1, NULL, '2019-10-20 05:10:28', '2019-10-20 05:10:28'),
(15, 8, 34, '699c3e7745340f58b123', 720.00, 1, NULL, '2019-10-20 05:19:21', '2019-10-20 05:19:21'),
(16, 5, 41, '607ec1bd787d9f05a935', 600.00, 1, NULL, '2019-10-20 05:32:00', '2019-10-20 05:32:00'),
(17, 5, 43, '0c6ced98c6d365aad220', 40000.00, 1, NULL, '2019-10-20 05:34:45', '2019-10-20 05:34:45'),
(18, 4, 46, '955f819ad2313917b20e', 6000.00, 1, NULL, '2019-10-20 05:43:19', '2019-10-20 05:43:19'),
(19, 8, 51, 'fad08e2cd30bb109f87f', 500.00, 1, NULL, '2019-10-20 08:44:38', '2019-10-20 08:44:38'),
(20, 8, 36, '976e6f20fcd1842bf566', 3000.00, 1, NULL, '2019-10-20 09:25:16', '2019-10-20 09:25:16');

--
-- Dumping data for table `pkg_bookings`
--

INSERT INTO `pkg_bookings` (`id`, `uid`, `pid`, `total_persons`, `total_amount`, `deposite`, `payment_status`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 9, 3, 25, 5000.00, 2500.00, 1, 1, NULL, '2019-10-19 01:50:16', '2019-10-19 01:51:39'),
(2, 7, 3, 30, 6000.00, 3000.00, 1, 1, NULL, '2019-10-20 00:31:46', '2019-10-20 00:33:16'),
(3, 8, 9, 150, 37500.00, 15000.00, 1, 1, NULL, '2019-10-20 01:03:10', '2019-10-20 01:05:35'),
(4, 8, 3, 10, 2000.00, 1000.00, 1, 1, NULL, '2019-10-20 01:09:47', '2019-10-20 01:11:19'),
(5, 7, 8, 10, 2000.00, 1200.00, 0, 3, NULL, '2019-10-20 01:18:03', '2019-10-20 01:18:36'),
(6, 7, 6, 300, 60000.00, 30000.00, 0, 2, NULL, '2019-10-20 01:20:22', '2019-10-20 01:21:34'),
(7, 7, 12, 10, 2000.00, 1000.00, 1, 1, NULL, '2019-10-20 03:15:37', '2019-10-20 03:16:15'),
(8, 3, 9, 5, 1250.00, 500.00, 1, 1, NULL, '2019-10-20 03:23:33', '2019-10-20 03:25:08'),
(9, 3, 8, 8, 1600.00, 960.00, 1, 1, NULL, '2019-10-20 03:31:11', '2019-10-20 03:32:06'),
(10, 3, 13, 2, 400.00, 200.00, 0, 3, NULL, '2019-10-20 03:35:31', '2019-10-20 03:36:01'),
(11, 3, 3, 8, 1600.00, 800.00, 0, 3, NULL, '2019-10-20 03:37:32', '2019-10-20 04:03:58'),
(12, 1, 4, 20, 3000.00, 2000.00, 1, 1, NULL, '2019-10-20 03:39:20', '2019-10-20 03:40:39'),
(13, 1, 5, 6, 900.00, 600.00, 1, 1, NULL, '2019-10-20 03:43:59', '2019-10-20 03:44:54'),
(14, 1, 13, 4, 800.00, 400.00, 0, 1, NULL, '2019-10-20 04:02:04', '2019-10-20 04:02:16'),
(15, 3, 3, 7, 1400.00, 700.00, 0, 1, NULL, '2019-10-20 04:03:38', '2019-10-20 04:03:48'),
(16, 3, 1, 5, 1000.00, 650.00, 1, 1, NULL, '2019-10-20 04:05:37', '2019-10-20 04:06:28'),
(17, 3, 15, 20, 3000.00, 2000.00, 0, 1, NULL, '2019-10-20 04:10:07', '2019-10-20 04:10:07'),
(19, 3, 2, 250, 42500.00, 25000.00, 0, 2, NULL, '2019-10-20 04:21:54', '2019-10-20 04:22:25'),
(20, 1, 7, 350, 52500.00, 35000.00, 0, 2, NULL, '2019-10-20 04:30:30', '2019-10-20 04:30:48'),
(21, 1, 14, 230, 39100.00, 23000.00, 0, 3, NULL, '2019-10-20 04:33:23', '2019-10-20 04:33:57'),
(22, 1, 10, 12, 1800.00, 1200.00, 0, 1, NULL, '2019-10-20 04:34:37', '2019-10-20 04:34:37'),
(23, 2, 6, 400, 80000.00, 40000.00, 0, 1, NULL, '2019-10-20 04:40:21', '2019-10-20 04:40:52'),
(24, 2, 17, 20, 3000.00, 2000.00, 1, 1, NULL, '2019-10-20 04:41:38', '2019-10-20 04:42:18'),
(25, 2, 16, 15, 2550.00, 1500.00, 0, 3, NULL, '2019-10-20 04:48:37', '2019-10-20 04:48:45'),
(26, 2, 13, 4, 800.00, 400.00, 0, 2, NULL, '2019-10-20 04:49:36', '2019-10-20 04:49:46'),
(27, 2, 12, 20, 4000.00, 2000.00, 1, 1, NULL, '2019-10-20 04:50:25', '2019-10-20 04:51:07'),
(28, 2, 14, 17, 2890.00, 1700.00, 0, 1, NULL, '2019-10-20 04:52:04', '2019-10-20 04:52:04'),
(29, 6, 3, 6, 1200.00, 600.00, 1, 1, NULL, '2019-10-20 05:00:13', '2019-10-20 05:01:11'),
(30, 6, 8, 14, 2800.00, 1680.00, 0, 1, NULL, '2019-10-20 05:05:13', '2019-10-20 05:05:13'),
(31, 6, 10, 6, 900.00, 600.00, 0, 3, NULL, '2019-10-20 05:07:27', '2019-10-20 05:07:31'),
(32, 6, 13, 60, 12000.00, 6000.00, 1, 1, NULL, '2019-10-20 05:09:21', '2019-10-20 05:10:33'),
(33, 6, 4, 12, 1800.00, 1200.00, 0, 1, NULL, '2019-10-20 05:11:17', '2019-10-20 05:11:17'),
(34, 8, 8, 6, 1200.00, 720.00, 1, 1, NULL, '2019-10-20 05:18:03', '2019-10-20 05:19:27'),
(35, 8, 9, 50, 12500.00, 5000.00, 0, 0, NULL, '2019-10-20 05:20:06', '2019-10-20 05:20:06'),
(36, 8, 15, 30, 4500.00, 3000.00, 1, 1, NULL, '2019-10-20 05:21:26', '2019-10-20 09:25:21'),
(37, 2, 3, 30, 6000.00, 3000.00, 0, 0, NULL, '2019-10-20 05:22:39', '2019-10-20 05:22:39'),
(38, 7, 16, 10, 1700.00, 1000.00, 0, 1, NULL, '2019-10-20 05:26:14', '2019-10-20 05:26:14'),
(39, 7, 7, 400, 60000.00, 40000.00, 0, 0, NULL, '2019-10-20 05:27:48', '2019-10-20 05:27:48'),
(40, 5, 9, 15, 3750.00, 1500.00, 0, 1, NULL, '2019-10-20 05:31:08', '2019-10-20 10:48:31'),
(41, 5, 8, 5, 1000.00, 600.00, 1, 1, NULL, '2019-10-20 05:31:34', '2019-10-20 05:32:07'),
(42, 5, 14, 8, 1360.00, 800.00, 0, 1, NULL, '2019-10-20 05:32:43', '2019-10-20 05:32:43'),
(43, 5, 6, 400, 80000.00, 40000.00, 1, 1, NULL, '2019-10-20 05:33:27', '2019-10-20 05:34:51'),
(44, 5, 4, 30, 4500.00, 3000.00, 0, 3, NULL, '2019-10-20 05:36:44', '2019-10-20 05:36:46'),
(45, 5, 1, 40, 8000.00, 5200.00, 0, 2, NULL, '2019-10-20 05:37:45', '2019-10-20 05:38:11'),
(46, 4, 8, 50, 10000.00, 6000.00, 1, 1, NULL, '2019-10-20 05:42:07', '2019-10-20 05:43:25'),
(47, 4, 9, 16, 4000.00, 1600.00, 0, 0, NULL, '2019-10-20 05:44:21', '2019-10-20 05:44:21'),
(48, 4, 3, 40, 8000.00, 4000.00, 0, 2, NULL, '2019-10-20 05:44:39', '2019-10-20 05:46:35'),
(49, 4, 2, 50, 8500.00, 5000.00, 0, 1, NULL, '2019-10-20 05:45:14', '2019-10-20 05:45:50'),
(50, 4, 5, 30, 4500.00, 3000.00, 0, 3, NULL, '2019-10-20 05:47:54', '2019-10-20 05:48:02'),
(51, 8, 9, 5, 1250.00, 500.00, 1, 1, NULL, '2019-10-20 08:43:13', '2019-10-20 08:44:43');

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `uid`, `pid`, `rating`, `review`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 8, 9, 4.5, 'Nice facilities', NULL, '2019-10-20 01:09:01', '2019-10-20 01:09:01'),
(2, 8, 3, 4.5, 'Nice facilities', NULL, '2019-10-20 01:14:02', '2019-10-20 01:14:02'),
(3, 7, 3, 5.0, 'Awesome party...:)', NULL, '2019-10-20 01:14:59', '2019-10-20 01:14:59'),
(4, 7, 3, 4.5, 'Nice decoration :)', NULL, '2019-10-20 01:15:41', '2019-10-20 01:15:41'),
(5, 7, 8, 3.0, 'Good :)', NULL, '2019-10-20 01:19:41', '2019-10-20 01:19:41'),
(6, 7, 6, 3.5, 'Nice Decoration...', NULL, '2019-10-20 01:24:12', '2019-10-20 01:24:12'),
(7, 7, 12, 5.0, 'One of the best mall in Surat. And New Year party of this mall is very popular in Surat.....', NULL, '2019-10-20 03:22:10', '2019-10-20 03:22:10'),
(8, 3, 8, 4.5, 'Great music thanks!', NULL, '2019-10-20 03:32:29', '2019-10-20 03:32:29'),
(9, 3, 9, 4.5, 'Decoration is awesome....', NULL, '2019-10-20 03:33:57', '2019-10-20 03:33:57'),
(10, 1, 5, 4.0, 'Nice Decoration :)', NULL, '2019-10-20 03:49:52', '2019-10-20 03:49:52'),
(11, 1, 4, 4.5, 'The music was fantastic, lots of people were dancing and it was a super day!', NULL, '2019-10-20 03:50:49', '2019-10-20 03:50:49'),
(12, 1, 4, 5.0, 'Really Enjoiy...... :)', NULL, '2019-10-20 04:00:30', '2019-10-20 04:00:30'),
(13, 3, 2, 4.0, 'Decoration is very attractive...', NULL, '2019-10-20 04:23:32', '2019-10-20 04:23:32'),
(14, 3, 15, 4.5, 'College party is memorable for me  :)', NULL, '2019-10-20 04:24:42', '2019-10-20 04:24:42'),
(15, 3, 1, 4.5, 'Very nice and Memorable Party...', NULL, '2019-10-20 04:26:26', '2019-10-20 04:26:26'),
(16, 1, 7, 4.5, 'Nice Decoration and food was so delicious....', NULL, '2019-10-20 04:32:11', '2019-10-20 04:32:11'),
(17, 1, 10, 4.5, 'The hall which we used was awesome and ambiance was perfectly set out for a full enjoyment...:)', NULL, '2019-10-20 04:38:33', '2019-10-20 04:38:33'),
(18, 2, 17, 4.5, 'Annual fuction of the year was very enjoyable and grate...', NULL, '2019-10-20 04:44:45', '2019-10-20 04:44:45'),
(19, 2, 17, 4.5, 'Awesome Fuction...', NULL, '2019-10-20 04:45:09', '2019-10-20 04:45:09'),
(20, 2, 6, 4.5, 'Function was grate....', NULL, '2019-10-20 04:46:47', '2019-10-20 04:46:47'),
(21, 2, 12, 4.5, 'Every year, this new year party is so enjoyable and fantastic.....', NULL, '2019-10-20 04:57:01', '2019-10-20 04:57:01'),
(22, 2, 13, 4.0, 'The environment and facilities for Conferences is grate...', NULL, '2019-10-20 04:59:02', '2019-10-20 04:59:02'),
(23, 6, 3, 4.5, 'Enjoyable party...', NULL, '2019-10-20 05:01:52', '2019-10-20 05:01:52'),
(24, 6, 8, 4.5, 'Music and party was awesome..', NULL, '2019-10-20 05:06:43', '2019-10-20 05:06:43'),
(25, 6, 10, 4.5, 'Get Together was memorable...', NULL, '2019-10-20 05:08:27', '2019-10-20 05:08:27'),
(26, 6, 4, 4.5, 'This Fresher is awesome for me.... :)', NULL, '2019-10-20 05:12:13', '2019-10-20 05:12:13'),
(27, 6, 13, 3.5, 'Peaceful Environment ..', NULL, '2019-10-20 05:14:24', '2019-10-20 05:14:24'),
(28, 6, 13, 4.0, 'Big hall and good facilities.....', NULL, '2019-10-20 05:15:36', '2019-10-20 05:15:36'),
(29, 8, 8, 4.5, 'Awesome Party.....', NULL, '2019-10-20 05:18:33', '2019-10-20 05:18:33'),
(30, 8, 9, 4.5, 'Awesome Decoration and Delicious food....', NULL, '2019-10-20 05:20:48', '2019-10-20 05:20:48'),
(31, 2, 3, 4.0, 'Great party...:)', NULL, '2019-10-20 05:23:22', '2019-10-20 05:23:22'),
(32, 7, 16, 4.5, 'Awesome party...:)', NULL, '2019-10-20 05:26:42', '2019-10-20 05:26:42'),
(33, 7, 7, 4.5, 'Wonderful Decoration..', NULL, '2019-10-20 05:29:20', '2019-10-20 05:29:20'),
(34, 7, 7, 4.0, 'Nice facilities....', NULL, '2019-10-20 05:29:40', '2019-10-20 05:29:40'),
(35, 5, 6, 4.5, 'Awesome function....', NULL, '2019-10-20 05:34:26', '2019-10-20 05:34:26'),
(36, 5, 14, 4.5, 'Nice Party....:)', NULL, '2019-10-20 05:35:27', '2019-10-20 05:35:27'),
(37, 5, 8, 4.5, 'I really enjoyed this party.....', NULL, '2019-10-20 05:36:18', '2019-10-20 05:36:18'),
(38, 5, 1, 5.0, 'Decoration was so attractive....:)', NULL, '2019-10-20 05:39:15', '2019-10-20 05:39:15'),
(39, 4, 8, 4.5, 'So enjoyable party...', NULL, '2019-10-20 05:42:56', '2019-10-20 05:42:56'),
(40, 4, 5, 4.5, 'Great Function...', NULL, '2019-10-20 05:48:37', '2019-10-20 05:48:37'),
(41, 4, 5, 4.5, 'Good...', NULL, '2019-10-20 05:48:59', '2019-10-20 05:48:59');

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `email`, `email_verified_at`, `password`, `dob`, `address`, `city`, `state`, `country`, `contact_no`, `profile`, `remember_token`, `deleted_at`, `created_at`, `updated_at`, `payumoney_key`, `payumoney_salt`) VALUES
(1, 'Nehal', 'Thoriya', 'nehalthoriya98@gmail.com', '2019-10-12 00:55:43', '$2y$10$WsVxN8iLUkQiMD10xanZge/J9oqfkDD3K2gcdY.YFcDVd1fbNq8T6', '1998-08-04', '5, Yogeshwer soc,  A.K.road', 'Surat', 'Gujarat', 'India', '9145045710', '1570884187.jpg', NULL, NULL, '2019-10-12 07:13:07', '2019-10-12 12:37:14', 'hOGCCWEz', 'uabYyBcBZ6'),
(2, 'Janak', 'Vaghela', 'vaghelajanak97145@gmail.com', '2019-10-12 07:51:21', '$2y$10$jRonba3dkEXhRwyxhFi/A.cgmLv.XYDdp3v389zfcYXWST3GcjKT6', '1997-12-05', '102, Vishal nagar, Hirabag', 'Surat', 'Gujarat', 'India', '9714558826', '1570885520.JPEG', NULL, NULL, '2019-10-12 07:35:20', '2019-10-13 13:19:19', '2vd4SP33', '7YhquAZvSI'),
(3, 'Shradhdha', 'Vekariya', 'shradhdhavekariya67@gmail.com', '2019-10-12 13:52:10', '$2y$10$cDJQRVk77Slc5tHDJiWih.MBFmAC3qPxTYSUq0aVqOaBbIlClbrFy', '1998-03-24', '102, Ajamaldham soc', 'Surat', 'Gujarat', 'India', '6552457852', '1570885670.jpeg', NULL, NULL, '2019-10-12 07:37:50', '2019-10-12 12:35:48', 'VqlhshwE', 'Idp9nPz5OG'),
(4, 'Radhe', 'Vekariya', 'radhevekariya1814@gmail.com', '2019-10-13 23:00:17', '$2y$10$Qd2oIcGvHvxf91TdacvBC..ykII2TfH0/ViFdlSYiYXazvQCGwjz2', '1998-10-27', '10, Dharam Nagar, A.k. road', 'Surat', 'Gujarat', 'India', '9966587741', '1570885963.jpg', NULL, NULL, '2019-10-12 07:42:43', '2019-10-13 14:08:29', 'VqlhshwE', 'Idp9nPz5OG'),
(5, 'Smita', 'Sorthiya', 'smitapatel95375@gmail.com', '2019-10-23 00:58:11', '$2y$10$YnlXB0eZxyO0mGMjaxaai./0haWQiJnKSKZMCOfL0ptt1lUdli6Zy', NULL, NULL, NULL, NULL, NULL, '9099199297', '1570887141.jpeg', NULL, NULL, '2019-10-12 08:02:21', '2019-10-12 08:02:21', NULL, NULL),
(6, 'Dhaval', 'Variya', 'variyadhaval758@gmail.com', '2019-10-18 03:07:51', '$2y$10$PVmosOHL5cqyhLk4f0Awwu/qYrNjfFaGyaupjkZzHJn0bq9T/HSbW', '1997-11-05', 'Katargam', 'Surat', 'Gujarat', 'India', '9308520142', '1571387750.JPG', NULL, NULL, '2019-10-18 03:05:50', '2019-10-18 03:14:35', 'B5ZyvbYT', 'gPptpYrLiH'),
(7, 'Sagar', 'Dhameshiya', 'sdhameshiya95@gmail.com', '2019-10-18 03:20:05', '$2y$10$1VDm7KmvxFL2LqwTzKf3L.E78XVgr6CduPf.pDQIaJbmz8FGa79Fq', NULL, NULL, NULL, NULL, NULL, '9712545516', '1571388381.jpeg', NULL, NULL, '2019-10-18 03:16:21', '2019-10-18 03:20:05', NULL, NULL),
(8, 'Gaurav', 'Thoriya', 'gauravthoriya0@gmail.com', '2019-10-18 21:53:12', '$2y$10$KH2gMvfkpXs8NzGejZC4cuaX/mO9jqWTkwZsO9OrQ9TrfUDGRtHYW', NULL, NULL, NULL, NULL, NULL, '9874563210', '1571468536.jpeg', NULL, NULL, '2019-10-19 01:32:16', '2019-10-19 01:32:16', NULL, NULL),
(9, 'Dhruv', 'Kanapara', 'kanaparadhruv@gmail.com', '2019-10-21 01:06:19', '$2y$10$PfgJas/HLnWVT6LAHAemwuT0zgyFAuAS2WrD2c0f7WxckJii0h9KW', NULL, NULL, NULL, NULL, NULL, '9909546140', '1571468801.jpg', NULL, NULL, '2019-10-19 01:36:41', '2019-10-19 01:36:41', NULL, NULL),
(10, 'Rinkal', 'Varasani', 'rinkalvarsani173@gmail.com', NULL, '$2y$10$1o0aS5hx7RAphhRJf4NppOGTIyzQKe2MaIQtW9s8UCwBGyI.zNdcq', NULL, NULL, NULL, NULL, NULL, '9565652320', '1571550823.jpg', NULL, NULL, '2019-10-20 00:23:43', '2019-10-20 00:23:43', NULL, NULL);
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
