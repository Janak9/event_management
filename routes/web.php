<?php
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('localization/{locale}','LocalizationController@index');
Route::get('/', 'HomeController@home');
//for contact us
Route::get('/contactus','HomeController@contact')->name('contactus');
Route::get('/aboutus','HomeController@aboutus');

//for event list
Route::get('eventlist','HomeController@EventList');

// Event detail
Route::get('eventDetail/{id}', 'HomeController@eventDetail')->name('event-detail');
Route::any('/eventsearch','HomeController@eventSearch')->name('eventsearch');


Route::get('insert_event',function(){
    $e=Artisan::call('add:event');
    //1 return true 0 return false
    if($e==0)
    {
        return response("insert successfully", 200);
    }
    else
    {
        return response("Not insert", 200);
    }
});

//for login , Registration and Logout
Auth::routes();
Auth::routes(['verify' => true]);

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/mail_demo', 'HomeController@mail_demo')->name('mail_demo');

Route::group(['middleware'=>['verified','auth']],function(){

    Route::group(['middleware'=>['checkPayUMoneyKey']],function(){
        //for event CRUD
        Route::resource('events', 'EventsController');
        Route::get('/events/delete/{event}', 'EventsController@destroy')->name('events.destroy');

        //to get only Trashed/Deleted Record of event
        Route::get('trashed_events','EventsController@trashed_event')->name('events.trashed_events');
       
        //to restore the softdeleted event record
        Route::get('restore_events/{id}','EventsController@restore_event')->name('events.restore_events');
       
        //to forcedelete event record
        Route::get('forcedelete_events/{id}','EventsController@forcedel_event')->name('events.forcedelete_events');


        //for package CRUD
        Route::resource('packages', 'PackagesController');
        Route::get('/packages/delete/{package}', 'PackagesController@destroy')->name('packages.destroy');
       
        //to get only Trashed/Deleted Record of package
        Route::get('trashed_packages','PackagesController@trashed_pkg')->name('packages.trashed_packages');
       
        //to restore the softdeleted package record
        Route::get('restore_packages/{id}','PackagesController@restore_pkg')->name('packages.restore_packages');
       
        //to forcedelete package record
        Route::get('forcedelete_packages/{id}','PackagesController@forcedel_pkg')->name('packages.forcedelete_packages');
       
        //for package feature CRUD
        Route::resource('package_features', 'PackageFeaturesController');
        Route::get('/package_features/delete/{package_features}', 'PackageFeaturesController@destroy')->name('package_features.destroy');

        //to get only Trashed/Deleted Record of package feature
        Route::get('trashed_packagefeatures','PackageFeaturesController@trashed_PackageFeatures')->name('package_features.trashed_PackageFeatures');
    
        //to restore the softdeleted package feature record
        Route::get('restore_packagefeatures/{id}','PackageFeaturesController@restore_PackageFeatures')->name('package_features.restore_PackageFeatures');
    
        //to forcedelete package feature record
        Route::get('forcedelete_packagefeatures/{id}','PackageFeaturesController@forcedel_PackageFeatures')->name('package_features.forcedelete_PackageFeatures');
 
    });

    // for profile update
    Route::get('/profileedit','ProfileEditController@index')->name('profile-edit');
    Route::post('/profileedit-update','ProfileEditController@update')->name('profile-edit-post');

    // add PayUMoney Key
    Route::match(['get','post'],'payumoney_key', 'ProfileEditController@add_payumoney_key')->name('payumoney-key');

    // change password
    Route::get('/changepassword','ChangePwdController@index')->name('change-pwd');
    Route::post('/pwd-change-post','ChangePwdController@change');
        
    // Event detail
    Route::post('eventDetail/{id}', 'HomeController@eventDetail')->name('event-detail');

    // for booking
    Route::get('booking/{id}','HomeController@booking');
    Route::post('booking/{id}','HomeController@booking');
    Route::get('booking/cancel/{id}','HomeController@booking_cancel');
    Route::get('mybookings','HomeController@myBookings')->name('my-bookings');
    Route::get('invoice/{id}','HomeController@invoice')->name('invoice');

    // payumoney gateway
    Route::get('payment/{uid}/{pkg_book_id}', 'PayUMoneyController@index')->name('payment');
    Route::any('payment/status', 'PayUMoneyController@status')->name('payment-status');

    // notifications
    Route::get('get_notification/{id}','HomeController@get_notification')->name('get-notification');
    Route::match(['get', 'post'],'notification_details/{id}','HomeController@notification_details')->name('notification-details');
});
